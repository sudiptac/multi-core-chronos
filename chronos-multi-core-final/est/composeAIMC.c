#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"
#include "tcfg.h"
#include "loops.h"
#include "cache.h"
#include "crpd.h"
#include "composeAIMC.h"
/* sudiptac:: For performance measurement */
typedef unsigned long long ticks;
#define CPU_MHZ 3000000000
static __inline__ ticks getticks(void)
{
		  unsigned a, d;
		  __asm volatile("rdtsc" : "=a" (a), "=d" (d));
		  return ((ticks)a) | (((ticks)d) << 32);
}

extern int nsets, bsize, assoc, nsets_l2, bsize_l2, assoc_l2;
int max_conflicts = 64;
int step_size = 8;
static int number_of_refinements = 0;
int number_of_mc_calls = 0;
int num_unclassified = 0;
/* do not export */
static int old_wcet;
static ticks start, end;
static FILE* LogPfp;

/* cached MC result ::: do not export this type */
typedef struct mc_result {
		  int set;
		  int mc_ret;
		  int residual_age;
		  struct mc_result* next;
} mc_refined_result;
mc_refined_result* mc_result_link = NULL;

/* refinement iteration bound */
#define MC_CALL_BOUND 100
/* transformed code file extension */
#define MCFILE_EXT	"__converted__MC"
/* adding a model checker */
#define CBMC "cbmc"
//#define CBMC_OPTIONS "--no-pointer-check --no-bounds-check --no-div-by-zero-check --slice --unwind 4 --no-unwinding-assertions"
#define CBMC_OPTIONS "--no-pointer-check --no-bounds-check --no-div-by-zero-check --slice"
#define CPROVER_ASSERT "__CPROVER_assert"
/* end model checker options */
/* adding a symbolic execution engine support */
#define LLVM_GCC "llvm-gcc"
#define KLEE "klee"
#define KLEE_OPTIONS "-write-cvcs -write-paths -write-pcs"
#define KLEE_ASSERT "klee_assert"
#define KLEE_INCLUDE "#include \"klee.h\""
/* end symbolic execution engine options */

/* for getting virtual address to source code line number */
#define ADDR2LINE	"sslittle-na-sstrix-addr2line"
#define TEMP_FILE_1 "/tmp/chronos_AI_MC_temp_file_1"
#define TEMP_FILE_2 "/tmp/chronos_AI_MC_temp_file_2"
/* following macros are related to the code transformation technique,
 * a bit gross, but cannot be helped */
#define MODIFY_MC_FILE(file, code) fputs(code, file)
#define COUNT_MACRO "#define __COUNT_MACRO(flag_mem_blk, conflict)	__COUNT_CODE(flag_mem_blk, conflict)\n"
#define COUNT_CODE "#define __COUNT_CODE(flag_mem_blk, conflict) {conflict+=(flag_mem_blk==0)?1:0; flag_mem_blk=1;}\n"

/* cache the MC refined result to reduce number of MC calls */
void cache_mc_result(addr_t addr, int mc_ret, int residual_age, int isL2)
{
		  int set;
		  mc_refined_result* rt;

		  if(isL2)
					 set = GET_SET_L2(addr); 
		  else
					 set = GET_SET_L1(addr); 
		  rt = (mc_refined_result *) calloc (1, sizeof(mc_refined_result));
		  CHECK_MEM(rt);
		  rt->set = set;
		  rt->mc_ret = mc_ret;
		  rt->residual_age = residual_age;
		  rt->next = mc_result_link;
		  mc_result_link = rt;
}

/* searched the cached MC refined result */
static int search_cached_mc_result(addr_t addr, int residual_age, int isL2)
{
		  mc_refined_result* rt;
		  int set;

		  if(isL2)
					 set = GET_SET_L2(addr);
		  else
					 set = GET_SET_L1(addr);

		  for (rt = mc_result_link; rt; rt = rt->next)
		  {
					 if(rt->set != set)
								continue;
					 /* cached result say "YES" */			
					 if (rt->mc_ret == 0) {
								if (residual_age >= rt->residual_age)
										  return 0;
					 }
					 /* cached result say "NO" */
					 if(rt->mc_ret != 0) {
								if(residual_age <= rt->residual_age)
										  return 1;
					 }
		  }

		  /* no cached result found */
		  return -1;
}
																
/* get the corresponding line number in source file from an instruction address */
static int get_line_number(char* filename, uint addr)
{
		  char cmd[256];
		  int ret;
		  int line;
		  FILE* fp;

		  sprintf(cmd, "%s -e %s 0x%x > %s", ADDR2LINE, filename, addr, TEMP_FILE_1);
		  ret = system(cmd);
		  if (ret) {
					 fprintf(stdout, "\"addr2line\" has encountered error.....exiting\n");
					 exit(-1);
		  }
		  sprintf(cmd, "cut -d: -f2 %s > %s", TEMP_FILE_1, TEMP_FILE_2);
		  ret = system(cmd);
		  if (ret) {
					 fprintf(stdout, "\"cut\" has encountered error.....exiting\n");
					 exit(-1);
		  }
		  fp = fopen(TEMP_FILE_2, "r");
		  if (!fp) {
					 fprintf(stdout, "Temporary file opening failed....exiting\n");
					 exit(-1);
		  }
		  fscanf(fp, "%d", &line);

		  if (line == 0)
		  {
					 fprintf(stdout, "Cannot determine the line number of address 0x%x\n", addr);
					 fclose(fp);
					 exit(-1);
		  }
		  fclose(fp);

		  return line;
}

/* make a link of conflicting address (with addr) in peer task */
static void link_conflicting_addr(addr_t** addr_link, addr_t addr, int* num_addr, int isL2)
{
		  int i;

		  for (i = 0; i < *num_addr; i++)
		  {
					 assert((*addr_link)[i]);

					 if(isL2 && GET_MEM_BLK_L2(addr) == GET_MEM_BLK_L2((*addr_link)[i]))
								return;						
					 else if(!isL2 && GET_MEM_BLK(addr) == GET_MEM_BLK((*addr_link)[i]))
								return;						
		  }

		  if (*num_addr >= max_conflicts) {
					 *addr_link = (addr_t *) realloc(*addr_link, (max_conflicts + step_size) * sizeof(addr_t));
					 CHECK_MEM(*addr_link);
					 max_conflicts += step_size;
		  }
		  (*addr_link)[*num_addr] = addr;			 
		  (*num_addr)++;
}

/* collect conflicting memory blocks in L1 cache for "addr" */
static addr_t* collect_conflicting_blocks_L1(task_p peer, addr_t addr, int* num_addrs)
{
		  int i, j;
		  addr_t* addr_link;
		  de_inst_t* inst;

		  addr_link = (addr_t *) calloc (max_conflicts, sizeof(addr_t));
		  CHECK_MEM(addr_link);


		  for (i = 0; i < peer->num_tcfg_nodes; i++)
		  {
					 inst = peer->tcfg[i]->bb->code;

					 for (j = 0; j < peer->tcfg[i]->bb->num_inst; j++)
					 {
								if (GET_SET_L1(inst->addr) == GET_SET_L1(addr)) {
										  link_conflicting_addr(&addr_link, inst->addr, num_addrs, 0);
								}
								inst++;
					 }
		  }

		  return addr_link;
}

/* collect conflicting memory blocks in shared L2 cache for "addr" */
static addr_t* collect_conflicting_blocks_L2(task_p peer, addr_t addr, int* num_addrs)
{
		  int i, j;
		  addr_t* addr_link;
		  de_inst_t* inst;

		  addr_link = (addr_t *) calloc (max_conflicts, sizeof(addr_t));
		  CHECK_MEM(addr_link);


		  for (i = 0; i < peer->num_tcfg_nodes; i++)
		  {
					 inst = peer->tcfg[i]->bb->code;

					 for (j = 0; j < peer->tcfg[i]->bb->num_inst; j++)
					 {
								if (peer->inst_chmc_l2[i][j] == ALL_X)
										  continue;

								if (GET_SET_L2(inst->addr) == GET_SET_L2(addr)) {
										  link_conflicting_addr(&addr_link, inst->addr, num_addrs, 1);
								}
								inst++;
					 }
		  }

		  return addr_link;
}

/* run the CBMC model checker on transformed file */
int run_MC(task_p task, addr_t addr, char* fileMC)
{
		  char cmd[256];
		  char llvm_bitcode[256];
		  int ret;
#ifdef _DEBUG_AI_MC
		  fprintf(stdout, "\n-----------------------------------------------------------------------------------------\n");
		  fprintf(stdout, "Trying refinement of instruction 0x%x in task %s..........\n", addr, task->name);
		  fprintf(stdout, "-----------------------------------------------------------------------------------------\n");
#endif
		  number_of_mc_calls++;
			

			/* here we wither call a model checker or a symbolic execution engine 
			 * for cache analysis refinement. For model checker we currently use 
			 * CBMC and for symbolic execution engine, we use KLEE, which works 
			 * on LLVM bitcode and use STP SMT solver */
			if (oracle_type == TYPE_MODEL_CHECK) {
				sprintf(cmd, "%s %s %s", CBMC, CBMC_OPTIONS, fileMC);
				printf("\n.....calling CBMC with the following command.....\n");
				printf("%s\n", cmd);
			}	
			/* use KLEE */	
			else if (oracle_type == TYPE_SYMBOLIC_EXECUTION) {
				sprintf(llvm_bitcode, "%s -o %s.bc -emit-llvm -c %s", LLVM_GCC, fileMC, fileMC); 
				printf("\n.....calling the LLVM gcc with the following command.....\n");
				printf("%s\n", llvm_bitcode);
				system(llvm_bitcode);
				/* now call the KLEE symbolic execution engine on LLVM bitcode */
				sprintf(cmd, "%s %s %s.bc", KLEE, KLEE_OPTIONS, fileMC);
				printf("\n.....calling KLEE with the following command.....\n");
				printf("%s\n", cmd);
			}	else 
				assert(0 && "you are a SATAN.....I don't know this ORACLE");

		  ret = system(cmd);
#ifdef _DEBUG_AI_MC
		  fprintf(stdout, "\n-----------------------------------------------------------------------------------------\n");
		  fprintf(stdout, "MC refinement finishes for instruction 0x%x in task %s..........\n", \
					 addr, task->name);
		  fprintf(stdout, "-----------------------------------------------------------------------------------------\n");
#endif

		  return ret;
}

/* modify a task source which would be passed to model checker and 
 * be used to refine the WCET */
void modify_task_for_WCET_improvement(task_p task, char* fileAI, FILE* fpAI, FILE* fpMC, 
		  addr_t addr, char residual_age)
{
		  static int refline = 0;
		  int i,j;
		  char line[MAX_LINE_LEN];
		  char code[MAX_FILENAME_LEN];
			char assertClause[MAX_FILENAME_LEN];
		  addr_t* addr_link = NULL;
		  int num_addrs = 0;
		  int count_line = 1;
		  int* conflict_lines = NULL;

		  /* collect list of addresses from "peer" that has conflicts with 
			* the address "addr" */
		  addr_link = collect_conflicting_blocks_L1(task, addr, &num_addrs);

		  conflict_lines = (int *) calloc(num_addrs, sizeof(int));
		  CHECK_MEM(conflict_lines);

		  refline = get_line_number(fileAI, addr);

		  for (i = 0; i < num_addrs; i++)
					 conflict_lines[i] = get_line_number(fileAI, addr_link[i]);
			
			/* include KLEE header in case KLEE is chosen for the refinement */
			if (oracle_type == TYPE_SYMBOLIC_EXECUTION) {
				sprintf(code, "%s\n", KLEE_INCLUDE);
				MODIFY_MC_FILE(fpMC, code);
			}

		  /* print all relevant meta declarations at the beginning */
		  sprintf(code, "/* __MC__ */ int C__%d=0;\n", GET_SET_L1(addr));
					 
		  MODIFY_MC_FILE(fpMC, code);
		  /* print conflicting flag declarations */
		  for (i = 0; i < num_addrs; i++)
		  {
					 sprintf(code, "/* __MC__ */ char flag__0x%x=0;\n", addr_link[i]);
					 MODIFY_MC_FILE(fpMC, code);
		  }
		  /* define the counting macro at the beginning of the file*/
		  /* this code will be the preprocessed whenever conflict block 
			* counting is required */
		  MODIFY_MC_FILE(fpMC, COUNT_CODE);
		  MODIFY_MC_FILE(fpMC, COUNT_MACRO);

			/* decide the assertion type */			
			if (oracle_type == TYPE_MODEL_CHECK) 
				strcpy(assertClause, CPROVER_ASSERT);	
			else if (oracle_type == TYPE_SYMBOLIC_EXECUTION)
				strcpy (assertClause, KLEE_ASSERT);
			else 
				assert(0 && "you are a SATAN.....I don't know this ORACLE");

		  while (fgets(line, MAX_LINE_LEN, fpAI)) 
		  {
					 if (count_line == refline) {
								if (oracle_type == TYPE_MODEL_CHECK)
										sprintf(code, "/* __MC__ */%s(C__%d <= %d, \"conflict check\");\n",\
												assertClause, GET_SET_L1(addr), residual_age);
								else if (oracle_type == TYPE_SYMBOLIC_EXECUTION)
										sprintf(code, "/* __MC__ */%s(C__%d <= %d);\n",\
												assertClause, GET_SET_L1(addr), residual_age);
								else
										assert(0 && "you are a SATAN.....I don't know this ORACLE");		
												
								MODIFY_MC_FILE(fpMC, code);
								sprintf(code, "/* __MC__ */ C__%d=0;\n", GET_SET_L1(addr));
								MODIFY_MC_FILE(fpMC, code);
								for (i = 0; i < num_addrs; i++)
								{
										  sprintf(code, "/* __MC__ */ flag__0x%x=0;\n", addr_link[i]);
										  MODIFY_MC_FILE(fpMC, code);
								}
					 }
					 /* check conflicting block accesses */
					 for (i = 0; i < num_addrs; i++) {
								if (count_line == conflict_lines[i] && addr != addr_link[i]) {
										  sprintf(code, "/* __MC__ */__COUNT_MACRO(flag__0x%x,C__%d);\n", \
													 addr_link[i], GET_SET_L1(addr));
										  MODIFY_MC_FILE(fpMC, code);
								} 
								#if 0
										  else if (addr == addr_link[i] && count_line != refline) {
										  sprintf(code, "/* __MC__ */ C__%d=0;\n", GET_SET_L1(addr));
										  MODIFY_MC_FILE(fpMC, code);
										  for (j = 0; j < num_addrs; j++)
										  {
													 sprintf(code, "/* __MC__ */ flag__0x%x=0;\n", addr_link[j]);
													 MODIFY_MC_FILE(fpMC, code);
										  }
								}
								#endif
					 }
					 fputs(line, fpMC);
					 count_line++;
		  }

		  /* free conflict set */
		  if(addr_link)
					 free(addr_link);
		  if(conflict_lines)
					 free(conflict_lines);
		  
}

/* modify source code of the peer -- some conflicting task from 
 * a different core */
void modify_peer(task_p peer, char* fileAI, FILE* fpAI, FILE* fpMC, 
		  addr_t addr, char residual_age, int isL2)
{
		  static int refline = 0;
		  int i;
		  char line[MAX_LINE_LEN];
		  char code[MAX_FILENAME_LEN];
			char assertClause[MAX_FILENAME_LEN];
		  addr_t* addr_link = NULL;
		  int num_addrs = 0;
		  int count_line = 1;
		  int* conflict_lines = NULL;
		  int exit_id = peer->topo_tcfg[peer->num_tcfg_nodes - 1];
		  tcfg_node_t* exit_node = peer->tcfg[exit_id];
		  de_inst_t* last_inst = exit_node->bb->code + exit_node->bb->num_inst - 1;
		  
		  /* get the penultimate address */
		  if (!refline)
					 refline = get_line_number(fileAI, last_inst->addr);

		  /* collect list of addresses from "peer" that has conflicts with 
			* the address "addr" */
		  if(isL2)
					 addr_link = collect_conflicting_blocks_L2(peer, addr, &num_addrs);
		  else
					 addr_link = collect_conflicting_blocks_L1(peer, addr, &num_addrs);

		  conflict_lines = (int *) calloc(num_addrs, sizeof(int));
		  CHECK_MEM(conflict_lines);

		  for (i = 0; i < num_addrs; i++)
					 conflict_lines[i] = get_line_number(fileAI, addr_link[i]);

			/* include KLEE header in case KLEE is chosen for the refinement */
			if (oracle_type == TYPE_SYMBOLIC_EXECUTION) {
				sprintf(code, "%s\n", KLEE_INCLUDE);
				MODIFY_MC_FILE(fpMC, code);
			}	

		  /* print all relevant meta declarations at the beginning */
		  if(isL2)
					 sprintf(code, "/* __MC__ */ int C__%d=0;\n", GET_SET_L2(addr));
		  else
					 sprintf(code, "/* __MC__ */ int C__%d=0;\n", GET_SET_L1(addr));
					 
		  MODIFY_MC_FILE(fpMC, code);
		  /* print conflicting flag declarations */
		  for (i = 0; i < num_addrs; i++)
		  {
					 sprintf(code, "/* __MC__ */ char flag__0x%x=0;\n", addr_link[i]);
					 MODIFY_MC_FILE(fpMC, code);
		  }
		  /* define the counting macro at the beginning of the file*/
		  /* this code will be the preprocessed whenever conflict block 
			* counting is required */
		  MODIFY_MC_FILE(fpMC, COUNT_CODE);
		  MODIFY_MC_FILE(fpMC, COUNT_MACRO);
			
			/* decide the assertion type */			
			if (oracle_type == TYPE_MODEL_CHECK) 
				strcpy(assertClause, CPROVER_ASSERT);	
			else if (oracle_type == TYPE_SYMBOLIC_EXECUTION)
				strcpy (assertClause, KLEE_ASSERT);
			else 
				assert(0 && "you are a SATAN.....I don't know this ORACLE");

		  while (fgets(line, MAX_LINE_LEN, fpAI)) 
		  {
					 if (count_line == refline) {
								if(isL2) {
											if (oracle_type == TYPE_MODEL_CHECK)	
												sprintf(code, "/* __MC__ */%s(C__%d <= %d, \"conflict check\");\n",\
													 assertClause, GET_SET_L2(addr), residual_age);
											else		 
												sprintf(code, "/* __MC__ */%s(C__%d <= %d);\n",\
													 assertClause, GET_SET_L2(addr), residual_age);
								}					 
								else {
											if (oracle_type == TYPE_MODEL_CHECK)	
												sprintf(code, "/* __MC__ */%s(C__%d <= %d, \"conflict check\");\n",\
													 assertClause, GET_SET_L1(addr), residual_age);
											else
												sprintf(code, "/* __MC__ */%s(C__%d <= %d);\n",\
													 assertClause, GET_SET_L1(addr), residual_age);
								}					 
										  
								MODIFY_MC_FILE(fpMC, code);
					 }
					 /* check conflicting block accesses */
					 for (i = 0; i < num_addrs; i++) {
								if (count_line == conflict_lines[i]) {
										  if(isL2)
													 sprintf(code, "/* __MC__ */__COUNT_MACRO(flag__0x%x,C__%d);\n", \
																addr_link[i], GET_SET_L2(addr));
										  else
													 sprintf(code, "/* __MC__ */__COUNT_MACRO(flag__0x%x,C__%d);\n", \
																addr_link[i], GET_SET_L1(addr));
										  MODIFY_MC_FILE(fpMC, code);
								}
					 }
					 fputs(line, fpMC);
					 count_line++;
		  }

		  /* free conflict set */
		  if(addr_link)
					 free(addr_link);
		  if(conflict_lines)
					 free(conflict_lines);
}

/* try MC refinement of one ECB (evicted cache block) to improve the CRPD */
int try_ECB_refinement_with_MC(task_p task, int mem_blk_id, int try_age)
{
		  FILE* fpr;
		  FILE* fpw;
		  char fileAI[MAX_FILENAME_LEN];
		  char fileMC[MAX_FILENAME_LEN];
		  int i;
		  int ret = 1;
		
#ifdef _DEBUG_AI_MC
		  fprintf(stdout, "\n-----------------------------------------------------------------------------------------\n");
		  fprintf(stdout, "Trying refinement of ECB 0x%x in task %s..........\n", mem_blk_id, task->name);
		  fprintf(stdout, "-----------------------------------------------------------------------------------------\n");
#endif
		  
		  /* searched cached result */
		  ret = search_cached_mc_result(mem_blk_id * bsize, try_age, 0);
		  
		  if(ret != -1) {
		  #ifdef _DEBUG_AI_MC
					 fprintf(stdout, "cached result found for ECB 0x%x in task %s\n", mem_blk_id, task->name);
		  #endif
					 return ret;
		  }
		  sprintf(fileAI, "%s.c", task->name);
		  sprintf(fileMC, "%s%s.c", task->name, MCFILE_EXT);

		  fpr = fopen(fileAI, "r");
		  if (!fpr) {
					 fprintf(stdout, "source file cannot be opened.....exiting\n");
					 exit(-1);
		  }
		  sprintf(fileAI, "%s", task->name);
		  fpw = fopen(fileMC, "w");
		  if (!fpw) {
					 fprintf(stdout, "MC file cannot be opened.....exiting\n");
					 exit(-1);
		  }
		  modify_peer(task, fileAI, fpr, fpw, mem_blk_id * bsize, try_age, 0);
		  fclose(fpw);
		  fclose(fpr);
		  ret = run_MC(task, mem_blk_id * bsize, fileMC);
		  
		  return ret;
}

/* try MC refinement of instruction "inst" by code transformation in single core */
int try_refinement_with_MC_single_core(task_p task, de_inst_t* inst)
{
		  FILE* fpr;
		  FILE* fpw;
		  char fileAI[MAX_FILENAME_LEN];
		  char fileMC[MAX_FILENAME_LEN];
		  int i;
		  int ret = 1;
		  
			/* sudiptac : removing this, handled inside the caller */
		  /* if(number_of_mc_calls > MC_CALL_BOUND) {
					 fprintf(stdout, "Exceeded the limit of MC calls (%d)......returning\n", MC_CALL_BOUND);
					 return -1;
		  } */
		  
		  /* searched cached result */
		  ret = search_cached_mc_result(inst->addr, assoc-1, 0);

		  if(ret != -1) {
		  #ifdef _DEBUG_AI_MC
					 fprintf(stdout, "\n-----------------------------------------------------------------------------------------\n");
					 fprintf(stdout, "Trying refinement of instruction 0x%x in task %s..........\n", inst->addr, task->name);
					 fprintf(stdout, "-----------------------------------------------------------------------------------------\n");
					 fprintf(stdout, "cached result found for instruction 0x%x in task %s\n", inst->addr, task->name);
		  #endif
					 return ret;
		  }

		  sprintf(fileAI, "%s.c", task->name);
		  sprintf(fileMC, "%s%s.c", task->name, MCFILE_EXT);

		  fpr = fopen(fileAI, "r");
		  if (!fpr) {
					 fprintf(stdout, "source file cannot be opened.....exiting\n");
					 exit(-1);
		  }
		  sprintf(fileAI, "%s", task->name);
		  fpw = fopen(fileMC, "w");
		  if (!fpw) {
					 fprintf(stdout, "MC file cannot be opened.....exiting\n");
					 exit(-1);
		  }
		  modify_task_for_WCET_improvement(task, fileAI, fpr, fpw, inst->addr, assoc - 1);
		  fclose(fpw);
		  fclose(fpr);
		  ret = run_MC(task, inst->addr, fileMC);

		  return ret;
}

/* try MC refinement of instruction "inst" by code transformation */
int try_refinement_with_MC(task_p task, de_inst_t* inst, char residual_age)
{
		  FILE* fpr;
		  FILE* fpw;
		  char fileAI[MAX_FILENAME_LEN];
		  char fileMC[MAX_FILENAME_LEN];
		  task_p peer;
		  int i;
		  int ret = 1;
		  
		  /* searched cached result */
		  ret = search_cached_mc_result(inst->addr, residual_age, 1);

		  if(ret != -1) {
		  #ifdef _DEBUG_AI_MC
					 fprintf(stdout, "\n-----------------------------------------------------------------------------------------\n");
					 fprintf(stdout, "Trying refinement of instruction 0x%x in task %s..........\n", inst->addr, task->name);
					 fprintf(stdout, "-----------------------------------------------------------------------------------------\n");
					 fprintf(stdout, "cached result found for instruction 0x%x in task %s\n", inst->addr, task->name);
		  #endif
					 return ret;
		  }

		  for (i = 0; i < ntasks; i++)
		  {
					 if (task == tasks[i])
								continue;
					 if (tasks[i]->ncore == task->ncore || depends(task, tasks[i]))
								continue;

					 peer = tasks[i];

					 sprintf(fileAI, "%s.c", peer->name);
					 sprintf(fileMC, "%s%s.c", peer->name, MCFILE_EXT);

					 fpr = fopen(fileAI, "r");
					 if (!fpr) {
								fprintf(stdout, "source file cannot be opened.....exiting\n");
								exit(-1);
					 }
					 sprintf(fileAI, "%s", peer->name);
					 fpw = fopen(fileMC, "w");
					 if (!fpw) {
								fprintf(stdout, "MC file cannot be opened.....exiting\n");
								exit(-1);
					 }
		   		 modify_peer(peer, fileAI, fpr, fpw, inst->addr, residual_age, 1);
					 fclose(fpw);
					 fclose(fpr);
					 ret = run_MC(task, inst->addr, fileMC);
		  }

		  return ret;
}

/* use model checking to refine the orginal single core cache analysis */
void refine_cache_analysis_with_MC_WCET(task_p task)
{
		  int i, j, ret;
		  tcfg_node_t* bbi;
		  de_inst_t* inst;
			int mc_calls = 0;
			ticks start_est, end_est, prev_start = 0, prev_end = 0;

		  for (i = 0; i < task->num_tcfg_nodes; i++)
		  {
					 bbi = task->tcfg[i];
					 /* we currently ignore refinement of cache 
					  * accesses outside any loop reason ::: not 
					  * much gain for the effort we need to pay */
					 if (task->loop_map[bbi->id] == task->loops[0])
								continue;
					 inst = bbi->bb->code;
					 for ( j = 0; j < bbi->bb->num_inst; j++)
					 {
								/* try refinement with model checking for the non 
								 * classified instructions by AI */
								if(task->inst_chmc_l1[i][j] == NOT_CLASSIFIED) {
										  num_unclassified++;
											/* dude, I have had enough of this oracle refinements.....*/
											if (mc_calls >= refinement_bound) 
												continue;
										  ret = try_refinement_with_MC_single_core(task, inst);
										  if(!ret) {
#ifdef _DEBUG_AI_MC		
													 fprintf(stdout, "MC refinement successful in task %s instruction 0x%x\n", \
																task->name, inst->addr);
#endif
													 /* refine CHMC */
													 task->inst_chmc_l1[i][j] = ALL_HIT;
													 number_of_refinements++;
										  } else {
#ifdef _DEBUG_AI_MC
													 fprintf(stdout, "NO MC refinement possible in task %s instruction 0x%x\n", \
																task->name, inst->addr);
#endif
													 
										  }
										  /* cache_mc_result(inst->addr, ret, task->residual_age_l2[i][j], 1);*/
											mc_calls++;

											/* record the <time,improvement> pair here */
											end = getticks();
											/* exclude the estimation time */
											start_est = getticks();
											set_task(tasks[0]);
											tasks[0]->wcet = run_est(tasks[0]->name);
											/* reset vaariable count */
											curr_grp = 0;
											end_est = getticks();
											fprintf(LogPfp, "%.2lf    %.2lf\n", (end - start + prev_start - prev_end)/((1.0) * CPU_MHZ), \
												(100 * (old_wcet - tasks[0]->wcet))/((1.0) * old_wcet));	
											prev_start += start_est;
											prev_end += end_est;
								}
								inst++;
					 }
		  }
}

/* use model checking to refine the cache analysis result of "task" */
void refine_cache_analysis_with_MC_of_one(task_p task)
{
		  int i, j, ret;
		  tcfg_node_t* bbi;
		  de_inst_t* inst;
			ticks start_est, end_est, prev_start = 0, prev_end = 0;

		  for (i = 0; i < task->num_tcfg_nodes; i++)
		  {
					 bbi = task->tcfg[i];
					 /* we currently ignore refinement of cache 
					  * accesses outside any loop reason ::: not 
					  * much gain for the effort we need to pay */
					 if (task->loop_map[bbi->id] == task->loops[0])
								continue;
					 inst = bbi->bb->code;
					 for ( j = 0; j < bbi->bb->num_inst; j++)
					 {
								/* try refinement with model checking for the non 
								 * classified instructions by AI */
								if(task->inst_chmc_l2[i][j] == NOT_CLASSIFIED && task->residual_age_l2[i][j] >= 0) {
										  ret = try_refinement_with_MC(task, inst, task->residual_age_l2[i][j]);
										  if(!ret) {
#ifdef _DEBUG_AI_MC		
													 fprintf(stdout, "MC refinement successful in task %s instruction 0x%x\n", \
																task->name, inst->addr);
#endif
													 /* refine CHMC */
													 task->inst_chmc_l2[i][j] = ALL_HIT;
													 number_of_refinements++;
										  } else {
#ifdef _DEBUG_AI_MC
													 fprintf(stdout, "NO MC refinement possible in task %s instruction 0x%x\n", \
																task->name, inst->addr);
#endif
													 
										  }
										  cache_mc_result(inst->addr, ret, task->residual_age_l2[i][j], 1);
											
											/* record the <time,improvement> pair here */
											end = getticks();
											/* exclude the estimation time */
											start_est = getticks();
											set_task(tasks[0]);
											tasks[0]->wcet = run_est(tasks[0]->name);
											/* reset variable count */
											curr_grp = 0;
											end_est = getticks();
											fprintf(LogPfp, "%.2lf    %.2lf\n", (end - start + prev_start - prev_end)/((1.0) * CPU_MHZ), \
												(100 * (old_wcet - tasks[0]->wcet))/((1.0) * old_wcet));	
											prev_start += start_est;
											prev_end += end_est;
								}
								inst++;
					 }
		  }
}

/* refinement of cache analysis with model checking */
void refine_cache_analysis_with_MC()
{
		  int i;
			
			/* oracle_type = TYPE_SYMBOLIC_EXECUTION;
			oracle_type = TYPE_MODEL_CHECK; */

			/* open the log file for writing */
			LogPfp = fopen("time_varying_progress.log", "w");
			if (!LogPfp) {
				fprintf(stdout, "file opening failed for writing\n");
				exit(-1);
			}	
											
			start = getticks();
		  
			old_wcet = tasks[0]->wcet;

		  /* single core WCET analysis refinement */
		  if (ntasks == 1) {
					 refine_cache_analysis_with_MC_WCET(tasks[0]);
		  } else {
					 for (i = 0; i < 1; i++)
					 {
								refine_cache_analysis_with_MC_of_one(tasks[i]);
					 }
		  }

		  /* run estimation after MC refinement */
		  set_task(tasks[0]);
		  tasks[0]->wcet = run_est(tasks[0]->name);
#ifdef _DEBUG_AI_MC
		  fprintf(stdout, "\n-----------------------------------------------------------------------------------------\n");
		  fprintf(stdout, "WCET with cache analysis only = %d\n", old_wcet);
		  fprintf(stdout, "-----------------------------------------------------------------------------------------\n");
		  /* for single core WCET analysis */
		  if (ntasks == 1) {
					 fprintf(stdout, "-----------------------------------------------------------------------------------------\n");
					 fprintf(stdout, "Number of NC classified instructions inside loop = %d\n", num_unclassified);
					 fprintf(stdout, "Number of CHMCs refined = %d\n", number_of_refinements);
					 fprintf(stdout, "WCET after MC refinement = %d\n", tasks[0]->wcet);
					 fprintf(stdout, "-----------------------------------------------------------------------------------------\n");
		  }
		  else {
					 fprintf(stdout, "-----------------------------------------------------------------------------------------\n");
					 fprintf(stdout, "Number of CHMCs changed after conflict analysis = %d\n", tasks[0]->chmc_changed);
					 fprintf(stdout, "Number of CHMCs refined = %d\n", number_of_refinements);
					 fprintf(stdout, "WCET after MC refinement = %d\n", tasks[0]->wcet);
					 fprintf(stdout, "-----------------------------------------------------------------------------------------\n");
		  }
#endif

			fclose(LogPfp);	

		  return;
}

