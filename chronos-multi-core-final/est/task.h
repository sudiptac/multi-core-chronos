#ifndef __TASK_H_
#define __TASK_H_
#include "tcfg.h"
#include "loops.h"

#define MAX_TASKNAME_LEN 256
/* global variables related to multi-tasking application */
int ntasks;
/* Define the task data structure....this is needed for SPM 
 * allocation methods reducing WCRT of an application */
struct task {
		  char* name;					/* task name */
		  int priority;				/* priority */
		  int ncore;					/* assigned core no. */
		  int tid;						/* task id */
		  int mscid;					/* Message sequence chart id....for compatibility 
											 * with the WCRT module */	  
		  unsigned int start_time_low;		/* earliest starting time of the task */
		  unsigned int start_time_high;		/* latest starting time of the task */
		  int	num_tcfg_nodes;					/* number of tcfg nodes */
		  int num_tcfg_edges;					/* number of tcfg edges */
		  tcfg_node_t** tcfg;					/* TCFG */
		  int* topo_tcfg;							/* topologically ordered TCFG ids */
		  tcfg_edge_t** tcfg_edges;			/* TCFG edges */
		  tcfg_nlink_t*** bbi_map;				/* mapping between CFG node to TCFG nodes */
		  int	num_tcfg_loops;					/* total number of loops */
		  loop_t**	loops;						/* all loop structure */
		  loop_t** 	loop_map; 					/* mapping of TCFG node to nesting loop */
		  loop_t***	loop_comm_ances;			/* common ancestor of any two loops */
		  int nsucc;								/* number of succssors */
		  int* succ;								/* list of successors */
		  char**	inst_chmc_l1;					/* CHMC for L1 cache */
		  char**	inst_chmc_l2;					/* CHMC for L2 cache */
		  char**	inst_age_l1;					/* "must" age of each instruction in L1 cache */
		  char**	inst_age_l2;					/* "must" age of each instruction in L2 cache */
		  char** residual_age_l2;				/* residual "must" age before inter-core conflicts */
		  char* ecb_log_l1;						/* evicted cache blocks of L1 */
		  char* ecb_log_l2;						/* evicted cache blocks of L2 */
		  int	wcet;									/* worst case execution time of the task */
		  int chmc_changed;						/* changed CHMCs after conflict analysis */
};
typedef struct task task_s;
typedef struct task* task_p;
task_p* tasks;
int successor(task_p task1, task_p task2);
int depends(task_p task1, task_p task2);
#endif
