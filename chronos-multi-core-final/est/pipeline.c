/*******************************************************************************
 *
 * Chronos: A Timing Analyzer for Embedded Software
 * =============================================================================
 * http://www.comp.nus.edu.sg/~rpembed/chronos/
 *
 * Copyright (C) 2005 Xianfeng Li
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * $Id: pipeline.c,v 1.4 2006/07/23 09:20:46 lixianfe Exp $
 *
 * traverse t-cfg to formulate execution paths; and estimate their execution
 * timing bounds
 *
 ******************************************************************************/


#include <string.h>
#include <stdlib.h>
#include "common.h"
#include "cfg.h"
#include "cache.h"
#include "bpred.h"
#include "loops.h"
#include "pipeline.h"
#include "busconfig.h"
/* offset tracking */
#include "offsetData.h"
#include "offsetGraph.h"
// Include local library headers
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <debugmacros/debugmacros.h>

/* ############################################################
 * #### Local data type definitions (will not be exported) ####
 * ############################################################ */


/* Represents the result of the combined BCET/WCET/offset analysis */
typedef struct {
  offset_data* offsets;
  ull bcet;
  ull wcet;
} combined_result;

/**** externs ****/

extern tcfg_node_t  **tcfg;
extern int	    num_tcfg_nodes;
extern tcfg_edge_t  **tcfg_edges;
extern int	    num_tcfg_edges;
extern cache_t	    cache;
extern cache_t	    cache_l2;
extern int	    bpred_scheme;
extern int	    enable_icache;
extern int	    enable_il2cache;
extern char	    ***hit_miss;
extern int	    *num_hit_miss;
extern loop_t	    ***bbi_hm_list;
extern int	    *num_bbi_hm_list;
extern de_inst_t    ***mp_insts;
extern int	    *num_mp_insts;
extern int	    num_eg_insts;
extern int	    **num_mblk_conflicts;
extern loop_t	    **loops;
extern loop_t	    **loop_map;		// bbi => loop
extern int	    num_tcfg_loops;
extern mem_blk_t    **mp_gen;
extern egraph_node_t **egraph;
extern int 		plog_len;
extern int		body_len;

/**** globals and static ****/
int		pipe_stages = 5;
int		pipe_ibuf_size;
int		pipe_iwin_size;
int		prolog_size, pipe_iwin_size;
code_link_t	**prologs, **epilogs;
mas_inst_t	**bodies, *start_body;
int		**cpred_times, **mpred_times, start_time;
static enum OffsetDataType currentOffsetRepresentation = OFFSET_DATA_TYPE_SET;
static struct offset_link*** all_offsets_in = NULL;
static struct offset_link*** all_offsets_out = NULL;
static _Bool haveExplicitTime;
static void WCET_Loop(int edge_id);

// temporary variables
// to indicate for each bbi whether it contains long latency instr
int		*mlat_bbi;
// to indicate for each edge, the shortest distance of producer to any long
// latency instr in edge->dst or its predecessors
int		*max_elog_len;
// the first multi-cycle latency instr along each mispred path
int		*mlat_mpinst;
// if est unit's source is BP_CPRED, the number of  mispred instr to be
// truncated at the end of the prolog
int		*num_plog_trunc;
mblk_tag_t	*tmp_cs;
/* sudiptac :::: for level 2 cache */
mblk_tag_t	*tmp_cs_l2;
int		num_plogs, num_elogs;

// if a bbi has mult-latency instructions, then set its corresponding entry
// to 1, such bbi has direct contribution to late contentions
		  static void
set_mlat_bbi()
{
		  int		bbi_id, i;
		  cfg_node_t	*bb;
		  de_inst_t	*inst;

		  mlat_bbi = (int *) calloc(num_tcfg_nodes, sizeof(int));
		  for (bbi_id = 0; bbi_id < num_tcfg_nodes; bbi_id++) {
					 bb = tcfg[bbi_id]->bb;
					 for (i = bb->num_inst - 1; i >= 0; i--) {
								if (max_inst_lat(&bb->code[i]) > 1)
										  break;
					 }
					 mlat_bbi[bbi_id] = i;
		  }
}


#define MAX_REGS    256
int		    dep_regs[MAX_REGS];

		  static void
lookup_mlat(int orig, int dist, tcfg_node_t *bbi, int no_mlat)
{
		  int		    i, k, tmp, r;
		  de_inst_t	    *inst;
		  tcfg_edge_t	    *e;

		  if (no_mlat && (mlat_bbi[bbi->id] < 0)) {
					 dist += bbi->bb->num_inst;
					 if ((bbi->in == NULL) || (dist >= pipe_iwin_size))
								return;
					 for (e = bbi->in; e != NULL; e = e->next_in)
								lookup_mlat(orig, dist, e->src, 1);
					 return;
		  }

		  for (i = bbi->bb->num_inst - 1; i >= 0; i--) {
					 inst = &bbi->bb->code[i];
					 for (k = 0; k < inst->num_out; k++) {
								r = inst->out[k];
								if (dep_regs[r] == orig) {
										  tmp = pipe_iwin_size - (dist + (bbi->bb->num_inst - i));
										  max_elog_len[orig] = max(tmp, max_elog_len[orig]);
										  return;
								}
					 }
					 if (max_inst_lat(inst) > 1) {
								for (k = 0; k < inst->num_in; k++) {
										  r = inst->in[k];
										  dep_regs[r] = orig;
								}
					 }
		  }

		  dist += bbi->bb->num_inst;
		  if (dist < pipe_iwin_size) {
					 for (e = bbi->in; e != NULL; e = e->next_in)
								lookup_mlat(orig, dist, e->src, 0);
		  }
}



		  static void
bound_elog_len()
{
		  int		    i;
		  tcfg_edge_t	    *e;

		  max_elog_len = (int *) malloc(num_tcfg_nodes * sizeof(int));
		  for (i = 0; i < MAX_REGS; i++)
					 dep_regs[i] = -1;

		  max_elog_len[0] = 0;
		  if (mlat_bbi[0] >= 0)
					 max_elog_len[0] = pipe_iwin_size - (tcfg[0]->bb->num_inst - mlat_bbi[0]);

		  for (i = 1; i < num_tcfg_nodes; i++) {
					 max_elog_len[i] = 0;
					 lookup_mlat(i, 0, tcfg[i], 1);
		  }
}



		  static void
find_mlat_mpinst()
{
		  int		    i, k;
		  tcfg_edge_t	    *e;

		  mlat_mpinst = (int *) calloc(num_tcfg_edges, sizeof(int));
		  num_plog_trunc = (int *) calloc(num_tcfg_edges, sizeof(int));

		  for (i = 0; i < num_tcfg_edges; i++) {
					 for (k = 0; k < num_mp_insts[i]; k++) {
								if (max_inst_lat(mp_insts[i][k]) > 1) {
										  mlat_mpinst[i] = k + 1;
										  break;
								}
					 }
		  }
}



// create a prologue from the reverse of path; and for the first node, skip the
// first num_skip instructions
		  static void
new_prolog(int log_set, tcfg_edge_t **path, int path_len, int num_skip)
{
		  int		i, k, num = 0, num_mp = 0, tag, set, edge_id, max_num_mp, tag_l2, set_l2;
		  tcfg_node_t	*bbi, *mbbi;
		  Queue	queue;
		  code_link_t	*plog;
		  mas_inst_t	mas_inst;
		  addr_t	sa;

		  if (num_skip < 0)
					 num_skip = 0;

		  memset(tmp_cs, 0, cache.ns * sizeof(mblk_tag_t));

		  /* sudiptac :::: for level 2 cache analysis */
		  if(enable_il2cache) {
					 memset(tmp_cs_l2, 0, cache_l2.ns * sizeof(mblk_tag_t));
		  }
		  
		  init_queue(&queue, sizeof(mas_inst_t));

		  for (i = path_len-1; i >= 0; i--) {
					 bbi = path[i]->src;
					 sa = bbi->bb->sa;
					 // copy code of e->src into prologue
					 for (k = num_skip; k < bbi->bb->num_inst; k++) {
								// bpred related
								mas_inst.inst = &bbi->bb->code[k];
								mas_inst.bbi_id = bbi->id;
								mas_inst.mblk_id = MBLK_ID(sa, mas_inst.inst->addr);
								/* sudiptac ::: for level 2 cache analysis */
								mas_inst.mblk_id_l2 = MBLK_ID_L2(sa, mas_inst.inst->addr);
								mas_inst.bp_flag = BP_CPRED;
								// cache related
								tag = TAG(mas_inst.inst->addr);
								set = SET(mas_inst.inst->addr);
								tag_l2 = TAG_L2(mas_inst.inst->addr);
								set_l2 = SET_L2(mas_inst.inst->addr);
								// sudiptac ::: shared bus related
								mas_inst.offset_in = NULL;
								mas_inst.offset_out = NULL;

								if (tmp_cs[set].valid == 0)
										  mas_inst.ic_flag = IC_UNCLEAR;
								else if (tmp_cs[set].tag == tag)
										  mas_inst.ic_flag = IC_HIT;
								else
										  mas_inst.ic_flag = IC_MISS;
								tmp_cs[set].valid = 1;
								tmp_cs[set].tag = tag;
								
								/* sudiptac :::: for L2 cache analysis */
								if(enable_il2cache) {
										  if (tmp_cs_l2[set_l2].valid == 0)
													 mas_inst.ic_flag_l2 = IC_UNCLEAR;
										  else if (tmp_cs_l2[set_l2].tag == tag_l2)
													 mas_inst.ic_flag_l2 = IC_HIT;
										  else
													 mas_inst.ic_flag_l2 = IC_MISS;
										  tmp_cs_l2[set_l2].valid = 1;
										  tmp_cs_l2[set_l2].tag = tag_l2;
								}

								enqueue(&queue, &mas_inst);
								num++;
					 }

					 num_skip = 0;

					 if ((bpred_scheme == NO_BPRED) || !cond_bbi(bbi))
								continue;

					 // mispred instructions
					 edge_id = path[i]->id;
					 if (mlat_mpinst[edge_id] > max_elog_len[bbi->id])
								continue;

					 max_num_mp = min(num_mp_insts[edge_id], max_elog_len[bbi->id]);

					 if (bbi->out == path[i])
								mbbi = bbi->out->next_out->dst;
					 else
								mbbi = bbi->out->dst;

					 num_mp = 0;

					 for (k = 0; k < max_num_mp; k++) {
								if (num_mp >= mbbi->bb->num_inst) {
										  mbbi = mbbi->out->dst;
										  num_mp = 0;
								}
								mas_inst.inst = mp_insts[edge_id][k];
								mas_inst.bbi_id = mbbi->id;
								mas_inst.mblk_id = MBLK_ID(mbbi->bb->sa, mas_inst.inst->addr);
								/* sudiptac ::: for level 2 cache analysis */
								mas_inst.mblk_id_l2 = MBLK_ID_L2(sa, mas_inst.inst->addr);

								if (i == 0)
										  mas_inst.bp_flag = BP_MPRED;
								else
										  mas_inst.bp_flag = BP_UNCLEAR;

								tag = TAG(mas_inst.inst->addr);
								set = SET(mas_inst.inst->addr);
								/* sudiptac :::: for level 2 cache analysis */
								tag_l2 = TAG_L2(mas_inst.inst->addr);
								set_l2 = SET_L2(mas_inst.inst->addr);

								if (tmp_cs[set].valid == 0)
										  mas_inst.ic_flag = IC_UNCLEAR;
								else if (tmp_cs[set].tag == tag)
										  mas_inst.ic_flag = IC_HIT;
								else {
										  mas_inst.ic_flag = IC_MISS;
										  tmp_cs[set].valid = 1;
								}

								/* sudiptac :::: for level 2 cache analysis */
								if (enable_il2cache) {
										  if (tmp_cs_l2[set_l2].valid == 0)
													 mas_inst.ic_flag_l2 = IC_UNCLEAR;
										  else if (tmp_cs_l2[set_l2].tag == tag_l2)
													 mas_inst.ic_flag_l2 = IC_HIT;
										  else {
													 mas_inst.ic_flag_l2 = IC_MISS;
													 tmp_cs_l2[set_l2].valid = 1;
										  }
								}
								
								// sudiptac ::: shared bus related
								mas_inst.offset_in = NULL;
								mas_inst.offset_out = NULL;

								enqueue(&queue, &mas_inst);
								num++;
								num_mp++;
					 }
					 if (i == 0)
								num_plog_trunc[log_set] = max_num_mp;
		  }

		  /* copy code from temporary storage to the new prologue node */
		  plog = (code_link_t *) calloc(1, sizeof(code_link_t));
		  CHECK_MEM(plog);
		  plog->code = (mas_inst_t *) calloc(num, sizeof(mas_inst_t));
		  CHECK_MEM(plog->code);
		  for (k = 0; k < num; k++)
					 plog->code[k] = *((mas_inst_t *) dequeue(&queue));
		  plog->num_inst = num;
		  plog->next = prologs[log_set];
		  prologs[log_set] = plog;

		  free_queue(&queue);
}



		  static void
trav_backward(int log_set, tcfg_edge_t **path, int path_len, int code_len)
{
		  tcfg_node_t	*bbi;
		  tcfg_edge_t	*e;
		  int		i;

		  bbi = path[path_len-1]->src;
		  code_len += bbi->bb->num_inst;
		  if ((code_len >= prolog_size) || (bbi->in == NULL))
					 new_prolog(log_set, path, path_len, code_len - prolog_size);
		  else {
					 for (e = bbi->in; e != NULL; e = e->next_in) {
								path[path_len] = e;
								trav_backward(log_set, path, path_len+1, code_len);
					 }
		  }
}



// collect the set of prologue code for each edge e, where e->src is the last
// block of the prologue code
		  static void
collect_prologs()
{
		  tcfg_edge_t	**path;
		  int		i;

		  path = (tcfg_edge_t **) calloc(prolog_size, sizeof(tcfg_edge_t *));
		  CHECK_MEM(path);
		  prologs = (code_link_t **) calloc(num_tcfg_edges, sizeof(code_link_t *));
		  CHECK_MEM(prologs);

		  for (i = 0; i < num_tcfg_edges; i++) {
					 path[0] = tcfg_edges[i];
					 trav_backward(i, path, 1, 0);
		  }

		  free(path);
}



// create an epilogue from the path
		  static int
new_epilog(int log_set, tcfg_edge_t **path, int path_len)
{
		  int		i, k, num = 0, num1 = 0, m = 0, max_len, hit_mlat, set, tag, set_l2, tag_l2;
		  tcfg_node_t	*bbi;
		  Queue	queue;
		  code_link_t	*elog;
		  mas_inst_t	mas_inst;
		  addr_t	sa;

		  max_len  = max_elog_len[tcfg_edges[log_set]->src->id];
		  memset(tmp_cs, 0, cache.ns * sizeof(mblk_tag_t));
		  
		  /* sudiptac :::: for level 2 cache */
		  if(enable_il2cache) {
					 memset(tmp_cs_l2, 0, cache_l2.ns * sizeof(mblk_tag_t));
		  }
		  
		  init_queue(&queue, sizeof(de_inst_t));

		  for (i = 0; i < path_len; i++) {
					 hit_mlat = 0;
					 // copy code of e->dst
					 bbi = path[i]->dst;
					 sa = bbi->bb->sa;
					 for (k = 0; (k < bbi->bb->num_inst) && (m < max_len); k++, m++) {
								mas_inst.inst = &bbi->bb->code[k];
								mas_inst.bbi_id = bbi->id;
								mas_inst.mblk_id = MBLK_ID(sa, mas_inst.inst->addr);
								/* sudiptac ::: for level 2 cache analysis */
								mas_inst.mblk_id_l2 = MBLK_ID_L2(sa, mas_inst.inst->addr);
								/* bpred related */
								mas_inst.bp_flag = BP_CPRED;
								/* cache related */
								tag = TAG(mas_inst.inst->addr);
								set = SET(mas_inst.inst->addr);
								/* sudiptac :::: for level 2 cache */
								tag_l2 = TAG_L2(mas_inst.inst->addr);
								set_l2 = SET_L2(mas_inst.inst->addr);

								if (tmp_cs[set].valid == 0) 
										  mas_inst.ic_flag = IC_UNCLEAR;
								else if (tmp_cs[set].tag == tag)
										  mas_inst.ic_flag = IC_HIT;
								else
										  mas_inst.ic_flag = IC_MISS;			
								tmp_cs[set].valid = 1;
								tmp_cs[set].tag = tag;

								/* sudiptac :::: for level 2 cache hierarchy */
								if (enable_il2cache)
								{
										  if(tmp_cs_l2[set_l2].valid == 0)
													 mas_inst.ic_flag_l2 = IC_UNCLEAR;
										  else if (tmp_cs[set_l2].tag == tag_l2)
													 mas_inst.ic_flag_l2 = IC_HIT;
										  else
													 mas_inst.ic_flag_l2 = IC_MISS;
										  tmp_cs_l2[set_l2].valid = 1;
										  tmp_cs_l2[set_l2].tag = tag_l2;
								}

								enqueue(&queue, &mas_inst);
								if  (max_inst_lat(mas_inst.inst) > 1) {
										  num1 = num + k + 1;
										  hit_mlat = 1;
								}
					 }
					 num += k;
		  }

		  if ((num1 == 0) || (hit_mlat == 0)) {
					 free_queue(&queue);
					 return 0;
		  }

		  // copy code from tmporary storage to the new epilogue node
		  elog = (code_link_t *) calloc(1, sizeof(code_link_t));
		  CHECK_MEM(elog);
		  elog->code = (mas_inst_t *) calloc(num1, sizeof(mas_inst_t));
		  CHECK_MEM(elog->code);
		  for (k = 0; k < num1; k++) {
					 elog->code[k] = *((mas_inst_t *) dequeue(&queue));
		  }
		  elog->num_inst = num1;
		  elog->next = epilogs[log_set];
		  epilogs[log_set] = elog;

		  free_queue(&queue);
		  return 1;
}



		  static int
trav_forward(int log_set, tcfg_edge_t **path, int path_len, int code_len)
{
		  tcfg_node_t	*bbi;
		  tcfg_edge_t	*e;
		  int		elog_created = 0, tmp;

		  bbi = path[path_len-1]->dst;
		  code_len += bbi->bb->num_inst;
		  if ((code_len >= max_elog_len[path[0]->src->id]) || (bbi->out == NULL)) {
					 if (mlat_bbi[bbi->id] >= 0)
								elog_created = new_epilog(log_set, path, path_len);
		  } else {
					 for (e = bbi->out; e != NULL; e = e->next_out) {
								path[path_len] = e;
								tmp = trav_forward(log_set, path, path_len+1, code_len);
								elog_created |= tmp;
					 }
					 if ((elog_created == 0) && (mlat_bbi[bbi->id] >= 0))
								elog_created = new_epilog(log_set, path, path_len);
		  }
		  return elog_created;
}



// collect the set of epilogue code for each edge e, where e->dst is the first
// block of the epilogue code
		  static void
collect_epilogs()
{
		  tcfg_edge_t	    **path;
		  int		    i;

		  path = (tcfg_edge_t **) calloc(pipe_iwin_size, sizeof(tcfg_edge_t *));
		  CHECK_MEM(path);
		  epilogs = (code_link_t **) calloc(num_tcfg_edges, sizeof(code_link_t *));
		  CHECK_MEM(epilogs);

		  for (i = 0; i < num_tcfg_edges; i++) {
					 if (max_elog_len[tcfg_edges[i]->src->id] == 0)
								continue;
					 path[0] = tcfg_edges[i];
					 trav_forward(i, path, 1, 0);
		  }

		  free(path);
}



		  static void
collect_bodies()
{
		  int		i, k, num;
		  tcfg_node_t	*bbi;

		  num = tcfg[0]->bb->num_inst;
		  start_body = (mas_inst_t *) calloc(num, sizeof(mas_inst_t));
		  CHECK_MEM(start_body);
		  for (k = 0; k < num; k++) {
					 start_body[k].inst = &tcfg[0]->bb->code[k];
					 start_body[k].bp_flag = BP_CPRED;
		  }
		  bodies = (mas_inst_t **) calloc(num_tcfg_edges, sizeof(mas_inst_t *));
		  CHECK_MEM(bodies);
		  for (i = 0; i < num_tcfg_edges; i++) {
					 bbi = tcfg_edges[i]->dst;
					 num = bbi->bb->num_inst;
					 bodies[i] = (mas_inst_t *) calloc(num, sizeof(mas_inst_t));
					 CHECK_MEM(bodies[i]);
					 for (k = 0; k < num; k++) {
								bodies[i][k].inst = &bbi->bb->code[k];
								bodies[i][k].bbi_id = bbi->id;
								bodies[i][k].bp_flag = BP_CPRED;
					 }
		  }
}



		  static void
set_body_hitmiss(int edge_id, int hm_id)
{
		  int		    i, num, ts0, ts, offset, offset_l2, mblk_id = 1, mblk_id_l2 = 1;
		  tcfg_node_t	    *bbi;
		  loop_t	    *lp = NULL;

		  bbi = tcfg_edges[edge_id]->dst; 

		  /* sudiptac :: check one context first */
		  lp = bbi_hm_list[bbi->id][hm_id]; 
		  num = bbi->bb->num_inst;
		  bodies[edge_id][0].ic_flag = get_mblk_hitmiss(bbi, 0, lp);

		  /* sudiptac :::: set L2 cache CHMC flag */
		  if(enable_il2cache)
					 bodies[edge_id][0].ic_flag_l2 = get_mblk_hitmiss_l2(bbi, 0, lp);

		  for (i = 1; i < num; i++) {
					 offset = CACHE_LINE(bbi->bb->code[i].addr);
					 if (offset == 0) {
								bodies[edge_id][i].ic_flag = get_mblk_hitmiss(bbi, mblk_id, lp);
								mblk_id++;
					 }
					 if(enable_il2cache) {
								offset_l2 = CACHE_LINE_L2(bbi->bb->code[i].addr);
								if (offset_l2 == 0)
								{
										  /* sudiptac :::: set L2 cache CHMC flag */
										  bodies[edge_id][i].ic_flag_l2 = get_mblk_hitmiss_l2(bbi, mblk_id_l2, lp);
										  mblk_id_l2++;
								}
					}			
		  }
}



		  static void
init_pa()
{
		  prolog_size = pipe_ibuf_size + pipe_iwin_size;
}



		  static void
alloc_est_units()
{
		  int		i, n, bbi_id, num_hm;
		  tcfg_edge_t	*e;

		  cpred_times = (int **) calloc(num_tcfg_edges, sizeof(int *));
		  mpred_times = (int **) calloc(num_tcfg_edges, sizeof(int *));
		  CHECK_MEM(cpred_times);
		  CHECK_MEM(mpred_times);

		  start_time = 0;
		  for (i = 0; i < num_tcfg_edges; i++) {
					 cpred_times[i] = (int *) calloc(TECHNICAL_OFFSET_MAXIMUM, sizeof(int));
					 CHECK_MEM(cpred_times);
					 if (cond_bbi(tcfg_edges[i]->src)) {
								mpred_times[i] = (int *) calloc(TECHNICAL_OFFSET_MAXIMUM, sizeof(int));
								CHECK_MEM(mpred_times);
					 }
		  }
}


// edge_id: tcfg edge id;
// hm_id: id in the set of hit/miss combinations of tcfg_edge[edge_id]->dst
// bp: BP_CORRECT or BP_MISPRED
		  static void
ctx_unit_time(int edge_id, int context, int bp)
{
		  int		    num_p, num_e, num_b, t, *t_cpred, *t_mpred;
		  tcfg_node_t	    *bbi;
		  tcfg_edge_t	    *e;
		  code_link_t	    *plog, *elog;
		  int		    unit_estimated;
		  loop_t	    *lp = NULL;
		  
		  all_offsets_in = NULL;
		  all_offsets_out = NULL;

		  if (context != -1) {
					t_cpred = &cpred_times[edge_id][context];
					t_mpred = &mpred_times[edge_id][context];
		  }

		  if (enable_icache)
					set_body_hitmiss(edge_id, 0);

		  bbi = tcfg_edges[edge_id]->dst;
		  num_b = bbi->bb->num_inst;

		  /* iterate through all prolog and epilog nodes */			 
		  for (plog = prologs[edge_id]; plog != NULL; plog = plog->next) {
					 num_p = plog->num_inst;
					 if (bpred_scheme != NO_BPRED && bp == BP_CPRED)
								num_p -= num_plog_trunc[edge_id];
					 unit_estimated = 0;
					 for (e = bbi->out; e != NULL; e = e->next_out) {
								for (elog = epilogs[e->id]; elog != NULL; elog = elog->next) {
										  num_e = elog->num_inst;
#ifdef _NDEBUG_CRPD
					printf("print loaded offsets for edge id %d\n", edge_id);
#endif
										  create_egraph(plog->code, num_p, elog->code, num_e,
																bodies[edge_id], num_b, bp, lp);
										  t = est_egraph();
										  
										  if (context != -1) {
													
													if ((bp == BP_CPRED) && (t > *t_cpred))
															   *t_cpred = t;
													if ((bp == BP_MPRED) && (t > *t_mpred))
															   *t_mpred = t;
										  }

										  unit_estimated = 1;
										  
										  /* sudiptac ::: merge the body offsets for each prologue and 
											* epilogue combination */
										  if(g_shared_bus_type != NOBUS) {
													 all_offsets_in = merge_offsets_from_body(all_offsets_in, num_b, 1);
													 all_offsets_out = merge_offsets_from_body(all_offsets_out, num_b, 0);
										  }
								}
					 }
					 if (unit_estimated == 1)
								continue;
#ifdef _NDEBUG_CRPD
					printf("print loaded offsets for edge id %d\n", edge_id);
#endif
					 create_egraph(plog->code, num_p, NULL, 0, bodies[edge_id], num_b, bp, lp);
					 t = est_egraph();
					 
					 if (context != -1) {
							   
							   if ((bp == BP_CPRED) && (t > *t_cpred))
										  *t_cpred = t;
							   if ((bp == BP_MPRED) && (t > *t_mpred))
										  *t_mpred = t;
					}

					 /* sudiptac ::: merge the body offsets for each prologue and 
					 * epilogue combination */
					 if(g_shared_bus_type != NOBUS) {
								all_offsets_in = merge_offsets_from_body(all_offsets_in, num_b, 1);
								all_offsets_out = merge_offsets_from_body(all_offsets_out, num_b, 0);
					 }
		  }

		  /* sudiptac ::: we need to save the offset information of body nodes, so that 
			* they can be reused when this body will be used as a prologue of some other 
			* body node */
#ifdef _NDEBUG_CRPD
					printf("print saved offsets for edge id %d\n", edge_id);
#endif
		  if(g_shared_bus_type != NOBUS) {
					 save_offsets_to_prologs(edge_id, all_offsets_in, num_b, 1);
					 save_offsets_to_prologs(edge_id, all_offsets_out, num_b, 0);
		  }
}



		  static void
est_start_unit()
{
		  int		    num_b, num_e, offset, offset_l2, i, t, unit_estimated = 0;
		  tcfg_node_t	    *bbi = tcfg[0];
		  tcfg_edge_t	    *e;
		  code_link_t	    *elog;

		  num_b = tcfg[0]->bb->num_inst;
		  start_body[0].ic_flag = IC_MISS;

		  all_offsets_in = NULL;
		  all_offsets_out = NULL;

		  /* sudiptac :::: for level 2 cache */
		  if(enable_il2cache)
					 start_body[0].ic_flag_l2 = IC_MISS;

		  for (i = 1; i < num_b; i++) {
					 offset = CACHE_LINE(bbi->bb->code[i].addr);
					 if (offset == 0)
								start_body[i].ic_flag = IC_MISS;
					 if (enable_il2cache) {
								/* sudiptac :::: for level 2 cache */
								offset_l2 = CACHE_LINE_L2(bbi->bb->code[i].addr);
								if(offset_l2 == 0)
										  start_body[i].ic_flag_l2 = IC_MISS;	 	 
					}					  
		  }

		  for (e = bbi->out; e != NULL; e = e->next_out) {
					 for (elog = epilogs[e->id]; elog != NULL; elog = elog->next) {
								num_e = elog->num_inst;
								create_egraph(NULL, 0, elog->code, num_e, start_body, num_b,
													 BP_CPRED, loops[0]);
								t = est_egraph();
								if (t > start_time)
										  start_time = t;
								unit_estimated = 1;
								
								/* sudiptac ::: merge the body offsets for each prologue and 
								 * epilogue combination */
								all_offsets_in = merge_offsets_from_body(all_offsets_in, num_b, 1);
								all_offsets_out = merge_offsets_from_body(all_offsets_out, num_b, 0);
					 }
		  }

		  if (unit_estimated == 0) {
					 create_egraph(NULL, 0, NULL, 0, start_body, num_b, BP_CPRED, loops[0]);
					 t = est_egraph();
					 if (t > start_time)
								start_time = t;
					 
					 /* sudiptac ::: merge the body offsets for each prologue and 
					 * epilogue combination */
					 all_offsets_in = merge_offsets_from_body(all_offsets_in, num_b, 1);
					 all_offsets_out = merge_offsets_from_body(all_offsets_out, num_b, 0);
		  }
		  
		  /* sudiptac ::: we need to save the offset information of body nodes, so that 
			* they can be reused when this body will be used as a prologue of some other 
			* body node */
		  save_offsets_to_prologs(-1, all_offsets_in, num_b, 1);
		  save_offsets_to_prologs(-1, all_offsets_out, num_b, 0);
}

/* WCET of one basic block */
static void WCET_Block(int edge_id, int context)
{
		  tcfg_edge_t	    *e;
		  int i, est_mispred;

		  e = tcfg_edges[edge_id];
		  
		  if ((bpred_scheme != NO_BPRED) && (cond_bbi(e->src)))
					 est_mispred = 1;
		  else
					 est_mispred = 0;
		 
		  ctx_unit_time(edge_id, context, BP_CPRED);
		  if (est_mispred)
					  ctx_unit_time(edge_id, context, BP_MPRED);
}

/* finalize the offset and solve the flow problem */
static combined_result 
finish_offset_graph_and_solve_flow_problem(combined_result iteration_result, offset_data start_offsets, 
		  offset_graph** graph, offset_graph** wcet_graph, offset_graph** bcet_graph, int iteration_number, 
		  loop_t* lp, int time_required, int is_pi_node_og)
{
		  uint i;
		  uint remaining_iterations;
		  combined_result result;
		  int tdma_interval = fx_slot_length * max_cores; 
		  offset_graph_node *search_node;

		  /* initialize */
		  result.wcet = result.bcet = 0;
		  result.offsets = NULL;
		  CALLOC( result.offsets, offset_data*, (*graph)->num_offsets_set, sizeof(offset_data), "result.offsets" );
		  
		  DOUT( "Analyzed %u iterations\n", iteration_number );
		  
		  /* If we note that all iterations have the same BCET/WCET, then we determine the loop
			* WCET without invoking the ILP solver, because these instances are trivial and
			* particularly hard to solve for the ILP solver, because no branch in the branch & bound
			* algorithm can be pruned. */

		  /* cleekee: pi node offset graph is always iterated one time less when finding loop's outgoing offset */
		  if (is_pi_node_og)
					remaining_iterations = lp->loopbound - 2;
		  else
					remaining_iterations = lp->loopbound - 1;
		  
		  /* TODO: Don't call ILP when all iterations were analyzed */
		  /* cleekee: temporarily disable the first condition*/
		  if ( 0 && (*graph)->allIterationsAreEqual ) {
					 result.bcet += remaining_iterations * iteration_result.bcet;
					 result.wcet += remaining_iterations * iteration_result.wcet;
					 result.offsets[0] = start_offsets;
					 updateOffsetData( &result.offsets[0], &result.offsets[0], result.bcet, result.wcet, FALSE );
		  } else {
					 if ( haveExplicitTime ) {
							    // DEPRECATED 
					 } else {
								if (time_required) {
										  generateOffsetGraphILPCons( *graph, remaining_iterations, lp->head->id );
								}
								
								/* iterate through offset graph to get outgoing offsets for the loop */
							    i = remaining_iterations;
								while(i > 0) {
										  search_node = (i == remaining_iterations)?
												        (*graph)->root : search_node->out->dst;
										  i--;

										  if (i==0)
												    result.offsets[0] = search_node->out->dst->offsets[0];
								}
					 }
		  }

		  if ( haveExplicitTime ) {
					 freeOffsetGraph( *bcet_graph );
					 freeOffsetGraph( *wcet_graph );
		  } else {
					 freeOffsetGraph( *graph );
		  }

		  assert( isOffsetDataValid( &result.offsets[0] ) && "Invalid result!" );

		  return result;
}

/* modify the offset graph after each iteration of graph tracking analysis */
static combined_result modify_offset_graph(int wcet_dag, struct offset_link* offset_in, 
		  offset_graph** graph, offset_graph** wcet_graph, offset_graph** bcet_graph, 
		  offset_data current_offsets, combined_result previous_result, int* graphChanged, 
		  loop_t* lp, int iteration_number, int is_pi_node_og) 
{
		  int i;
		  combined_result iteration_result;
		  offset_graph_node *new_node = NULL;

		  /* compute iteration_result */
		  iteration_result.wcet = wcet_dag;
		  iteration_result.bcet = 0;

		  if (is_pi_node_og) {
					CALLOC( iteration_result.offsets, offset_data*, 1, sizeof(offset_data), "iteration_result.offsets" );
					iteration_result.offsets[0] = createOffsetDataFromOffsetLink(currentOffsetRepresentation, offset_in);
		  } else {
					CALLOC(iteration_result.offsets, offset_data*, (*graph)->num_offsets_set, sizeof(offset_data), "iteration_result.offsets");
					for ( i=0; i<(*graph)->num_offsets_set; i++)
							  iteration_result.offsets[i] = createOffsetDataFromOffsetLink( currentOffsetRepresentation,
										all_offsets_in[(*graph)->offsets_ins[i]][(*graph)->offsets_stage[i]] );
		  }

		  uint tdma_interval = fx_slot_length * max_cores;
		  
		  if ( iteration_number > 1 &&  previous_result.wcet != iteration_result.wcet ) {
					 
					 (*graph)->allIterationsAreEqual = FALSE;
		  }
					 
		  if ( haveExplicitTime ) {
					 iteration_result.offsets[0].content.time_range.bcet_time %= tdma_interval;
					 iteration_result.offsets[0].content.time_range.wcet_time %= tdma_interval;
		  }
		  
		  /* If the user selected this, then try to analyze the same iteration using
		   * a fixed alignment and use the better one of the two results. */
		  /* For graph-tracking we also try this to limit the complexity of the
		   * resulting ILP, if the offset results are very bad currently. */
		  const _Bool stdOffsetsUnbounded = isOffsetDataMaximal( &iteration_result.offsets[0] );

		  /* In offset analysis mode we must add edges from all
		  * starting offsets to all end offsets. */
		  if ( !haveExplicitTime ) {
					 if ((*graph)->last->num_outgoing_edges == 0) {
							   new_node = getOffsetGraphNode( *graph, iteration_result.offsets );
							   /* If no node with the same context is found in the graph */ 
							   if (new_node == NULL) {
										addOffsetGraphNode(*graph, iteration_result.offsets, 
												  iteration_result.bcet, iteration_result.wcet);
										/* update number of contexts in this loop */
										if (!is_pi_node_og) {
												  if (lp->parent && lp->parent != loops[0]) {
															if (lp->parent_prev_num_context < lp->parent->total_num_context)
																	  lp->total_num_context++;
												  } else {
															lp->total_num_context++;
												  }
										}
										*graphChanged = 1;
							   } else {
										/* Link from last node to the found node to complete the graph */
										addOffsetGraphEdge(*graph, (*graph)->last, new_node,
												 iteration_result.bcet, iteration_result.wcet);
										*graphChanged = 1;
										
							   }
					 }

		  /* In absolute time analysis mode we must only add edges from the best-case time
		   * to the new best-case time-offset, and the same for the worst-case scenario.
			*/
		  } else {
					// DEPRECATED
#if 0
					 offset_graph_node * const bcStart = getOffsetGraphNode( *bcet_graph, 
								current_offsets.content.time_range.bcet_time );
					 offset_graph_node * const bcEnd = getOffsetGraphNode( *bcet_graph, 
								iteration_result.offsets.content.time_range.bcet_time );
					 offset_graph_edge * const bcEdge = getOffsetGraphEdge( *bcet_graph, 
								bcStart, bcEnd );

					 if ( bcEdge == NULL ) {
								addOffsetGraphEdge( *bcet_graph, bcStart, bcEnd,
													 iteration_result.bcet, iteration_result.bcet );
								*graphChanged = 1;
					 } else {
								assert( bcEdge->bcet == bcEdge->wcet &&
													 bcEdge->bcet == iteration_result.bcet &&
													 "Invalid internal state!" );
					 }

					 offset_graph_node * const wcStart = getOffsetGraphNode( *wcet_graph,
										  current_offsets.content.time_range.wcet_time );
					 offset_graph_node * const wcEnd = getOffsetGraphNode( *wcet_graph,
										  iteration_result.offsets.content.time_range.wcet_time );
					 offset_graph_edge * const wcEdge = getOffsetGraphEdge( *wcet_graph,
										  wcStart, wcEnd );

					 if ( wcEdge == NULL ) {
								addOffsetGraphEdge( *wcet_graph, wcStart, wcEnd,
													 iteration_result.wcet, iteration_result.wcet );
								*graphChanged = 1;
					 } else {
								assert( wcEdge->wcet == wcEdge->bcet &&
													 wcEdge->wcet == iteration_result.wcet &&
													 "Invalid internal state!" );
					 }
#endif
		  }

		  return iteration_result;
}

void add_og_offsets(offset_graph* graph, uint ins, uint stage)
{
		  int i;

		  //check whether the egraph node is already collected
		  for ( i=0; i<graph->num_offsets_set; i++ ) {
					if ( graph->offsets_ins[i] == (ins - plog_len) && graph->offsets_stage[i] == stage)
							  return;
		  }

		  graph->num_offsets_set++;

		  CALLOC_OR_REALLOC( graph->offsets_ins, uint*, graph->num_offsets_set * sizeof(uint), "graph->offsets_ins");
		  graph->offsets_ins[graph->num_offsets_set - 1] = ins - plog_len;
		  CALLOC_OR_REALLOC( graph->offsets_stage, uint*, graph->num_offsets_set * sizeof(uint), "graph->offsets_stage");
		  graph->offsets_stage[graph->num_offsets_set - 1] = stage;
#ifdef _DEBUG_OFFSETS
		  printf("collected pi node (%i, %i)'s bus context\n", ins, stage);
#endif
}

// collect set of egraph nodes responsible for changing bus contexts over iterations of a loop
void collect_offsets_set(offset_graph* graph, int edge_id) 
{
		  int i, j;
		  int c_prolog_len; 
		  egraph_edge_t *e;
		
	      // exclude mispredicted prologue instructions	
		  if (bpred_scheme != NO_BPRED && !cond_bbi(tcfg_edges[edge_id]->src))
		  		  c_prolog_len = plog_len - num_plog_trunc[edge_id];
		  else
		          c_prolog_len = plog_len;

		  /* for each prologue instructions */
		  for ( i=0; i<c_prolog_len; i++ )
					/* for each pipeline stage */
					for ( j=0; j<pipe_stages; j++ ) {
							  for( e=egraph[i][j].out; e!=NULL; e=e->next_out ) {
										if( e->dst->inst >= plog_len && e->dst->inst < plog_len + body_len ) {
#ifdef _DEBUG_OFFSETS
												  printf("prologue node (%i, %i) link to body node (%i, %i)\n", i, j, e->dst->inst, e->dst->stage);
#endif
												  add_og_offsets(graph, e->dst->inst, e->dst->stage);
										}
							  }
							  
							  for( e=egraph[i][j].l_contd; e!=NULL; e=e->next_out ) {
										if( e->dst->inst >= plog_len && e->dst->inst < plog_len + body_len ) {
#ifdef _DEBUG_OFFSETS
												  printf("prologue node (%i, %i) link to body node (%i, %i)\n", i, j, e->dst->inst, e->dst->stage);
#endif
												  add_og_offsets(graph, e->dst->inst, e->dst->stage);
										}
							  }
					}
}

/* initialize offset graph at the IF node of the first loop instruction */
static combined_result init_offset_graph(offset_graph** graph, offset_graph** wcet_graph, 
		  offset_graph** bcet_graph, struct offset_link* offset_in, loop_t* lp, int edge_id, int is_pi_node_og)
{
		  int i;
		  const uint tdma_interval = fx_slot_length * max_cores;

		  *graph = createOffsetGraph( tdma_interval );
		  (*graph)->loop = lp;
		  *bcet_graph = ( haveExplicitTime ? createOffsetGraph( tdma_interval ) : NULL );
		  *wcet_graph = ( haveExplicitTime ? *graph : NULL );
		  
		  combined_result result;
		  result.wcet = result.bcet = 0;
		  
		  if (is_pi_node_og) {
					(*graph)->num_offsets_set = 1;
					CALLOC( result.offsets, offset_data*, 1, sizeof(offset_data), "result.offsets" );
					result.offsets[0] = createOffsetDataFromOffsetLink(currentOffsetRepresentation, offset_in);
		  } else {
					collect_offsets_set( *graph, edge_id );
					CALLOC( result.offsets, offset_data*, (*graph)->num_offsets_set, sizeof(offset_data), "result.offsets" );
					for ( i=0; i<(*graph)->num_offsets_set; i++)
							  result.offsets[i] = createOffsetDataFromOffsetLink( currentOffsetRepresentation,
										all_offsets_in[(*graph)->offsets_ins[i]][(*graph)->offsets_stage[i]] );
		  }

		  /* create initial edges */

		  if ( haveExplicitTime ) {
					// DEPRECATED
		  } else {
					addOffsetGraphNode(*graph, result.offsets, 0, 0);
					/* update number of contexts in this loop */
					if (!is_pi_node_og) {
							  if (lp->parent && lp->parent != loops[0]) {
										if (lp->parent_prev_num_context < lp->parent->total_num_context)
												  lp->total_num_context++;
							  } else {
										lp->total_num_context++;
							  }
					}
		  }

		  return result;
}

/* solve auxilliary node offset graphs and restore the contents in relevant prologs */
static void 
finish_offset_graphs_and_restore(struct pi_node* pi_nodes, int num_pi_nodes, 
		  int iteration_number, loop_t* lp) 
{
		  int i, edge_id;
		  combined_result result, iteration_result;
		  int inst, stage;
		  struct offset_link* offset_out;

		  CALLOC( iteration_result.offsets, offset_data*, 1, 
					sizeof(offset_data), "iteration_result.offsets" );

		  for (i = 0; i < num_pi_nodes; i++)
		  {
					 iteration_result.wcet = iteration_result.bcet = 0;
		  			 iteration_result.offsets[0] = pi_nodes[i].offsets;
					 inst = pi_nodes[i].inst;
					 stage = pi_nodes[i].stage;
					 edge_id = pi_nodes[i].edge_id;

					 result = finish_offset_graph_and_solve_flow_problem(iteration_result, 
								pi_nodes[i].start_offsets, &(pi_nodes[i].graph), 
								&(pi_nodes[i].wcet_graph), &(pi_nodes[i].bcet_graph), 
								iteration_number, lp, 0, 1);
					 
					 offset_out = createOffsetLinkFromOffsetData(currentOffsetRepresentation, result.offsets[0]); 
					 modify_offsets_to_prologs(edge_id, offset_out, inst, stage, 0);
		  }
}

/* initialize, modify all auxulliary nodes' offset graphs */
static void
handle_offset_graphs_of_aux_nodes (struct pi_node* pi_nodes, int num_pi_nodes, 
		  int first_iteration, int edge_id, int* graphChanged, int iteration_number, int *offsetPropagated, loop_t* lp)
{
		  int i, inst, stage;
		  struct offset_link* offset_out;
		  combined_result result, iteration_result;
		  
		  for (i = 0; i < num_pi_nodes; i++)
		  {
					 if (pi_nodes[i].edge_id == edge_id) {
								inst = pi_nodes[i].inst;
								stage = pi_nodes[i].stage;
							    offset_out = all_offsets_out[inst][stage]; 
								*offsetPropagated = 0;
								/* for the first iteration initialize the offset graphs, for other 
								 * iterations modify the offset graphs */
								if (first_iteration) {
										  result = init_offset_graph(&pi_nodes[i].graph, &pi_nodes[i].wcet_graph, 
													 &pi_nodes[i].bcet_graph, offset_out, lp, edge_id, 1);
										  pi_nodes[i].offsets = result.offsets[0];
										  pi_nodes[i].start_offsets = result.offsets[0];
								} else {
										  iteration_result.wcet = iteration_result.bcet = 0;
										  iteration_result = modify_offset_graph(0, 
																		  offset_out, 
																		  &pi_nodes[i].graph, 
																		  &pi_nodes[i].wcet_graph, 
																		  &pi_nodes[i].bcet_graph, 
																		  pi_nodes[i].offsets, 
																		  iteration_result, 
																		  graphChanged,
																		  lp,
																		  iteration_number,
																		  1);
										  pi_nodes[i].offsets = iteration_result.offsets[0];
								}
					 }
		  }
}

/* WCET of a loop with shared bus */
/* call through WCET_Loop */
static void WCET_Loop_with_shared_bus(int edge_id)
{
		  tcfg_node_t* header;
		  tcfg_edge_t* bedge;
		  loop_t* this_loop;
		  loop_t* lp;
		  int edge, i;
		  struct pi_node* pi_nodes = NULL;
		  int num_pi_nodes = 0;
		  int first_iteration = 1;
		  int iteration_number = 0;
		  int wcet_dag = 0;
		  const uint tdma_interval = fx_slot_length * max_cores;
		  int graphChanged = 0;
		  int offsetPropagated = 0;
		  int back_edge_swapped = 0;
		  int context = 0;
		  struct offset_link* offset_in;
		  offset_data current_offsets; 
		  offset_data start_offsets; 
		  offset_graph* graph, *wcet_graph, *bcet_graph;
		  /* combined result after loop analysis : <WCET, outgoing offsets> */
		  combined_result result, iteration_result;
#ifdef _DEBUG_OFFSETS
		  printf("-------------- START OF NEW LOOP --------------\n");
#endif
		  /* loop header */
		  header = tcfg_edges[edge_id]->dst;
		  this_loop = loop_map[header->id];
		  this_loop->cur_iter = 0;
		  assert( this_loop->loopbound!=0 && "Loop bound is 0!");

		  /* for optimizing WCET through offset graph */
		  haveExplicitTime = (currentOffsetRepresentation == OFFSET_DATA_TYPE_TIME_RANGE);
		  graph = (offset_graph *)this_loop->graph;
		  wcet_graph = (offset_graph *)this_loop->wcet_graph;
		  bcet_graph = (offset_graph *)this_loop->bcet_graph;

		  do {
					 back_edge_swapped = 0;
					 graphChanged = 0;

					 for (edge = edge_id; edge < num_tcfg_edges; edge++)
					 {
							    /* for subsequent iterations except the first, back edge should be processed first */
							    if (!first_iteration && edge == edge_id && !back_edge_swapped) {
										edge = this_loop->back_edge->id;
								} else if (!first_iteration && edge == this_loop->back_edge->id) {
										continue;
							    }

								estimating_body = tcfg_edges[edge]->dst;
								lp = loop_map[estimating_body->id];
					 
								/* if both cfg nodes are not inside this loop, then skip 
								* the computation. The relevant computation would be 
								* done somewhere else */
								if (loop_comm_ances[this_loop->id][lp->id] != this_loop)
										  continue;
							  				
							    /* handle inner loop here */
							    /* cleekee: prevents back edge of inner loop from calling WCET_Loop() again */
								if(tcfg_is_loop_head(estimating_body) && estimating_body != header && tcfg_edges[edge] != lp->back_edge) {
										  if (lp->parent && lp->parent != loops[0]) { 
												    /* cleekee: if parent loop's og is unchanged 
													   (parent->total_num_context not increased), skip WCET_Loop() */
												    if (lp->parent_prev_num_context < lp->parent->total_num_context)
															  WCET_Loop(edge);
										  } else {
											 	    WCET_Loop(edge);
										  }
							    } else if (lp == this_loop) {
										  /* at this point, the loop which immediately nests estimating body 
										   * must be this_loop */
										  
										  /* cleekee: in 1st iteration, back edge cannot enter WCET_Block()
										     in subsequent iterations, front edge cannot enter WCET_Block() */
										  if ((first_iteration && (edge == edge_id || estimating_body != header))
												  || (!first_iteration && edge != edge_id)) {
												  /* make sure context number is properly assigned in ILP */
												  if (lp->parent && lp->parent != loops[0] && loop_map[tcfg_edges[edge]->src->id] == lp->parent) {
															context = lp->parent->total_num_context - 1;
												  } else if (lp->total_num_context > 0) {
															context = lp->total_num_context - 1;
												  } else {
															context = 0;
												  }
												  WCET_Block(edge, context);
												  offsetPropagated = 1;
										  }

										  if (first_iteration) {
													 /* create and initialize offset graph for the fetch node of 
													  * first loop instruction */
													 if (estimating_body == header && edge == edge_id) {
																result = init_offset_graph(&graph, &wcet_graph, 
																		  &bcet_graph, NULL, lp, edge_id, 0);
																graphChanged = 1;		    
																
																start_offsets = result.offsets[0];
													 }
													 if (tcfg_edges[edge] != lp->back_edge)
		  													 collect_pi_nodes(&pi_nodes, &num_pi_nodes, edge, this_loop);
										  } else if (estimating_body == header && edge != edge_id) {
													 iteration_result = modify_offset_graph(wcet_dag, NULL, &graph, 
																&wcet_graph, &bcet_graph, current_offsets, iteration_result, 
																&graphChanged, lp, iteration_number, 0);
										  }
										  
										  /* handle pi_nodes here */
										  if (offsetPropagated)
												     handle_offset_graphs_of_aux_nodes(pi_nodes, num_pi_nodes, first_iteration, 
															    edge, &graphChanged, iteration_number, &offsetPropagated, lp);
								}
 
							    /* after back edge is processed, process the next edge directly after front edge */
							    if (!first_iteration && !back_edge_swapped) {
										edge = edge_id - 1;
										back_edge_swapped = 1;
								}
					 } // for all edges

					 if (first_iteration)
								current_offsets = result.offsets[0];
					 else
								current_offsets = iteration_result.offsets[0];
#ifdef _DEBUG_OFFSETS
					 if (!first_iteration) {
							  printf("\ncurrent_offsets:\n");
							  for(i=0; i < TECHNICAL_OFFSET_MAXIMUM; i++)
										if(current_offsets.content.offset_set.offsets[i]!=0)
												  printf("%-2i ", i);
							  printf("\n\n");
					 }
#endif
					 first_iteration = 0;
					 
					 /* compute one iteration WCET of the loop */ 
					 /* sudiptac: this one is not needed anymore */
					 /* wcet_dag = do_wcet_loop_iteration(this_loop); */

					 iteration_number++;
					 this_loop->cur_iter = iteration_number;
		  
		  } while(graphChanged && (iteration_number <= 1 || 
					 !isOffsetDataEqual(&current_offsets, &iteration_result.offsets[0])) && 
					 iteration_number < this_loop->loopbound);
#ifdef _DEBUG_OFFSETS
		  printf("Offset graph for loop %i with total %i contexts:\n", this_loop->id, this_loop->total_num_context);
		  dumpOffsetGraph(graph, stdout);}		  
#endif
		  /* cleeke: update context information for tracking contexts in ILP */
		  if (this_loop->parent && this_loop->parent != loops[0]) {
					if (this_loop->parent_prev_num_context < this_loop->parent->total_num_context) {
							  CALLOC_OR_REALLOC(this_loop->num_context, int*,
										(this_loop->parent->total_num_context + 1) * sizeof(int), "this_loop->num_context");
							  this_loop->num_context[this_loop->parent->total_num_context - 1] = graph->num_nodes;
							  this_loop->parent_prev_num_context = this_loop->parent->total_num_context;
					}
		  } else {
					this_loop->num_context[0] = graph->num_nodes;
		  }

		  /* finish and solve the flow problem using offset graph */
		  result = finish_offset_graph_and_solve_flow_problem(iteration_result, start_offsets, 
					 &graph, &wcet_graph, &bcet_graph, iteration_number, this_loop, 1, 0);

		  /* finish, solve and restore offsets from the offset graphs of pi nodes */
		  finish_offset_graphs_and_restore(pi_nodes, num_pi_nodes, iteration_number, this_loop);
		  
#ifdef _DEBUG_CRPD
		  dump_pi_nodes(pi_nodes, num_pi_nodes, this_loop);
#endif

		  if (pi_nodes)
					 /* clean up pi_nodes structure */
					 cleanup_pi_nodes(pi_nodes);

		  free(result.offsets);
#ifdef _DEBUG_OFFSETS
		  printf("-------------- END OF LOOP --------------\n\n");
#endif
		  return;
}

/* sudiptac: The following function is not needed anymore */
#if 0
/* WCET of a loop without shared bus */
/* call through WCET_Loop */
static void WCET_Loop_without_shared_bus(int edge_id)
{
		  tcfg_node_t* header;
		  tcfg_edge_t* bedge;
		  loop_t* this_loop;
		  loop_t* lp;
		  int edge, i;
		  int wcet_dag = 0;

		  /* loop header */
		  header = tcfg_edges[edge_id]->dst;
		  this_loop = loop_map[header->id];

		  do {
					 for (edge = edge_id; edge < num_tcfg_edges; edge++)
					 {
								estimating_body = tcfg_edges[edge]->dst;
								lp = loop_map[estimating_body->id];
					 
								/* if both cfg nodes are not inside this loop, then skip 
								* the computation. The relevant computation would be 
								* done somewhere else */
								if (loop_comm_ances[this_loop->id][lp->id] != this_loop)
										  continue;

								if(tcfg_is_loop_head(estimating_body) && estimating_body != header) {
										  WCET_Loop(edge);
								} else {
										  /* at this point, the loop which immediately nests estimating body 
										  * must be this_loop */
										  WCET_Block(edge, 0);
								}

								/* exit node of the loop ? end computation */
								if (estimating_body == this_loop->tail) {
										  for (bedge = estimating_body->out; bedge; bedge = bedge->next_out)
										  {
													 /* find the back edge */
													 if (bedge->dst == this_loop->head) {
																WCET_Block(bedge->id, 0);
													 }
										  }
										  break;
								}
					 }

					 /* compute one iteration WCET of the loop */ 
					 /* wcet_dag = do_wcet_loop_iteration(this_loop); */

		  } while(0);

		  /* set loop WCET */
		  this_loop->wcet_lp = this_loop->loopbound * wcet_dag;

		  return;
}
#endif

/* WCET of a loop */
static void WCET_Loop(int edge_id) 
{
		  if (g_shared_bus_type != NOBUS)
					 WCET_Loop_with_shared_bus(edge_id);
		  else
					 /* must not come here */
					 assert(0);
					 /* WCET_Loop_without_shared_bus(edge_id); */
}

/* top level routine to estimate the WCET of each basic block */

		  static void
est_units()
{
		  int		    edge_id, num_hm, hm, est_mispred;
		  tcfg_edge_t	    *e;
		  code_link_t	    *plog, *elog;
		  tcfg_node_t* src;
		  char toploop = 0;
		  char prehead = 0;

		  estimating_body = tcfg[0];
		 
		  alloc_est_units();
		  est_start_unit();
		  
		  //printf("\test_units: %d\n", num_tcfg_edges);
		  for (edge_id = 0; edge_id < num_tcfg_edges; edge_id++) {
					 
					 estimating_body = tcfg_edges[edge_id]->dst;
					 src = tcfg_edges[edge_id]->src;
					 prehead = (loop_map[src->id] == loops[0]) ? 1 : 0;
					 toploop = (loop_map[estimating_body->id] == loops[0]) ? 1 : 0;

					 if (g_shared_bus_type != NOBUS) {
								/* differentiate loop head and normal basic blocks */
								if(tcfg_is_loop_head(estimating_body) && prehead)
								{
										if(loop_map[estimating_body->id]->loopbound > 2 
												  || loop_map[estimating_body->id]->num_child > 0)
												  WCET_Loop(edge_id);
										else
												  WCET_Block(edge_id, 0);
							    }
								/* only if outside any loop */			
								else if (toploop)
										  WCET_Block(edge_id, 0);
					 } else
								WCET_Block(edge_id, 0);
								
		  }
}




// handling cache misses caused by loop-exit mispreds
tcfg_elink_t    ***mp_affected_sets;
int		    ***mp_times;

// temporary variables that become useless after affected sets are collected
int		**mp_set_tags;
int		*num_mp_set_tags;



		  static void
get_loop_affected_sets(int lp_id, tcfg_edge_t *mp_edge, int num_inst)
{
		  tcfg_edge_t		*edge;
		  tcfg_node_t		*bbi;
		  tcfg_elink_t	*elink;
		  int			offset, i, k, set, tag;

		  if (mp_edge == mp_edge->src->out)
					 edge = mp_edge->next_out;
		  else
					 edge = mp_edge->src->out;
		  if (edge->flags == 1)
					 return;

		  for (i = num_inst; i < num_mp_insts[edge->id]; i++) {
					 offset = CACHE_LINE(mp_insts[edge->id][i]->addr);
					 if (offset == 0)
								break;
		  }
		  memset(num_mp_set_tags, 0, cache.ns * sizeof(int));
		  for (; i < num_mp_insts[edge->id]; i++) {
					 set = SET(mp_insts[edge->id][i]->addr);
					 if (num_mp_set_tags[set] == cache.na)
								continue;
					 tag = TAG(mp_insts[edge->id][i]->addr);
					 if (num_mp_set_tags[set] == 0) {
								mp_set_tags[set][0] = tag;
								num_mp_set_tags[set] = 1;
					 } else {
								for (k = 0; k < num_mp_set_tags[set]; k++) {
										  if (tag == mp_set_tags[set][k])
													 break;
								}
								if (k == num_mp_set_tags[set]) {
										  mp_set_tags[set][k] = tag;
										  num_mp_set_tags[set]++;
								}
					 }
		  }

		  for (set = 0; set < cache.ns; set++) {
					 i = num_mblk_conflicts[lp_id][set];
					 if (i > cache.na)
								continue;
					 if ((num_mp_set_tags[set] + i) > cache.na) {
								elink = (tcfg_elink_t *) calloc(1, sizeof(tcfg_elink_t *));
								elink->edge = edge;
								elink->next = mp_affected_sets[lp_id][set];
								mp_affected_sets[lp_id][set] = elink;
					 }
		  }
}



		  static void
find_cond_exit(int lp_id, tcfg_edge_t *edge, int num_inst)
{
		  tcfg_elink_t	*elink;

		  if (cond_bbi(edge->src)) {
					 get_loop_affected_sets(lp_id, edge, num_inst);
		  } else {
					 num_inst += edge->src->bb->num_inst;
					 if (num_inst >= (pipe_ibuf_size + pipe_iwin_size - 1))
								return;
					 for (edge = edge->src->in; edge != NULL; edge = edge->next_in)
								find_cond_exit(lp_id, edge, num_inst);
		  }
}



// for each loop level, collect cache sets which are affected in terms of cache
// misses
		  static void
collect_affected_sets()
{
		  tcfg_node_t	    *bbi;
		  tcfg_edge_t	    *edge;
		  tcfg_elink_t    *elink;
		  loop_t	    *lp;
		  int		    lp_id, set, tag;

		  mp_affected_sets = (tcfg_elink_t ***) calloc(num_tcfg_loops, sizeof(tcfg_elink_t **));
		  for (lp_id = 1; lp_id < num_tcfg_loops; lp_id++)
					 mp_affected_sets[lp_id] = (tcfg_elink_t **) calloc(cache.ns, sizeof(tcfg_elink_t *));

		  mp_set_tags = (int **) calloc(cache.ns, sizeof(int *));
		  for (set = 0; set < cache.ns; set++)
					 mp_set_tags[set] = (int *) calloc(cache.na, sizeof(int));
		  num_mp_set_tags = (int *) calloc(cache.ns, sizeof(int));

		  for (lp_id = 1; lp_id < num_tcfg_loops; lp_id++) {
					 lp = loops[lp_id];
					 for (elink = lp->exits; elink != NULL; elink = elink->next)
								find_cond_exit(lp_id, elink->edge, 0);
					 clear_tcfg_edge_flags();
		  }

		  for (set = 0; set < cache.ns; set++)
					 free(mp_set_tags[set]);
		  free(mp_set_tags);
		  free(num_mp_set_tags);
}



		  static void
mp_set_body_hitmiss(int edge_id, int hm_id, int set)
{
		  int		    i, num, ts0, ts, offset, mblk_id = 1, set1;
		  tcfg_node_t	    *bbi;
		  loop_t	    *lp;

		  bbi = tcfg_edges[edge_id]->dst; 
		  lp = bbi_hm_list[bbi->id][hm_id];
		  num = bbi->bb->num_inst;
		  set1 = SET(bbi->bb->sa);

		  if (set1 == set)
					 bodies[edge_id][0].ic_flag = IC_UNCLEAR;
		  else
					 bodies[edge_id][0].ic_flag = get_mblk_hitmiss(bbi, 0, lp);

		  for (i = 1; i < num; i++) {
					 offset = CACHE_LINE(bbi->bb->code[i].addr);
					 if (offset == 0) {
								set1 = SET(bbi->bb->code[i].addr);
								if (set1 == set)
										  bodies[edge_id][i].ic_flag = IC_UNCLEAR;
								else
										  bodies[edge_id][i].ic_flag = get_mblk_hitmiss(bbi, mblk_id, lp);
								mblk_id++;
					 }
		  }
}



		  static void
ctx_mpmiss_time(int edge_id, int hm_id, int set, int bp)
{
		  int		    num_p, num_e, num_b, t, *t_mp, unit_estimated;
		  tcfg_node_t	    *bbi;
		  tcfg_edge_t	    *e;
		  code_link_t	    *plog, *elog;
		  loop_t	    *lp;

		  t_mp = &mp_times[edge_id][hm_id][set];
		  if (enable_icache)
					 mp_set_body_hitmiss(edge_id, hm_id, set);
		  bbi = tcfg_edges[edge_id]->dst;
		  num_b = bbi->bb->num_inst;

		  if (enable_icache)
					 lp = bbi_hm_list[bbi->id][hm_id];
		  else
					 lp = NULL;

		  for (plog = prologs[edge_id]; plog != NULL; plog = plog->next) {
					 num_p = plog->num_inst;
					 if (bpred_scheme != NO_BPRED && bp == BP_CPRED)
								num_p -= num_plog_trunc[edge_id];
					 unit_estimated = 0;
					 for (e = bbi->out; e != NULL; e = e->next_out) {
								for (elog = epilogs[e->id]; elog != NULL; elog = elog->next) {
										  num_e = elog->num_inst;
										  create_egraph(plog->code, num_p, elog->code, num_e,
																bodies[edge_id], num_b, bp, lp);
										  t = est_egraph();
										  if (t > *t_mp)
													 *t_mp = t;
										  unit_estimated = 1;
								}
					 }
					 if (unit_estimated == 1)
								continue;

					 create_egraph(plog->code, num_p, NULL, 0, bodies[edge_id], num_b, bp, lp);
					 t = est_egraph();
					 if (t > *t_mp)
								*t_mp = t;
		  }
}



		  static void
est_mpmiss_times()
{
		  int		    edge_id, num_hm, hm, bp, set;
		  tcfg_node_t	    *bbi;
		  tcfg_edge_t	    *e;
		  loop_t	    *lp;
		  code_link_t	    *plog, *elog;

		  mp_times = (int ***) calloc(num_tcfg_edges, sizeof(int **));

		  for (edge_id = 0; edge_id < num_tcfg_edges; edge_id++) {
					 e = tcfg_edges[edge_id];
					 num_hm = num_hit_miss[e->dst->id];
					 mp_times[edge_id] = (int **) calloc(num_hm, sizeof(int *));
					 bbi = e->dst;
					 if (cond_bbi(e->src))
								bp = BP_MPRED;
					 else
								bp = BP_CPRED;
					 for (hm = 0; hm < num_hm; hm++) {
								mp_times[edge_id][hm] = (int *) calloc(cache.ns, sizeof(int));
								lp = bbi_hm_list[bbi->id][hm];
								if ((lp == NULL) || (lp->id == 0))
										  continue;
								for (set = 0; set < cache.ns; set++) {
										  if (mp_affected_sets[lp->id][set] != NULL) {
													 ctx_mpmiss_time(edge_id, hm, set, bp);
										  }
								}
					 }
		  }
}



// estimate contributions of mispred raised cache misses to execution time
		  static void
handle_mpmiss()
{
		  collect_affected_sets();
		  est_mpmiss_times();
}



		  void
pipe_analysis()
{
		  tmp_cs = (mblk_tag_t *) calloc(cache.ns, sizeof(mblk_tag_t));
		  CHECK_MEM(tmp_cs);
		  
		  /* sudiptac :::: for level 2 cache analysis */
		  if (enable_il2cache) {
					 tmp_cs_l2 = (mblk_tag_t *) calloc(cache_l2.ns, sizeof(mblk_tag_t));
					 CHECK_MEM(tmp_cs_l2);
		  }

		  init_pa();
		  set_mlat_bbi();
		  bound_elog_len();

		  if (bpred_scheme != BP_NONE)
					 find_mlat_mpinst();

		  collect_bodies();
		  collect_prologs();
		  collect_epilogs();
		  est_units();

		  if (enable_icache && (bpred_scheme != NO_BPRED))
					 handle_mpmiss();

		  free(mlat_bbi);
		  free(tmp_cs);

		  if(enable_il2cache)
					 free(tmp_cs_l2);
}






		  static void
dump_xlogs(code_link_t **xlogs)
{
		  tcfg_node_t	    *src, *dst;
		  int		    i, k;
		  code_link_t	    *log;
		  mas_inst_t	    *mas_inst;

		  for (i = 0; i < num_tcfg_edges; i++) {
					 src = tcfg_edges[i]->src;
					 dst = tcfg_edges[i]->dst;
					 printf("\nedge: %d.%d -> %d.%d", bbi_pid(src), bbi_bid(src),
										  bbi_pid(dst), bbi_bid(dst));
					 for (log = xlogs[i]; log != NULL; log = log->next) {
								for (k = 0; k < log->num_inst; k++) {
										  mas_inst = &log->code[k];
										  if ((k & 7) == 0)
													 printf("\n    %x", mas_inst->inst->addr);
										  else
													 printf("  %x", mas_inst->inst->addr);
										  if (mas_inst->bp_flag == BP_MPRED)
													 printf("/M");
										  else if (mas_inst->bp_flag == BP_UNCLEAR)
													 printf("/U");
										  else
													 printf("/C");
										  if (mas_inst->ic_flag == IC_MISS)
													 printf("M");
										  else if (mas_inst->ic_flag == IC_UNCLEAR)
													 printf("U");
										  else
													 printf("H");
								}
								printf("\n    -------------------------------------------");
								printf("-------------------------------------------\n");
					 }
		  }
}



		  void
dump_units_times()
{
		  tcfg_edge_t	*e;
		  int		edge_id, hm, num_hm;

		  printf("dump timing estimates for basic blocks\n");
		  for (edge_id = 0; edge_id < num_tcfg_edges; edge_id++) {
					 e = tcfg_edges[edge_id];
					 printf("%d.%d -> %d.%d\n",
										  bbi_pid(e->src), bbi_bid(e->src), bbi_pid(e->dst), bbi_bid(e->dst));
					 if (enable_icache) {
								num_hm = num_hit_miss[tcfg_edges[edge_id]->dst->id];
					 } else
								num_hm = 1;
					 for (hm = 0; hm < num_hm; hm++) {
								printf("    hm[%d]: %d", hm, cpred_times[edge_id][hm]);
								if ((bpred_scheme != NO_BPRED) && cond_bbi(e->src))
										  printf(" %d(m)", mpred_times[edge_id][hm]);
								printf("\n");
					 }
		  }
		  printf("\n");
}



		  void
dump_unit_time(int edge_id, int hm, int bp)
{
		  int		t;
		  tcfg_edge_t	*e = tcfg_edges[edge_id];

		  if (bp == BP_CPRED)
					 t = cpred_times[edge_id][hm];
		  else
					 t = mpred_times[edge_id][hm];
		  printf("    %d.%d -> %d.%d (hm:%d bp:%d): %d\n",
								bbi_pid(e->src), bbi_bid(e->src), bbi_pid(e->dst), bbi_bid(e->dst),
								hm, bp, t);
}



		  void
dump_edge_mp_times(int edge_id, int hm)
{
		  int		*t, set;
		  tcfg_edge_t	*e = tcfg_edges[edge_id];
		  tcfg_node_t	*bbi = e->dst;
		  loop_t	*lp;

		  t = mp_times[edge_id][hm];
		  printf("    %d.%d -> %d.%d (hm:%d): ",
								bbi_pid(e->src), bbi_bid(e->src), bbi_pid(e->dst), bbi_bid(e->dst), hm);
		  for (set = 0; set < cache.ns; set++) {
					 if (t[set] != 0)
								printf(" s[%d]=%d", set, t[set]);
		  }
		  printf("\n");
}



		  void
dump_mp_times()
{
		  tcfg_edge_t	*e;
		  loop_t	*lp;
		  int		edge_id, hm, num_hm, set, *t;

		  printf("dump mispred timing estimates for basic blocks\n");
		  for (edge_id = 0; edge_id < num_tcfg_edges; edge_id++) {
					 e = tcfg_edges[edge_id];
					 printf("%d.%d -> %d.%d\n",
										  bbi_pid(e->src), bbi_bid(e->src), bbi_pid(e->dst), bbi_bid(e->dst));
					 num_hm = num_hit_miss[tcfg_edges[edge_id]->dst->id];
					 for (hm = 0; hm < num_hm; hm++) {
								lp = bbi_hm_list[e->dst->id][hm];
								if (lp == NULL)
										  continue;
								printf("    hm[%d]:", hm);
								t = mp_times[edge_id][hm];
								for (set = 0; set < cache.ns; set++) {
										  if (t[set] != 0)
													 printf(" s[%x]=%d", set, t[set]);
								}
								printf("\n");
					 }
		  }
		  printf("\n");
}



		  void
dump_plog_stats()
{
		  int		    i;
		  code_link_t	    *p;
		  int		    len_stat[48];

		  printf("dump prolog len statistics\n");
		  for (i = 0; i < 48; i++)
					 len_stat[i] = 0;

		  for (i = 0; i < num_tcfg_edges; i++) {
					 for (p = prologs[i]; p != NULL; p = p->next) {
								len_stat[p->num_inst]++;
					 }
		  }

		  for (i = 0; i < 48; i++) {
					 if (len_stat[i] > 0)
								printf("len[%d]: %d\n", i, len_stat[i]);

		  }
}



		  void
dump_elog_stats()
{
		  int		    i;
		  code_link_t	    *p;
		  int		    len_stat[32];

		  printf("dump epilog len statistics\n");
		  for (i = 0; i < 32; i++)
					 len_stat[i] = 0;

		  for (i = 0; i < num_tcfg_edges; i++) {
					 for (p = epilogs[i]; p != NULL; p = p->next) {
								len_stat[p->num_inst]++;
					 }
		  }

		  for (i = 0; i < 32; i++) {
					 if (len_stat[i] > 0)
								printf("len[%d]: %d\n", i, len_stat[i]);

		  }
}



		  void
dump_context_stats()
{
		  code_link_t	    *p;
		  tcfg_edge_t	    *e;
		  int		    i, *num_plogs, *num_elogs, num_ctxs, total = 0;

		  dump_plog_stats();
		  dump_elog_stats();

		  num_plogs = (int *) calloc(num_tcfg_nodes, sizeof(int));
		  num_elogs = (int *) calloc(num_tcfg_nodes, sizeof(int));

		  printf("dump context statistics\n");
		  for (i = 0; i < num_tcfg_nodes; i++) {
					 for (e = tcfg[i]->in; e != NULL; e = e->next_in) {
								for (p = prologs[e->id]; p != NULL; p = p->next)
										  num_plogs[i]++;
					 }
					 for (e = tcfg[i]->out; e != NULL; e = e->next_out) {
								for (p = epilogs[e->id]; p != NULL; p = p->next)
										  num_elogs[i]++;
					 }
					 num_ctxs = max(num_plogs[i], 1) * max(num_elogs[i], 1);
					 printf("%3d: (%3d, %3d/%2d) %6d\n", i, num_plogs[i], num_elogs[i], max_elog_len[i], num_ctxs);
					 total += num_ctxs;
		  }
		  printf("\n-----------------------\ntotal: %d\n", total);

		  free(num_plogs);
		  free(num_elogs);
}



		  void
dump_elog_len()
{
		  int		    i;

		  printf("dump max_elog_len\n");
		  for (i = 0; i < num_tcfg_nodes; i++) {
					 printf("bbi[%d]/%x: %d\n", i, tcfg[i]->bb->sa, max_elog_len[i]);
		  }
}



		  void
dump_mlat_mpinst()
{
		  int		    i;

		  printf("dump mlat_mpinst\n");
		  for (i = 0; i < num_tcfg_edges; i++) {
					 printf("edge[%d->%d]/%x->%x: %d\n", tcfg_edges[i]->src->id,
										  tcfg_edges[i]->dst->id, tcfg_edges[i]->src->bb->sa,
										  tcfg_edges[i]->dst->bb->sa, mlat_mpinst[i]);
		  }
}
