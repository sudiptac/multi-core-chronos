
/*******************************************************************************
 *
 * Chronos: A Timing Analyzer for Embedded Software
 * =============================================================================
 * http://www.comp.nus.edu.sg/~rpembed/chronos/
 *
 * Copyright (C) 2005 Xianfeng Li
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * $Id: loops.h,v 1.2 2006/06/24 08:54:57 lixianfe Exp $
 *
 ******************************************************************************/


#ifndef LOOPS_H
#define LOOPS_H


#include "tcfg.h"

#define MAX_LOOP_NEST	32
#define LOOP_ENTRY	0
#define LOOP_EXIT	1


// data structure for loop information
typedef struct loop_t	loop_t;

struct loop_t {
		  int		    id;
		  tcfg_node_t	    *head;	// [head, tail]
		  tcfg_node_t	    *tail;
		  tcfg_edge_t		*back_edge;
		  loop_t	    *parent;
		  tcfg_elink_t    *exits;
		  int		num_child;
		  int 		proc_lpid;		/* procedure relative loop id, added by sudiptac */
		  int		   flags;
		  int			loop_exec_count;	
		  int			wcet_lp;			/* required for path-based analysis */
		  int 		loopbound;
		  int 		*num_context; 			/* number of bus context that can reach loop entry from an outer loop context */
		  int 		total_num_context; 		/* total number of bus context that can reach loop entry */
		  int		parent_prev_num_context;/* total_num_context of the parent loop in the previous iteration of the parent loop */
		  int		cur_iter;

		  void* graph;
		  void* wcet_graph;
		  void* bcet_graph;
};


void find_loops();
void map_bbi_loop();
void loop_relations();
int tcfg_is_loop_head(tcfg_node_t* bbi);

int num_tcfg_loops;
loop_t** loops;
loop_t	    **loop_map;		// bbi => loop
loop_t***	loop_comm_ances;
#endif
