#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tcfg.h"
#include "isa.h"
#include "cache.h"
#include "crpd.h"
#include "loops.h"
#include "common.h"
//#include "pathwcet.h"
#include "busconfig.h"
#include "sharedbus.h"
#include "task.h"

char** inst_age_l1;
char** inst_age_l2;
int miss_lat[MAX_CACHE_LEVELS];
static hashtab_p* g_ecb_hashtab;
static int** all_ucbs;
static int** lub;
static Queue queue;
static char* visited;
static char** affectors;
static int number_of_ecbs_l1 = 0;
static int number_of_ecbs_l2 = 0;
static int max_ucb_l1_l2 = 0;
static int max_ucb_l1_not_l2 = 0;
static int max_ucb_not_l1_l2 = 0;
static int total_max_bdelay = 0;
static int max_max_bdelay = 0;
/* FIXME: remove this (just to compile) */
int* wtime;

extern tcfg_node_t** tcfg;
extern int num_tcfg_nodes;
extern prog_t	prog;
extern int enable_il2cache;
extern int assoc, nsets, bsize, cache_il2_lat;
extern int assoc_l2, nsets_l2, bsize_l2, mem_lat[2];
extern cache_t  cache;
extern cache_t  cache_l2;
extern int number_of_mc_calls;

void set_ind_disturbance(mucb_p entry, mucb_p ecb_entry, int bbi_id);

/* static type , not to be exported */
typedef struct temp_hold {
		  int mem_blk_id;
		  int dcrt;
		  struct temp_hold* next;
} mem_blk_holder;

#ifdef _DEBUG_CRPD
/* print hit-miss classification of all iinstructions */
void print_classification()
{
		  tcfg_node_t* bbi;
		  de_inst_t* inst;
		  int i, j;

		  for(i = 0; i < num_tcfg_nodes; i++)
		  {
					 inst = tcfg[i]->bb->code;

					 printf("---------------------------------------------------------------------------------------------------------------\n");

					 for(j = 0; j < tcfg[i]->bb->num_inst; j++)
					 {
								if(inst_chmc_l1[i][j] == ALL_HIT)
										  printf("BBI(%d) start address = 0x%x, Instruction address = 0x%x, CHMC = AH (age = %d)\n", i, tcfg[i]->bb->sa, \
																inst->addr, inst_age_l1[i][j]);
								else if(inst_chmc_l1[i][j] == ALL_MISS)
										  printf("BBI(%d) start address = 0x%x, Instruction address = 0x%x, CHMC = AM\n", i, tcfg[i]->bb->sa, inst->addr);
								else if(inst_chmc_l1[i][j] == PS)
										  printf("BBI(%d) start address = 0x%x, Instruction address = 0x%x, CHMC = PS\n", i, tcfg[i]->bb->sa, inst->addr);
								else if(inst_chmc_l1[i][j] == NOT_CLASSIFIED)
										  printf("BBI(%d) start address = 0x%x, Instruction address = 0x%x, CHMC = NC\n", i, tcfg[i]->bb->sa, inst->addr);
								else
										  printf("BBI(%d) start address = 0x%x, Should not come here %x\n", i, tcfg[i]->bb->sa, inst->addr);

								if(enable_il2cache) {
										  if(inst_chmc_l2[i][j] == ALL_HIT)
													 printf("BBI(%d) start address = 0x%x, Instruction address = 0x%x, CHMC = AH (age = %d)\n", i, tcfg[i]->bb->sa, \
																		  inst->addr, inst_age_l2[i][j]);
										  else if(inst_chmc_l2[i][j] == ALL_MISS)
													 printf("BBI(%d) start address = 0x%x, Instruction address = 0x%x, CHMC = AM\n", i, tcfg[i]->bb->sa, inst->addr);
										  else if(inst_chmc_l2[i][j] == PS)
													 printf("BBI(%d) start address = 0x%x, Instruction address = 0x%x, CHMC = PS\n", i, tcfg[i]->bb->sa, inst->addr);
										  else if(inst_chmc_l2[i][j] == NOT_CLASSIFIED)
													 printf("BBI(%d) start address = 0x%x, Instruction address = 0x%x, CHMC = NC\n", i, tcfg[i]->bb->sa, inst->addr);
										  else if(inst_chmc_l2[i][j] == ALL_X)
													 printf("BBI(%d) start address = 0x%x, Instruction address = 0x%x, CHMC = AX\n", i, tcfg[i]->bb->sa, inst->addr);
										  else
													 printf("BBI(%d) start address = 0x%x, Should not come here %x\n", i, tcfg[i]->bb->sa, inst->addr);
								}

								printf("---------------------------------------------------------------------------------------------------------------\n");

								inst++;
					 }
		  }
}

/* Dump the cache state */
/* All for debugging purposes */

/* Dump cache states of a single basic block (at 
 * exit point) */
void dumpCacheBB(acs_p** acs, FILE* fp)
{
		  int i,k;
		  mem_blk_set_t* iter;

		  if(!acs)
					 return;

		  for(k = 0; k < nsets; k++)
		  {
					 for(i = 0; i <= assoc; i++)
					 {
								fprintf(fp, "*************************************\n");  
								fprintf(fp, "Printing cache (set,block) (%d,%d)\n", k,i);  
								fprintf(fp, "*************************************\n");  
								if(!acs[k])
										  fprintf(fp, "{empty}\n");
								else if(!acs[k][i])
										  fprintf(fp, "{empty}\n");
								else if(!acs[k][i]->mem_blk_h)  
										  fprintf(fp, "{empty}\n");
								else {
										  for(iter = acs[k][i]->mem_blk_h; iter; iter = iter->next)
										  {
													 fprintf(fp, "Memory Block %x,", iter->block);	 
										  }

										  fprintf(fp, "\n");
								}
					 }
		  }	
}

/* Dump cache states of all basic blocks (at exit
 * point) */
void dumpCache()
{
		  int i;
		  FILE* fp;

		  fp = fopen("cache.dump", "w");
		  if(!fp)
					 printf("Internal file opening failed\n");  

		  for(i = 0; i < num_tcfg_nodes; i++)
		  {
#if 0
					 de_inst_t* l_inst;
					 /* Go to last instruction */
					 l_inst = tcfg[i]->bb->code + tcfg[i]->bb->num_inst - 1;
#endif
					 fprintf(fp, "=====================================\n");
					 fprintf(fp, "At tcfg block %d\n", i);
					 fprintf(fp, "=====================================\n");
					 dumpCacheBB(tcfg[i]->acs_out, fp);
		  }

		  fclose(fp);
}

/* Dump instruction cache states of all basic blocks 
 * (at exit point) */
void dumpInstCache()
{
		  int i;
		  FILE* fp;

		  fp = fopen("inst_cache.dump", "w");
		  if(!fp)
					 printf("Internal file opening failed\n");  

		  for(i = 0; i < num_tcfg_nodes; i++)
		  {
					 de_inst_t* l_inst;
					 /* Go to last instruction */
					 l_inst = tcfg[i]->bb->code + tcfg[i]->bb->num_inst - 1;
					 /* l_inst = tcfg[i]->bb->code ; */
					 fprintf(fp, "=====================================\n");
					 fprintf(fp, "At tcfg block %d\n", i);
					 fprintf(fp, "=====================================\n");
					 dumpCacheBB(tcfg[i]->acs_out, fp);
		  }

		  fclose(fp);
}
#endif

#ifdef _DEBUG_CRPD
static void dump_mem_blk_hashtab(hashtab_p* mem_blk_hashtab)
{
		  int i;
		  hashtab_p iter;
		  mucb_p entry;
		  int count = 0;

		  fprintf(stdout, "\n----------------------------------------------------------\n");
		  fprintf(stdout, "printing the memory block related hash table\n");
		  fprintf(stdout, "----------------------------------------------------------\n");

		  for (i = 0; i < HASH_TAB_SIZE; i++)
		  {
					 fprintf(stdout, "HASHTAB id [%d]->", i);

					 if(mem_blk_hashtab[i] == NULL) {
								fprintf(stdout, "(empty)\n\n");
								continue;
					 }

					 for (iter = mem_blk_hashtab[i]; iter; iter = iter->next)
					 {
								entry = (mucb_p) iter->entry;

								fprintf(stdout, "[mem=0x%x, ", entry->mem_blk_id);
								fprintf(stdout, "L1=%d, ", entry->age[0]);
								fprintf(stdout, "L2=%d, ", entry->age[1]);
								fprintf(stdout, "bbi=%d, ", entry->bbi_id);
								fprintf(stdout, "dcrt=%d, ", entry->dcrt);
								fprintf(stdout, "icrt=%d, ", entry->icrt);
								fprintf(stdout, "offset=%d]", entry->offset);
								fprintf(stdout, "->");
								count++;
					 }
					 
					 fprintf(stdout, "NULL\n\n");
		  }
		  fprintf(stdout, "----------------------------------------------------------\n");
		  fprintf(stdout, "End printing the memory block hash table\n");
		  fprintf(stdout, "Total elements = %d\n", count);
		  fprintf(stdout, "----------------------------------------------------------\n");
}
#endif

/************************* HASH TABLE related routines start **********************/

/* shift and rotate hash function */
static int hash32shift(int key)
{
		  key = ~key + (key << 15); /* key = (key << 15) - key - 1; */
		  key = key ^ (key >> 12);
		  key = key + (key << 2);
		  key = key ^ (key >> 4);
		  key = key * 2057; 			/* key = (key + (key << 3)) + (key << 11); */
		  key = key ^ (key >> 16);
  
		  return key;
}

/* call a hash function here */
static int get_hash_key(int mem_blk_id)
{
		  return hash32shift(mem_blk_id) % HASH_TAB_SIZE;
}

/* search an entry in the hash table */
static hashtab_p found_in_hashtab(hashtab_p* mem_blk_hashtab, int mem_blk_id, int bbi_id)
{
		  int hashkey;
		  hashtab_p iter;
		  mucb_p entry;

		  hashkey = get_hash_key(mem_blk_id);
		  assert(hashkey < HASH_TAB_SIZE);

		  for (iter = mem_blk_hashtab[hashkey]; iter; iter = iter->next)
		  {
					 entry = (mucb_p) iter->entry;
					 
					 /* check combination of basic block and memory block */
					 if (entry->mem_blk_id == mem_blk_id /*&& (entry->bbi_id == bbi_id || entry->bbi_id == -1)*/)
					 //if (entry->mem_blk_id == mem_blk_id && (entry->bbi_id == bbi_id || entry->bbi_id == -1))
								return iter;
		  }
		  
		  return NULL;
}

/* add an entry to the hastable */
void add_in_hashtab(hashtab_p* mem_blk_hashtab, int bbi_id, int mem_blk_id, int offset, int age_l1, int age_l2)
{
		  int hashkey;
		  hashtab_p head;
		  mucb_p entry;

		  /* if already in the hashtable, return */
		  if(found_in_hashtab(mem_blk_hashtab, mem_blk_id, bbi_id))
					 return;

		  hashkey = get_hash_key(mem_blk_id);
		  assert(hashkey < HASH_TAB_SIZE);
		  
		  head = (hashtab_p) calloc(1, sizeof(hashtab_s));
		  CHECK_MEM(head);
		  
		  entry = (mucb_p) calloc(1, sizeof(mucb_s));
		  CHECK_MEM(entry);
		  
		  entry->mem_blk_id = mem_blk_id;
		  entry->age[0] = age_l1;
		  entry->age[1] = age_l2;
		  entry->indisturb[0] = 0;
		  entry->indisturb[1] = -1;
		  entry->bbi_id = bbi_id;
		  entry->offset = offset;

		  head->entry = entry;
		  head->next = mem_blk_hashtab[hashkey];
		  mem_blk_hashtab[hashkey] = head;
}

/* build the memory block related hash table */
static void build_mem_blk_hashtab(hashtab_p* mem_blk_hashtab)
{
		  int i,j;
		  de_inst_t* inst;
		  int mem_blk_id;
		  int delay = 0;
		  int offset = 0;

		  for (i = 0; i < num_tcfg_nodes; i++)
		  {
					 inst = tcfg[i]->bb->code;
					 delay = 0;

					 for (j = 0; j < tcfg[i]->bb->num_inst; j++)
					 {
								if (g_shared_bus_type != NOBUS) {
										  delay++;
										  if (inst_chmc_l2[i][j] == ALL_HIT) {
													 int offset = (wtime[i] + delay) % (fx_slot_length * max_cores);
													 delay += compute_bus_delay(offset, miss_lat[0]);
										  } else {
													 int offset = (wtime[i] + delay) % (fx_slot_length * max_cores);
													 delay += compute_bus_delay(offset, miss_lat[0] + miss_lat[1]);
										  }
										  offset = (wtime[i] + delay) % (fx_slot_length * max_cores);
								}
								mem_blk_id = GET_MEM_BLK(inst->addr);
								/* search the hash table. if not found add the data */
								/* uniqueness is searched inside add entry */
								//add_in_hashtab(mem_blk_hashtab, i, mem_blk_id, CINFTY, CINFTY);
								add_in_hashtab(mem_blk_hashtab, -1, mem_blk_id, offset, CINFTY, CINFTY);

								inst++;
					 }
		  }
}

/* reset the age of all memory blocks to INFTY */
static void reset_hashtab(hashtab_p* mem_blk_hashtab)
{
		  int i;
		  hashtab_p iter;
		  mucb_p entry;

		  for (i = 0; i < HASH_TAB_SIZE; i++)
		  {
					 for (iter = mem_blk_hashtab[i]; iter; iter = iter->next)
					 {
								entry = (mucb_p)iter->entry;
								entry->age[0] = CINFTY;	/* mimic INFTY */
								entry->age[1] = CINFTY;
					 }
		  }
}

/* initialize the hash table */
hashtab_p* init_hashtab()
{
		  int i;
		  hashtab_p* newTab;

		  newTab = (hashtab_p *) calloc (HASH_TAB_SIZE, sizeof(hashtab_p));
		  CHECK_MEM(newTab);

		  return newTab;
}

/************************* HASH TABLE related routines end **********************/

/* compute static TDMA offsets to compute the effect on CRPD */
void compute_structural_TDMA_offsets() 
{
		  int i, j;
		  tcfg_node_t* bbi;
		  tcfg_edge_t* edge;
		  int* f_offsets;
		  int delay;

		  f_offsets = (int *) calloc (num_tcfg_nodes, sizeof(int));
		  CHECK_MEM(f_offsets);

		  memset(wtime, 0, num_tcfg_nodes * sizeof(int));

		  for (i = 1; i < num_tcfg_nodes; i++)
		  {
					 int topo_id = topo_tcfg[i];
					 bbi = tcfg[topo_id];
					 delay = 0;

					 if (tcfg_is_loop_head(bbi))
								wtime[bbi->id] = 0;
					 else {
								for (edge = bbi->in; edge; edge = edge->next_in) 
								{
										  if (wtime[bbi->id] < f_offsets[edge->src->id])
													 wtime[bbi->id] = f_offsets[edge->src->id];			 
								}
								/* FIXME: cost computation */
								delay += bbi->bb->num_inst;
								for (j = 0; j < bbi->bb->num_inst; j++)
								{
										  if (inst_chmc_l1[topo_id][j] == ALL_HIT)
													 continue;
										  if (inst_chmc_l2[topo_id][j] == ALL_HIT) {
													 int offset = (wtime[bbi->id] + delay) % (fx_slot_length * max_cores);
													 delay += compute_bus_delay(offset, miss_lat[0]);
										  } else {
													 int offset = (wtime[bbi->id] + delay) % (fx_slot_length * max_cores);
													 delay += compute_bus_delay(offset, miss_lat[0] + miss_lat[1]);
										  }
								}
								f_offsets[bbi->id] = wtime[bbi->id] + delay;
					 }
		  }

		  free(f_offsets);
}

/* initialize all UCB structures */
void init_all_ucbs()
{
		  int i, j;
		  int id = 0;

		  all_ucbs = (int **) calloc (assoc_l2 + 1, sizeof(int));
		  CHECK_MEM(all_ucbs);
		  
		  for (i = 0; i < assoc_l2 + 1; i++)
		  {
					 all_ucbs[i] = (int *) calloc (assoc_l2 + 1, sizeof(int));
					 CHECK_MEM(all_ucbs[i]);
		  }

		  for (i = 0; i < assoc_l2 + 1; i++)
		  {
					 for (j = 0; j < assoc_l2 + 1; j++)
					 {
								all_ucbs[i][j] = id++;
					 }
		  }

		  lub = (int **) calloc (id, sizeof(int));
		  CHECK_MEM(lub);
		  
		  for (i = 0; i < id; i++)
		  {
					 lub[i] = (int *) calloc (id, sizeof(int));
					 CHECK_MEM(lub[i]);
		  }
		  
		  for (i = 0; i < id; i++)
		  {
					 for (j = 0; j < id; j++)
					 {
								lub[i][j] = -1;
					 }
		  }
}

/* get the evicted block hash table */
hashtab_p* get_ecb_hashtab()
{
		  return g_ecb_hashtab;
}


/* build the evicted cache block hash table */
void build_ecb_hashtab(int preempt_task)
{
		  int i, j;
		  de_inst_t* inst;
		  int mem_blk_id;
		  int line_l1, line_l2, ecb_l1, ecb_l2;

		  g_ecb_hashtab = init_hashtab();

		  for (i = 0; i < num_tcfg_nodes; i++)
		  {
					 inst = tcfg[i]->bb->code;

					 for (j = 0; j < tcfg[i]->bb->num_inst; j++)
					 {
								mem_blk_id = GET_MEM_BLK(inst->addr);
								line_l1 = GET_SET_L1(inst->addr);	
								line_l2 = GET_SET_L2(inst->addr);	
								ecb_l1 = tasks[preempt_task]->ecb_log_l1[line_l1];
								ecb_l2 = tasks[preempt_task]->ecb_log_l2[line_l2];
								add_in_hashtab(g_ecb_hashtab, -1, mem_blk_id, 0, ecb_l1, ecb_l2);
								inst++;
					 }
		  }
}

/* count number of ECBs */
void set_count_ECB(int preempt_task)
{
		  int i;
		  number_of_ecbs_l1 = 0;
		  number_of_ecbs_l2 = 0;

		  for (i = 0; i < nsets; i++)
		  {
					 if(tasks[preempt_task]->ecb_log_l1[i] > assoc)
								number_of_ecbs_l1 += assoc;
					 else
								number_of_ecbs_l1 += tasks[preempt_task]->ecb_log_l1[i];
		  }
		  for (i = 0; i < nsets_l2; i++)
		  {
					 if(tasks[preempt_task]->ecb_log_l2[i] > assoc_l2)
								number_of_ecbs_l2 += assoc_l2;
					 else
								number_of_ecbs_l2 += tasks[preempt_task]->ecb_log_l2[i];
		  }
}

#if 0
/* use model checking to refine one evicted cache block (given as the set 
 * number) */
void refine_one_ecb(mucb_p entry)
{
		  char try_age;
		  char age = entry->age[0];	/* try only the level 1 cache (vanilla CRPD) */
		  int ret;
		  int line_l1 = GET_SET_L1(entry->mem_blk_id * bsize);	

		  if (age >= assoc) {
					 try_age = assoc - 1;
		  }
		  else {
					 try_age = age - 1;
		  }

		  for (; try_age >= 0; try_age--) 
		  {
					 ret = try_ECB_refinement_with_MC(tasks[0], entry->mem_blk_id, try_age);
					 
					 if (!ret) {
#ifdef _DEBUG_AI_MC
								fprintf(stdout, "MC refinement successful in task %s ECB 0x%x\n", \
										  tasks[0]->name, entry->mem_blk_id);
#endif
								/* refine ECB parameters */
								entry->age[0] = try_age;
								tasks[0]->ecb_log_l1[line_l1] = try_age;
					 } else {
#ifdef _DEBUG_AI_MC
								fprintf(stdout, "NO MC refinement possible in task %s ECB 0x%x\n", \
										  tasks[0]->name, entry->mem_blk_id);
#endif
					 }
					 cache_mc_result(entry->mem_blk_id * bsize, ret, try_age, 0);
		  }
}

/* If possible, refine the number of evicted cache blocks by model checking */
void refine_ecbs_with_MC()
{
		  int i;
		  hashtab_p* ecb_hashtab;
		  hashtab_p iter;
		  mucb_p ecb_entry;

		  ecb_hashtab = get_ecb_hashtab();

		  for ( i = 0; i < HASH_TAB_SIZE; i++)
		  {
					 for (iter = ecb_hashtab[i]; iter; iter = iter->next) 
					 {
								ecb_entry = (mucb_p)iter->entry;
								refine_one_ecb(ecb_entry);
					 }
		  }
}
#endif

/* set miss latencies */
void init_crpd(int preempt_task)
{
		  if (getenv("IGNORE_L2")) {
					 miss_lat[0] = cache_il2_lat + mem_lat[0]; 
					 miss_lat[1] = 0;
		  } else {
					 miss_lat[0] = cache_il2_lat;
					 miss_lat[1] = mem_lat[0];
		  }

		  /* build the evicted cache block hash table */
		  build_ecb_hashtab(preempt_task);
}

int top_entry(char* age)
{
		  return (age[0] == -1 && age[1] == -1);
}

int bottom_entry(char* age)
{
		  return (age[0] == CINFTY && age[1] == CINFTY);
}

int zero_entry(char* age)
{
		  return (age[0] == 0 && age[1] == 0);
}

char* boxplus(char* arg1, char* arg2)
{
		  char* result = calloc (MAX_CACHE_LEVELS, sizeof(int));
		  CHECK_MEM(result);

		  if (arg1[0] + arg2[0] < assoc)
					 result[0] = arg1[0] + arg2[0];
		  else
					 result[0] = CINFTY;
		  
		  if (arg1[1] + arg2[1] < assoc_l2)
					 result[1] = arg1[1] + arg2[1];
		  else
					 result[1] = CINFTY;

		  return result;
}

int dist(char* arg)
{
		  int i;

		  for (i = 0; i < MAX_CACHE_LEVELS; i++)
		  {
					 if (arg[i] != CINFTY)
								return i;
		  }

		  return MAX_CACHE_LEVELS;
}

/* compute the delay introduced by ecb_entry (evicted cache block) on 
 * entry (useful cache blocks) */
void set_delay(mucb_p entry, mucb_p ecb_entry, mem_blk_holder** hold)
{
		  int i;
		  mem_blk_holder* iter;

#ifdef _NDEBUG_CRPD
			fprintf(stdout, "[U1=%d,U2=%d,E1=%d,E2=%d]\n", entry->age[0],entry->age[1], \
				ecb_entry->age[0], ecb_entry->age[1]);	
#endif

		  entry->dcrt = 0;

		  /* CASE I */
		  if (top_entry(entry->age) && zero_entry(ecb_entry->age)) {
					 entry->dcrt = 0;
					 return;
		  }

		  /* CASE II */
		  if (bottom_entry(entry->age)) {
					 entry->dcrt = 0;
					 return;
		  }

		  /* CASE III */
		  if (!top_entry(entry->age)) {
					 int low = dist(entry->age);
					 char* arg  = boxplus(entry->age, ecb_entry->age);
					 int high = dist(arg);
					 
					 entry->dcrt = 0;

					 for (i = low; i <= high-1; i++)
					 {
								entry->dcrt += miss_lat[i];			
					 }

					 free(arg);

					 for (iter = *hold; iter; iter = iter->next) 
					 {
								if (iter->mem_blk_id == entry->mem_blk_id && 
										  entry->dcrt > iter->dcrt) {
										  iter->dcrt = entry->dcrt;
										  break;
								}
					 }
					 if (!iter) {
								mem_blk_holder* node = (mem_blk_holder *) calloc (1, sizeof(mem_blk_holder));
								CHECK_MEM(node);
								node->dcrt = entry->dcrt;
								node->mem_blk_id = entry->mem_blk_id;
								node->next = *hold;
								*hold = node;
					 }
					 
					 return;
		  }

		  /* CASE IV */
		  for (i = 0; i < MAX_CACHE_LEVELS; i++)
					 entry->dcrt += miss_lat[i];
		  
		  for (iter = *hold; iter; iter = iter->next) 
		  {
					 if (iter->mem_blk_id == entry->mem_blk_id && 
								entry->dcrt > iter->dcrt) {
								iter->dcrt = entry->dcrt;
								break;
					 }
		  }
		  if (!iter) {
					 mem_blk_holder* node = (mem_blk_holder *) calloc (1, sizeof(mem_blk_holder));
					 CHECK_MEM(node);
					 node->dcrt = entry->dcrt;
					 node->mem_blk_id = entry->mem_blk_id;
					 node->next = *hold;
					 *hold = node;
		  }
		  
		  return;
}

/* compute direct effect of preemption at the entry of basic block "i" */
int compute_direct_preemption_at_point(int bbi_id)
{
		  int i;
		  hashtab_p head, ecb_head;
		  mucb_p entry, ecb_entry;
		  tcfg_node_t* bbi = tcfg[bbi_id];
		  hashtab_p* ecb_hashtab;
		  int total_dcrt = 0;
		  mem_blk_holder* hold = NULL;
		  mem_blk_holder* iter;
		  int ucb_l1_l2 = 0;
		  int ucb_l1_not_l2 = 0;
		  int ucb_not_l1_l2 = 0;

		  /* get the evicted cache blocks from the preempting task */
		  ecb_hashtab = get_ecb_hashtab();

		  for (i = 0; i < HASH_TAB_SIZE; i++)
		  {
					 for (head = bbi->mem_blk_hashtab_in[i]; head; head = head->next)
					 {
								entry = (mucb_p) head->entry;
								ecb_head = found_in_hashtab(ecb_hashtab, entry->mem_blk_id, -1);		
								ecb_entry = (mucb_p) ecb_head->entry;
								set_delay(entry, ecb_entry, &hold);
								
								/* count UCBs */
								if (entry->age[0] != CINFTY && entry->age[1] != CINFTY)
										  ucb_l1_l2++;
								else if (entry->age[0] < CINFTY && entry->age[1] == CINFTY)
										  ucb_l1_not_l2++;
								else if (entry->age[0] == CINFTY && entry->age[1] < CINFTY)
										  ucb_not_l1_l2++;

#ifdef _NDEBUG_CRPD
								if(entry->dcrt > 0) {
										  fprintf(stdout, "delay %d set for L1 set %d@%d@0x%x[%d,%d](%d)\n", entry->dcrt, \
													 GET_SET_L1(entry->mem_blk_id * bsize), \
													 bbi_id, entry->mem_blk_id, entry->age[0], entry->age[1], entry->bbi_id);
								}
#endif
					 }
		  }

		  for (iter = hold; iter; iter = iter->next)
					 total_dcrt += iter->dcrt;

		  free(hold);

		  if (max_ucb_l1_l2 < ucb_l1_l2)
					 max_ucb_l1_l2 = ucb_l1_l2;
		  if (max_ucb_l1_not_l2 < ucb_l1_not_l2)
					 max_ucb_l1_not_l2 = ucb_l1_not_l2;
		  if (max_ucb_not_l1_l2 < ucb_not_l1_l2)
					 max_ucb_not_l1_l2 = ucb_not_l1_l2;

		  return total_dcrt;
}

/* compute direct effect of preemption */
int compute_direct_preemption_delay()
{
		  int i;
		  int max_dcrt = 0;
		  int total_dcrt;

		  for (i = 0; i < num_tcfg_nodes; i++)
		  {
					 total_dcrt = compute_direct_preemption_at_point(i);

					 if (max_dcrt < total_dcrt)
								max_dcrt = total_dcrt;
		  }

		  return max_dcrt;

}

/* check whether bbi_id can be followed by affector_id */
int check_affector(int bbi_id, int affector_id)
{
		  tcfg_edge_t* edge;
		  tcfg_node_t* bbi;
		  loop_t* lp;
		  
		  clear_queue(&queue);
		  enqueue(&queue, &tcfg[affector_id]);
		  memset(visited, 0, num_tcfg_nodes * sizeof(char));	  
		//  printf("check affector visited\n");
		  
		  while (!queue_empty(&queue)) 
		  {
					 bbi = *((tcfg_node_t **) dequeue(&queue));	 		 
					 
					 if (visited[bbi->id])
								continue;

					 visited[bbi->id] = 1;

					 if (bbi->id == bbi_id)
								return 1;

					 for (edge = bbi->out; edge; edge = edge->next_out)
					 {
								lp = loop_map[bbi->id];
								
								/* handle back edges */
								if (edge->dst == lp->head)
										  continue;

								if (!visited[edge->dst->id])
								{
										  enqueue(&queue, &tcfg[edge->dst->id]);
								}
					 }
		  }

		  return 0;
}

/* build affector matrix */
void build_affector_matrix()
{
		  int i, j;

		  affectors = (char **) calloc (num_tcfg_nodes, sizeof(char *));
		  CHECK_MEM(affectors);

		  for (i = 0; i < num_tcfg_nodes; i++)
		  {
					 affectors[i] = (char *) calloc (num_tcfg_nodes, sizeof(char));
					 CHECK_MEM(affectors[i]);
		  }

		  for (i = 0; i < num_tcfg_nodes; i++)
		  {
					 for (j = 0; j < num_tcfg_nodes; j++)
					 {
								if (j == i) continue;

								if (affectors[i][j] || affectors[j][i]) continue;
								
								if (check_affector(i,j))
										  affectors[i][j] = 1;
					 }
		  }
}

/* compute the indirect effect of preemption created in different cache levels */
char* find_ind_disturbance(mucb_p entry, int bbi_id)
{
		  tcfg_node_t* bbi = tcfg[bbi_id];
		  hashtab_p head, data_h;
		  mucb_p entry_h, ecb_entry_h;
		  int bbi_f;
		  char* disturbance;
		  char* disturbance_h;
		  int i;

		  disturbance = (char *) calloc (MAX_CACHE_LEVELS, sizeof(char));
		  CHECK_MEM(disturbance);

		  for ( i = 0; i < HASH_TAB_SIZE; i++)
		  {
					 for (head = bbi->mem_blk_hashtab_in[i]; head; head = head->next)
					 {
								entry_h = (mucb_p) head->entry;
								
								/* ignore itself */
								if (entry_h == entry)
										  continue;

								/* ignore entries which cannot create disturbance */
								if(bottom_entry(entry_h->age))
										  continue;
								
								/* check follower in same basic block */
								if (entry->bbi_id == entry_h->bbi_id && entry->mem_blk_id < entry_h->mem_blk_id)
										  continue;

								if (GET_SET_L1(entry->mem_blk_id * bsize) != GET_SET_L1(entry_h->mem_blk_id * bsize) 
										  && GET_SET_L2(entry->mem_blk_id * bsize) != GET_SET_L2(entry_h->mem_blk_id * bsize)) 
										  continue;
								
								/* check follower in different basic block */
								if (entry->bbi_id != entry_h->bbi_id && !affectors[entry->bbi_id][entry_h->bbi_id])
										  continue;
								
								/* identical memory blocks are not considered */
								if (entry->mem_blk_id == entry_h->mem_blk_id) 
										  continue;

								/* all entries coming here are subjected to create disturbance on entry 
								 * if they map to same cache set */

								/* L1 cache */
								/*if (GET_SET_L1(entry->mem_blk_id * bsize) == GET_SET_L1(entry_h->mem_blk_id * bsize)) {
										  if (entry_h->indisturb[0] != -1)
													 disturbance[0] += entry_h->indisturb[0];
										  else {
													 data_h = found_in_hashtab(g_ecb_hashtab, entry_h->mem_blk_id, -1);			 
													 ecb_entry_h = (mucb_p)data_h->entry;
													 set_ind_disturbance(entry_h, ecb_entry_h, bbi_id);
													 disturbance[0] += entry_h->indisturb[0];
										  }
								}*/
								
								/* L2 cache */
								if (GET_SET_L2(entry->mem_blk_id * bsize) == GET_SET_L2(entry_h->mem_blk_id * bsize)) {
										  if (entry_h->indisturb[1] != -1)
													 disturbance[1] += entry_h->indisturb[1];
										  else {
													 data_h = found_in_hashtab(g_ecb_hashtab, entry_h->mem_blk_id, -1);			 
													 ecb_entry_h = (mucb_p)data_h->entry;
													 set_ind_disturbance(entry_h, ecb_entry_h, bbi_id);
													 disturbance[1] += entry_h->indisturb[1];
										  }
								}
					 }	
		  }

		  return disturbance;
}

/* set indirect disturbance created by a memory block */
void set_ind_disturbance(mucb_p entry, mucb_p ecb_entry, int bbi_id)
{
		  int i;
		  
		  /* first cache level never create an indirect disturbance */
		  entry->indisturb[0] = 0;
		  entry->icrt = 0;

		  /* CASE I */
		  if (top_entry(entry->age))
		  {
					 entry->indisturb[1] = 1;
					 entry->icrt = 0;
					 return;
		  }
		  
		  /* CASE II */
		  if (bottom_entry(entry->age))
		  {
					 entry->indisturb[0] = entry->indisturb[1] = 0;
					 entry->icrt = 0;
					 return;
		  }

		  /* CASE III */
		  {
					 char* disturbance = find_ind_disturbance(entry, bbi_id);	
					 char* arg1 = boxplus(entry->age, ecb_entry->age);
					 int dist_d = dist(arg1);
					 int dist_safe = dist(entry->age);
					 char* arg2 = boxplus(arg1,  disturbance);
					 int dist_i =  dist(arg2);

					 for (i = 1; i < MAX_CACHE_LEVELS; i++)
					 {
								if (i <= dist_i && i > dist_safe)
										  entry->indisturb[i] = 1;
								else
										  entry->indisturb[i] = 0;			
					 }
					 for (i = dist_d; i <= dist_i - 1; i++)
								entry->icrt += miss_lat[i];
					  
					 free(arg1);
					 free(arg2);
					 free(disturbance);
		  }	

		  return;
}


/* compute the effect of indirect preemption for a particular point */
int compute_indirect_preemption_at_point(int bbi_id)
{
		  int i, j;
		  hashtab_p head, ecb_head;
		  mucb_p entry, ecb_entry;
		  tcfg_node_t* bbi = tcfg[bbi_id];
		  hashtab_p* ecb_hashtab;
		  int total_icrt = 0;
		  /* temporary memory block store */
		  mem_blk_holder* hold = NULL;
		  mem_blk_holder* iter;

		  /* get the evicted cache blocks from the preempting task */
		  ecb_hashtab = get_ecb_hashtab();

		  for (i = 0; i < HASH_TAB_SIZE; i++)
		  {
					 for (head = bbi->mem_blk_hashtab_in[i]; head; head = head->next)
					 {
								entry = (mucb_p) head->entry;
								ecb_head = found_in_hashtab(ecb_hashtab, entry->mem_blk_id, -1);			
								ecb_entry = (mucb_p) ecb_head->entry;
								/* set indirect disturbance and latency */
								set_ind_disturbance(entry, ecb_entry, bbi_id);
								total_icrt += entry->icrt;

								#ifdef _NDEBUG_CRPD
										  for (iter = hold; iter; iter = iter->next) {
													 if (iter->mem_blk_id == entry->mem_blk_id)
																break;
										  }
										  if (!iter) {
													 total_icrt += entry->icrt;
													 mem_blk_holder* node = (mem_blk_holder *) calloc (1, sizeof(mem_blk_holder));
													 CHECK_MEM(node);
													 node->dcrt = entry->icrt;
													 node->mem_blk_id = entry->mem_blk_id;
													 node->next = hold;
										  }
								#endif
					 }
		  }

		  if(hold)
					 free(hold);

		  return total_icrt;
}

/* compute indirect effect of preemption */
int compute_indirect_preemption_delay()
{
		  int i;
		  int max_icrt = 0;
		  int total_icrt;

		  for (i = 0; i < num_tcfg_nodes; i++)
		  {
					 total_icrt = compute_indirect_preemption_at_point(i);
					 
					 if (max_icrt < total_icrt)
								max_icrt = total_icrt;
		  }

		  return max_icrt;
		  
}

/* set additional bus delay according to the old and new offset; old and new 
 * fetch latency */
void set_bdelay(mucb_p entry, int bbi_id)
{
		  int i;
		  hashtab_p head;
		  mucb_p entry_c;
		  int new_offset = 0;
		  int oldLAT, newLAT;
		  int delay = 0;

		  for (i = 0; i < HASH_TAB_SIZE; i++)
		  {
					 for (head = tcfg[bbi_id]->mem_blk_hashtab_in[i]; head; head = head->next) 
					 {
								entry_c = (mucb_p) head->entry;
								/* different loop memory blocks do not affect the bus delay, 
								 * therefore skip */
								if(loop_map[entry_c->bbi_id] != loop_map[entry->bbi_id])
										  continue;
								/* ignore, if not changed due to cache effect */		  
								if(entry == entry_c || entry_c->dcrt + entry_c->icrt == 0)
										  continue;
								if (!affectors[entry->bbi_id][entry_c->bbi_id])
										  continue;
								delay += entry_c->dcrt + entry_c->icrt + entry_c->bdelay;
					 }
		  }

		  /* if no preemption related delay has been generated prior to this access, 
			* ignore the following computation....no additional bus delay can be 
			* generated for this computation either */
		  if (delay == 0) {
					 entry->bdelay = 0;
					 entry->max_bdelay = 0;
					 return;
		  }
		  
		  /* get changed offset */
		  new_offset = (entry->offset + delay) % (fx_slot_length * max_cores);

		  /* set delay according to the creation of new offset */
		  if (entry->age[0] != -1 && entry->age[0] != CINFTY)
					 oldLAT = 0;
		  else if (entry->age[1] != -1 && entry->age[1] != CINFTY)
					 oldLAT = miss_lat[0];
		  else
					 oldLAT = miss_lat[0] + miss_lat[1];
		  newLAT = oldLAT + entry->dcrt + entry->icrt;

		  if (newLAT < miss_lat[0]) {
					 entry->bdelay = 0;
					 entry->max_bdelay = 0;
					 return;
		  }
		  /* just for comparing our model */ 
		  /* baseline, taking maximum bus delay */
		  if (newLAT < miss_lat[1])
					 entry->max_bdelay = miss_lat[0] - 1 + fx_slot_length * (max_cores - 1);
		  else if (newLAT >= miss_lat[1])
					 entry->max_bdelay = miss_lat[0] + miss_lat[1] -1 + fx_slot_length * (max_cores - 1);
		  
		  /* computation of bus delay through our modeling */
		  if (new_offset + newLAT <= fx_slot_length)
					 entry->bdelay = 0;
		  else if (entry->offset + oldLAT <= fx_slot_length || oldLAT == 0)
					 entry->bdelay = fx_slot_length * max_cores - new_offset;
		  else {
					 if (entry->offset - new_offset <= 0)
								entry->bdelay = 0;
					 else
								entry->bdelay = entry->offset - new_offset;
		  }
}

/* compute additional bus delay for a preemption at a particular point */
int compute_bdelay_for_preemption_at_point(int bbi_id)
{
		  int i;
		  hashtab_p head;
		  mucb_p entry;
		  tcfg_node_t* bbi = tcfg[bbi_id];
		  int total_bdelay = 0;
		  
		  total_max_bdelay = 0;
		  
		  for (i = 0; i < HASH_TAB_SIZE; i++)
		  {
					 for (head = bbi->mem_blk_hashtab_in[i]; head; head = head->next) 
					 {
								entry = (mucb_p) head->entry;
								set_bdelay(entry, bbi_id);
								total_bdelay += entry->bdelay;
								total_max_bdelay += entry->max_bdelay;
					 }
		  }

		  return total_bdelay;
}

/* compute additional bus delay created due to preemption */
int compute_bdelay_for_preemption()
{
		  int i;
		  int max_bdelay = 0;
		  int total_bdelay = 0;

		  /* shared bus not enabled */
		  if (g_shared_bus_type == NOBUS)
					 return 0;

		  for (i = 0; i < num_tcfg_nodes; i++)
		  {
					 total_bdelay = compute_bdelay_for_preemption_at_point(i);

					 if (max_bdelay < total_bdelay)
								max_bdelay = total_bdelay;
					 if (max_max_bdelay < total_max_bdelay)
								max_max_bdelay = total_max_bdelay;
		  }

		  return max_bdelay;
}

int get_ucb_id(char* age)
{
		  assert(age);

		  return all_ucbs[age[0]][age[1]];
}

/* check if "up_age" succeeds "age" in the multi-dimensional 
 * UCB partial order */

int is_up_in_poset(char* up_age, char* age)
{
		  int i, j, k;
		  char newage[2];
		  int dist_up, dist_do, miss, miss_up;

		  if (bottom_entry(age))
					 return 1;

		  if (top_entry(up_age))
					 return 1;

		  for (i = 0; i < MAX_CACHE_LEVELS; i++)
		  {
					 if (up_age[i] < age[i])
								return 0;
		  }

		  for (i = 0; i < assoc + 1; i++)
		  {
					 for (j = 0; j < assoc_l2 + 1; j++)
					 {
								if(i+j == 0) continue;
								
								newage[0] = i; newage[1] = j;
								miss_up = 0; miss = 0;
								
								dist_up = dist(boxplus(up_age, newage));
								dist_do = dist(boxplus(age, newage));

								for (k = dist(age); k <= dist_do - 1; k++) 
										  miss += miss_lat[k];
								for (k = dist(up_age); k <= dist_up - 1; k++) 
										  miss_up += miss_lat[k];

								if (miss_up < miss)
										  return 0;
					 }
		  }

		  return 1;
}
								
/* compute and store the least upper bound for further use */
char* get_lub(int ucb1, int ucb2)
{
		  int i, j;
		  char age1[2], age2[2], newage[2];
		  char* lubage;
		  int found = 0;

		  lubage = calloc(2, sizeof(char));
		  CHECK_MEM(lubage);

		  age1[0] = ucb1 / (assoc_l2 + 1);
		  age1[1] = ucb1 % (assoc_l2 + 1);
		  age2[0] = ucb2 / (assoc_l2 + 1);
		  age2[1] = ucb2 % (assoc_l2 + 1);

		  lubage[0] = -1;
		  lubage[1] = -1;

		  for (i = 0; i < assoc + 1; i++)
		  {
					 for(j = 0; j < assoc_l2 + 1; j++)
					 {
							newage[0] = i; newage[1] = j;

							if (is_up_in_poset(newage, age1) && is_up_in_poset(newage, age2) 
								&& is_up_in_poset(lubage, newage)) {

										  lubage[0] = newage[0]; lubage[1] = newage[1];
										  found = 1;
								}
					 }
		  }
		  
		  /* top entry is the LUB */
		  if (!found) {
					 lubage[0] = lubage[1] = -1;
					 lub[ucb1][ucb2] = TOPUCB;
		  }
		  else
					 lub[ucb1][ucb2] = get_ucb_id(lubage);

		  return lubage;
}

/* find the least uppper bound of multi UCB domain */
char* findLUB(char* age1, char* age2)
{
		  int id1, id2;
		  char* newage;

		  newage = calloc(2, sizeof(char));
		  CHECK_MEM(newage);

		  if (top_entry(age1) || bottom_entry(age2)) {
					 newage[0] = age1[0]; newage[1] = age1[1];
					 return newage;
		  }
		  
		  if (top_entry(age2) || bottom_entry(age1)) {
					 newage[0] = age2[0]; newage[1] = age2[1];
					 return newage;
		  }

		  if(age1[0] == age2[0] && age1[1] == age2[1]) {
					 newage[0] = age2[0]; newage[1] = age2[1];
					 return newage;
		  }
		  
		  id1 = get_ucb_id(age1);
		  id2 = get_ucb_id(age2);

		  if (lub[id1][id2] != -1) {
					 if (lub[id1][id2] != TOPUCB) {
								newage[0] = lub[id1][id2] / (assoc_l2 + 1);
								newage[1] = lub[id1][id2] % (assoc_l2 + 1);
					 }
					 else {
								newage[0] = -1;
								newage[1] = -1;
#ifdef _DEBUG_CRPD
								fprintf(stdout, "Top made from (%d,%d) and (%d,%d)\n", age1[0], age1[1], age2[0], age2[1]);
#endif
					 }

					 return newage;
		  }

		  return newage;
}

/* join function to combine multiple multi-dimensional UCBs */
void join_multi_UCB(tcfg_node_t* succ, tcfg_node_t* bbi, int first)
{
		  int i;
		  mucb_p entry, bbi_e;
		  hashtab_p head, bbi_h;
		  char* newage;

		  for (i = 0; i < HASH_TAB_SIZE; i++)
		  {		 
					 for (head = succ->mem_blk_hashtab_in[i]; head; head = head->next)
					 {
								entry = (mucb_p) head->entry;
								bbi_h = found_in_hashtab(bbi->mem_blk_hashtab_out, entry->mem_blk_id, entry->bbi_id);
								assert(bbi_h);
								bbi_e = (mucb_p)bbi_h->entry;
										  
								if (first)	{
										  if (bbi_e->age[0] != entry->age[0] || bbi_e->age[1] != entry->age[1]) {
													 bbi_e->age[0] = entry->age[0];
													 bbi_e->age[1] = entry->age[1];
										  }
								}
								else {
										  /* join two states by taking the least upper bound */
										  newage =	findLUB(bbi_e->age, entry->age);
#ifdef _NDEBUG_CRPD
										  fprintf(stdout, "Generated arg1[0] = %d, arg1[1] = %d\n", bbi_e->age[0], bbi_e->age[1]);
										  fprintf(stdout, "Generated arg2[0] = %d, arg2[1] = %d\n", entry->age[0], entry->age[1]);
										  fprintf(stdout, "Generated newage[0] = %d, newage[1] = %d\n", newage[0], newage[1]);
#endif
										  if (bbi_e->age[0] != newage[0] || bbi_e->age[1] != newage[1]) {
													 bbi_e->age[0] = newage[0];
													 bbi_e->age[1] = newage[1];
										  }

										  free(newage);
								}
								/* remove */
								if(entry->bbi_id != -1)
										  bbi_e->bbi_id = entry->bbi_id;
					 }
		  }
}

/* transfer function to update multi dimensional UCBs */
void transform_multi_UCB(tcfg_node_t* bbi, int* change_flag)
{
		  int i;
		  de_inst_t* inst = bbi->bb->code + bbi->bb->num_inst - 1; 
		  int mblk_id = 0;
		  hashtab_p data_out, data_in, iter;
		  mucb_p entry_out, entry_in;
		  mem_blk_set_t temp;

		  #if 0
		  /* update all memory blocks that are not in cache */
		  for (i = 0; i < HASH_TAB_SIZE; i++)
		  {
					 for (iter = bbi->mem_blk_hashtab_out[i]; iter; iter = iter->next)
					 {
								entry_out = (mucb_p) iter->entry;
								if(!found_in_hashtab(bbi->all_blks_L1, entry_out->mem_blk_id, -1)) {
								#ifdef _DEBUG_CRPD
										  fprintf(stdout, "Possibly conflicting L1 age of block 0x%x = %d\n", entry_out->mem_blk_id, entry_out->age[0]);
								#endif
										  entry_out->age[0] = CINFTY;
								}
								#ifdef _NDEBUG_CRPD
								else
										  fprintf(stdout, "Found in hashtab from cache analysis\n");
								#endif
								if(!found_in_hashtab(bbi->all_blks_L2, entry_out->mem_blk_id, -1)) {
								#ifdef _DEBUG_CRPD
										  fprintf(stdout, "Possibly conflicting L2 age of block 0x%x = %d\n", entry_out->mem_blk_id, entry_out->age[1]);
								#endif
										  entry_out->age[1] = CINFTY;
								}
								#ifdef _NDEBUG_CRPD
								else
										  fprintf(stdout, "Found in hashtab from cache analysis\n");
								#endif
								if (entry_out->age[0] == -1 && entry_out->age[1] != -1)
										  entry_out->age[1] = -1;
								if (entry_out->age[1] == -1 && entry_out->age[0] != -1)
										  entry_out->age[0] = -1;
					 }
		  }
		  #endif

		  for (i = bbi->bb->num_inst - 1; i >= 0; i--)
		  {
					 mblk_id = ((inst->addr) / bsize);
					 
					 data_out = found_in_hashtab(bbi->mem_blk_hashtab_out, mblk_id, bbi->id);
					 entry_out = (mucb_p)data_out->entry;

					 if (entry_out->age[0] != inst_age_l1[bbi->id][i]) {
								entry_out->age[0] = inst_age_l1[bbi->id][i];
					 }
					 if (entry_out->age[1] != inst_age_l2[bbi->id][i]) {
								entry_out->age[1] = inst_age_l2[bbi->id][i];
					 }

					 /* set the basic block id */
					 entry_out->bbi_id = bbi->id;
					 
					 inst--;
		  }

		  for (i = 0; i < HASH_TAB_SIZE; i++)
		  {
					 for (iter = bbi->mem_blk_hashtab_in[i]; iter; iter = iter->next)
					 {
								entry_in = (mucb_p) iter->entry;
								data_out = found_in_hashtab(bbi->mem_blk_hashtab_out, entry_in->mem_blk_id, entry_in->bbi_id);
								entry_out = (mucb_p) data_out->entry;

								if (entry_out->age[0] != entry_in->age[0]) {
										  entry_in->age[0] = entry_out->age[0];
										  *change_flag = 1;
								}

								if (entry_out->age[1] != entry_in->age[1]) {
										  entry_in->age[1] = entry_out->age[1];
										  *change_flag = 1;
								}
					 
								/* set the basic block id */
								entry_in->bbi_id = entry_out->bbi_id;
					 }
		  }
}

/* do a backward flow analysis to compute multi-dimensional 
 * useful cache blocks */
void do_backward_flow_analysis()
{
		  int change_flag = 1;
		  tcfg_edge_t* edge;
		  int i;

#ifdef _DEBUG_CRPD
		  __count_crpd_analysis = 0;
#endif

		  while(change_flag)
		  {
					 change_flag = 0;

					 for (i = num_tcfg_nodes - 1; i >= 0; i--)
					 {
								int topo_id = topo_tcfg[i];
								int first = 1;

								/* join UCB-S from all exit nodes, its a backward flow 
								 * operation */
								for (edge = tcfg[topo_id]->out; edge; edge = edge->next_out)
								{
										  join_multi_UCB(edge->dst, tcfg[topo_id], first);
										  first = 0;
								}
								/* transfer function */
								transform_multi_UCB(tcfg[topo_id], &change_flag);
					 }
#ifdef _DEBUG_CRPD
					 __count_crpd_analysis++;
#endif
		  }
}

/* sudiptac :::: top level call to compute CRPD. CRPD denotes the 
 * intra-core and inter-task conflicts */
void crpd_analysis()
{
		  int i = 0;
		  int j, max_dcrt, max_icrt, crpd_final, max_old_dcrt, max_old_num_ecbs, max_bdelay;
		  de_inst_t* inst;
		  int mem_blk;
		  hashtab_p* mem_blk_hashtab;

#ifdef _DEBUG_CRPD
		  fprintf(stdout, "Starting CRPD analysis..............................................\n");
#endif
		  /* sudiptac :::: always computing the preemption effect on second task */
		  set_task(tasks[1]);
			
			/* FIXME: disabling, but needed */	
		  /* init_time(); */
		  
			/* compute initial TDMA offsets. This will help determining the effect 
			* of bus on CRPD */
		  if (g_shared_bus_type != NOBUS)
					 compute_structural_TDMA_offsets();

		  /* initialize and build memory block hash table */
		  /* you should free up all the cache states before doing this */
		  for (i = 0; i < num_tcfg_nodes; i++)
		  {
					 tcfg[i]->mem_blk_hashtab_in = init_hashtab();
					 tcfg[i]->mem_blk_hashtab_out = init_hashtab();
					 build_mem_blk_hashtab(tcfg[i]->mem_blk_hashtab_in);
					 build_mem_blk_hashtab(tcfg[i]->mem_blk_hashtab_out);
#ifdef _NDEBUG_CRPD
					 dump_mem_blk_hashtab(tcfg[i]->mem_blk_hashtab_in);
#endif
		  }

		  init_all_ucbs();
		  init_crpd(0);
		  set_count_ECB(0);

#ifdef _NDEBUG_CRPD
			for (i = 0; i < nsets; i++) 
				printf("ECB_L1[%d] = %d\n", i, tasks[0]->ecb_log_l1[i]);
			for (i = 0; i < nsets_l2; i++) 
				printf("ECB_L2[%d] = %d\n", i, tasks[0]->ecb_log_l2[i]);
#endif

		  /* do the backward flow analysis to compute the multi level 
			* useful cache blocks */
		  do_backward_flow_analysis(); 

#ifdef _NDEBUG_CRPD
		  for (i = 0; i < num_tcfg_nodes; i++)
		  {
					 fprintf(stdout, "\n------------------------------------------------------------\n");
					 fprintf(stdout, "UCBs at the entry of bbi-id %d\n", tcfg[i]->id);
					 fprintf(stdout, "------------------------------------------------------------\n");
					 dump_mem_blk_hashtab(tcfg[i]->mem_blk_hashtab_in);
		  }
		  fprintf(stdout, "CRPD backward flow analysis count = %d\n", __count_crpd_analysis);

		  fprintf(stdout, "\n------------------------------------------------------------------------\n");
		  fprintf(stdout, "Printing evicted cache block hash table\n");
		  fprintf(stdout, "------------------------------------------------------------------------\n");
		  dump_mem_blk_hashtab(g_ecb_hashtab);
#endif
		  

		  
		  /************************************/
		  /* compute preemption related delay */
		  /************************************/
		  /* compute direct effect of preemption */
		  /* currently, preemption points are set at the entry of each basic block */
		  max_dcrt = compute_direct_preemption_delay();
#ifdef _DEBUG_CRPD
		  fprintf(stdout, "\n------------------------------------------------------------------------\n");
		  fprintf(stdout, "Preemption delay due to the direct effect = %d\n", max_dcrt);
		  fprintf(stdout, "------------------------------------------------------------------------\n");
#endif
		  
		  /* compute indirect effect of preemption */
		  init_queue(&queue, sizeof(tcfg_node_t *));
		  visited = (char *) calloc (num_tcfg_nodes, sizeof(char));
		  CHECK_MEM(visited);
		  
		  /* CAUTION ********* this requires memory 
			* if memory runs out, try to run this 
			* operation online. This will of course 
			* increase the computation time */
		  build_affector_matrix();
		  max_icrt = compute_indirect_preemption_delay();

#ifdef _DEBUG_CRPD
		  fprintf(stdout, "\n------------------------------------------------------------------------\n");
		  fprintf(stdout, "Preemption delay due to the indirect effect = %d\n", max_icrt);
		  fprintf(stdout, "------------------------------------------------------------------------\n");
#endif

		  /* compute the additional bus delay created due to preemption */
		  max_bdelay = compute_bdelay_for_preemption();

#ifdef _DEBUG_CRPD
		  fprintf(stdout, "\n------------------------------------------------------------------------\n");
		  fprintf(stdout, "Preemption delay due to the shared bus = %d\n", max_bdelay);
		  fprintf(stdout, "------------------------------------------------------------------------\n");
#endif
		  
		  free(visited);
		  free_queue(&queue);

		  /* compute final CRPD */
		  crpd_final = max_dcrt + max_icrt + max_bdelay;

#ifdef _DEBUG_CRPD
		  fprintf(stdout, "\n--------------------------------------------------------------------------------------------\n");
		  fprintf(stdout, "[__CRPD__]UCB in L1&L2 = %d\n", max_ucb_l1_l2);
		  fprintf(stdout, "[__CRPD__]UCB in L1&~L2 = %d\n", max_ucb_l1_not_l2);
		  fprintf(stdout, "[__CRPD__]UCB in ~L1&L2 = %d\n", max_ucb_not_l1_l2);
		  fprintf(stdout, "[__CRPD__]ECB[L1] = %d\n", number_of_ecbs_l1);
		  fprintf(stdout, "[__CRPD__]ECB[L2] = %d\n", number_of_ecbs_l2);
		  fprintf(stdout, "[__CRPD__]BDELAY = %d\n", max_bdelay);
		  fprintf(stdout, "[__CRPD__]MAX_BDELAY = %d\n", max_max_bdelay);
		  fprintf(stdout, "[__CRPD__]Final CRPD of task %s = %d\n", tasks[1]->name, crpd_final);
		  fprintf(stdout, "--------------------------------------------------------------------------------------------\n");
		  fprintf(stdout, "Finished CRPD analysis..............................................\n");
#endif

#if 0
		  
		  /* try model checking to refine the results generated for 
			* evicted cache blocks */
		  if(getenv("RUN_AI_MC_CRPD")) {
				refine_ecbs_with_MC();
				set_count_ECB(0);
				max_dcrt = compute_direct_preemption_delay();
#ifdef _DEBUG_CRPD
				fprintf(stdout, "\n------------------------------------------------------------------------\n");
				fprintf(stdout, "Cache related preemption delay with UCB and ECB only = %d\n", max_old_dcrt);
				fprintf(stdout, "------------------------------------------------------------------------\n");
				fprintf(stdout, "------------------------------------------------------------------------\n");
				fprintf(stdout, "Total number of ECBs with CRPD analysis only = %d\n", max_old_num_ecbs);
				fprintf(stdout, "Total number of ECBs after MC refinement = %d\n", number_of_ecbs);
				fprintf(stdout, "Total number of MC calls = %d\n", number_of_mc_calls);
				fprintf(stdout, "Cache related preemption delay after MC refinement = %d\n", max_dcrt);
				fprintf(stdout, "------------------------------------------------------------------------\n");
#endif
			}	
#endif
}




