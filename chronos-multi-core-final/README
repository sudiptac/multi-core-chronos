This is the README file for the multicore version of the Chronos timing analyzer.

Source code
============
Source code of the shared cache and bus is in directory "est"

To compile the code
======================
From top level directory (i.e., chronos_multi_core)

./autotools.sh (it may require some new software installation)
./configure --with-lp-solve=<path to LP solve directory>
make clean
make

It should produce a binary file named "est/est"

Processor configuration
=========================
Various processor configurations are listed in "processor_config" directory. Please run "simplesim-3.0/sim-outorder" 
without any argument to know the interpretation of each parameter.

Task configuration
=========================
Sample task configurations are listed in "task_config" directory. It has the following format (subject to change):

<Total number of tasks>
<Path to task 1>
<priority of task 1> <assigned core to task 1> <MSC id to task 1> <list of successor task ids of task 1 according to the partial order>
<Path to task 2>
<priority of task 2> <assigned core to task 2> <MSC id to task 2> <list of successor task ids of task 2 according to the partial order>
.....

Note: MSC id is the message sequence chart id. It is mainly retained for some compatibility reason and can be removed later. 
MSC id can be put same for each task (say 0). List of successor task ids are the list of integers representing different 
task identities.

Setting up the environment (temporary)
=======================================
Currently, this version of Chronos has a few parallel project running. Therefore, before running the multi-core tool only 
environment set up is required. 

Run the script using "source scripts/env_multi_core_wcet" from the top level directory before running any program. This 
sets the shell environment. Therefore, must be set for each new shell opened. 


To run the code
======================================================================

./est/est -config <processor_config> <task_config>

processor_config is a processor configuration from the "processor_config" directory. On the other hand, "task_config" 
is a sample task configuration from "task_config" directory. 
