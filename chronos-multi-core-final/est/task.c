#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "task.h"

int successor(task_p task1, task_p task2)
{
		  int i;
		  int succ = 0;

		  for (i = 0; i < task1->nsucc; i++)
		  {
					 assert(task1->succ[i]);

					 if (task1->succ[i] == task2->tid)
								return 1;
					 else
							succ |= successor(tasks[task1->succ[i]], task2);	
		  }

		  return succ;
}

int depends(task_p task1, task_p task2)
{
		 return (successor(task1, task2) || successor(task2, task1)); 
}
