#ifndef __COMPOSE_AI_MC_H_
#define __COMPOSE_AI_MC_H_
#include "task.h"
void refine_cache_analysis_with_MC();
void refine_cache_analysis_with_MC_of_one(task_p task);
void cache_mc_result(unsigned addr, int ret, int residual_age, int isL2);
/* define a type of checker for cache analysis refinement phase */
/* currently suppported oracles: a model checker (CBMC) and a symbolic 
 * execution engine (KLEE) */
char oracle_type;
/* take the maximum number of oracle calls from user input */
int refinement_bound;
typedef enum {
	TYPE_MODEL_CHECK = 0,
	TYPE_SYMBOLIC_EXECUTION
} ORACLE_TYPE;
extern int curr_grp;
#endif
