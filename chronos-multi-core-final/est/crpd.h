#ifndef __CRPD_H_
#define __CRPD_H_
#include "cache.h"
#define HASH_TAB_SIZE 131

#define GET_MEM_BLK(x) ((x) / bsize)
#define GET_MEM_BLK_L2(x) ((x) / bsize_l2)
#define GET_SET_L1(x) ((((x) / bsize)) % nsets)
#define GET_SET_L2(x) ((((x) / bsize_l2)) % nsets_l2)
#define CINFTY assoc_l2
#define TOPUCB INFTY

/* print hit-miss classification of all instructions */
void print_classification();

struct mucb {
		  int mem_blk_id;	/* memory block id */
		  int bbi_id;		/* basic block id */
		  int dcrt;			/* direct preemption delay */
		  int icrt;			/* indirect preemption delay */
		  int indisturb[MAX_CACHE_LEVELS]; /* indirect disturbance */
		  char age[MAX_CACHE_LEVELS];
		  int offset;
		  int bdelay;
		  int max_bdelay;	/* baseline, for comapring model */
};

typedef struct mucb mucb_s;
typedef struct mucb* mucb_p;

#ifdef _DEBUG_CRPD
int __count_crpd_analysis;
#endif

hashtab_p* init_hashtab();
void add_in_hashtab(hashtab_p* mem_blk_hashtab, int bbi_id, int mem_blk_id, int offset, int age_l1, int age_l2);


#endif
