#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "sharedcache.h"
#define MAX_CONFLICT_SIZE 32

static int add_unique_entry(addr_t* addr_link, addr_t addr, int nconflicts)
{
		  int i;
		  int mem_blk_id;

		  mem_blk_id = GET_MEM_BLK_L2(addr);
		  
		  for (i = 0; i < nconflicts; i++)
		  {
					 assert(addr_link[i]);
					 
					 if (mem_blk_id == GET_MEM_BLK_L2(addr_link[i]))
								return 0;
		  }
		  addr_link[nconflicts] = addr;

		  return 1;

}

static int get_conflicts_from_other_core(task_p this_task, de_inst_t* this_inst)
{
		  int i, j, k;
		  tcfg_node_t* bbi;
		  de_inst_t* inst;
		  addr_t* addr_link;
		  int nconflicts = 0;
		  int* ncset = NULL;

		  addr_link = calloc(MAX_CONFLICT_SIZE, sizeof(addr_t));
		  CHECK_MEM(addr_link);
		  ncset = calloc(ntasks, sizeof(int));
		  CHECK_MEM(ncset);

		  for (i = 0; i < ntasks; i++)
		  {
					 if (tasks[i]->ncore == this_task->ncore)
								continue;
					 /* add partitioned multi-core features (e.g. 2 cores sharing an 
					  * L2 each in a 4 core system */
					 if ((tasks[i]->ncore)/max_cores_sharing_il2 != (this_task->ncore)/max_cores_sharing_il2)
								continue;
					 if (depends(tasks[i], this_task))
								continue;
					 memset(addr_link, 0, MAX_CONFLICT_SIZE * sizeof(addr_t));			
					 for (j = 0; j < tasks[i]->num_tcfg_nodes; j++)
					 {
								bbi = tasks[i]->tcfg[j];
								inst = bbi->bb->code;

								for (k = 0; k < bbi->bb->num_inst; k++)
								{
										  /* if L2 cache is never accessed by this instruction 
											* of the conflicting task then we skip */
										  if (tasks[i]->inst_chmc_l2[j][k] == ALL_X) {
													 /* sudiptac: have to increment "inst" here as well */
													 inst++;	
													 continue;
											}

										  if (GET_SET_L2(inst->addr) == GET_SET_L2(this_inst->addr))
										  {
													 if (add_unique_entry(addr_link, inst->addr, ncset[i]))
																ncset[i]++;
													 if (ncset[i] >= MAX_CONFLICT_SIZE)
																break;
										  }
										  
										  inst++;
								}
								if (ncset[i] >= MAX_CONFLICT_SIZE)
										  break;
					 }
					 if (ncset[i] >= MAX_CONFLICT_SIZE)
								break;
		  }


		  for(i = 0; i < ntasks; i++)
					 nconflicts += ncset[i];
		  
		  free(addr_link);
		  free(ncset);

		  return nconflicts;
}

static void update_chmc(task_p task, de_inst_t* inst, char* chmc_l1, char* chmc_l2, char* age_l1, char* age_l2)
{
		  int nconflicts;

		  /* all hit in L1 instructions are ignored */
			/* well, not exactly....because chmc_l1 can be changed (assume multi-tasking) */
		  if (*chmc_l1 == ALL_HIT)
					 return;

		  /* following instructions may potentially access L2 cache */
		  /* but if CHMC = NC or AM, conflicts from other cores cannot 
			* degrade the classification */
		  if (*chmc_l2 == ALL_MISS || *chmc_l2 == NOT_CLASSIFIED)
					 return;

		  nconflicts = get_conflicts_from_other_core(task, inst);
#ifdef _NDEBUG_CRPD
		  fprintf(stdout, "-------------------------------------------------------------------------------\n");
#endif
		  if (*age_l2 + nconflicts >= assoc_l2)
		  {
#ifdef _NDEBUG_CRPD
					 fprintf(stdout, "Affected task = %s....\n", task->name);
					 fprintf(stdout, "Affected instruction = 0x%x....\n", inst->addr);
					 fprintf(stdout, "Current age in shared L2 cache set = %d....\n", *age_l2);
					 fprintf(stdout, "Number of conflicting blocks = %d....\n", nconflicts);
					 fprintf(stdout, "Changing instruction in L2 from AH to NC....\n");
#endif
					 *age_l2 = CINFTY;
					 *chmc_l2 = NOT_CLASSIFIED;
					 task->chmc_changed++;
		  }
		  else
		  {
#ifdef _NDEBUG_CRPD
					 fprintf(stdout, "Affected task = %s....\n", task->name);
					 fprintf(stdout, "Affected instruction = 0x%x....\n", inst->addr);
					 fprintf(stdout, "Current age in shared L2 cache set = %d....\n", *age_l2);
					 fprintf(stdout, "Number of conflicting blocks = %d....\n", nconflicts);
					 fprintf(stdout, "Still all-hit after considering inter-core conflicts\n");
#endif
					 *age_l2 = *age_l2 + nconflicts;
		  }
#ifdef _NDEBUG_CRPD
		  fprintf(stdout, "-------------------------------------------------------------------------------\n");
#endif
}

/* compute inter-core conflicts and update CHMC */
static void compute_inter_core_conflicts_one(task_p task)
{
		  tcfg_node_t* bbi;
		  int i, j;
		  de_inst_t* inst;

		  for (i = 0; i < task->num_tcfg_nodes; i++)
		  {
					 bbi = task->tcfg[i];
					 inst = bbi->bb->code;

					 for (j = 0; j < bbi->bb->num_inst; j++)
					 {
								/* se residual age before inter core conflict analysis */
								if(getenv("RUN_AI_MC_SHARED_CACHE"))
										  task->residual_age_l2[i][j] = CINFTY - task->inst_age_l2[i][j] - 1;

								update_chmc(task, inst, &(task->inst_chmc_l1[i][j]), &(task->inst_chmc_l2[i][j]), 
										  &(task->inst_age_l1[i][j]), &(task->inst_age_l2[i][j]));
								
								inst++;		  
					 }
		  }

}

/* save the residual age of each memory block before inter core 
 * conflicts */
static void allocate_memory_for_residual_age(task_p task)
{
		  int i;
		  tcfg_node_t* bbi;

		  task->residual_age_l2 = (char **) calloc(task->num_tcfg_nodes, sizeof(char *));
		  CHECK_MEM(task->residual_age_l2);
		  
		  for (i = 0; i < task->num_tcfg_nodes; i++)
		  {
					 bbi = task->tcfg[i];

					 task->residual_age_l2[i] = (char *) calloc(bbi->bb->num_inst, sizeof(char));
					 CHECK_MEM(task->residual_age_l2[i]);
		  }

		  return;
}

void compute_inter_core_conflicts()
{
		  int i;

#ifdef _DEBUG_CRPD
		  fprintf(stdout, "\n-------------------------------------------------------------------------------\n");
		  fprintf(stdout, "Starting shared cache conflict analysis\n");
		  fprintf(stdout, "-------------------------------------------------------------------------------\n");
#endif

		  for (i = 0; i < ntasks; i++)
		  {
					 if(getenv("RUN_AI_MC_SHARED_CACHE"))
								allocate_memory_for_residual_age(tasks[i]);
					 compute_inter_core_conflicts_one(tasks[i]);
		  }
#ifdef _DEBUG_CRPD
		  fprintf(stdout, "-------------------------------------------------------------------------------\n");
		  fprintf(stdout, "Ending shared cache conflict analysis\n");
		  fprintf(stdout, "-------------------------------------------------------------------------------\n");
#endif
}
