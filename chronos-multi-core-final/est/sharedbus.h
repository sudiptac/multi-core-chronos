#ifndef _SHARED_BUS_H_
#define _SHARED_BUS_H_
#include "exegraph.h"
struct offset_link {
		  int off_val;
		  struct offset_link* next;
};
int max_latency(egraph_node_t* v);
int max_edge_latency(egraph_node_t* v, int edge_lat);
int min_latency(egraph_node_t* v);
int min_edge_latency(egraph_node_t* v, int edge_lat);
void update_offset(egraph_node_t* v);
offset_link_t* merge_offsets(offset_link_t* offset1, offset_link_t* offset2);
offset_link_t* propagate_offsets(egraph_node_t* v);
void link_offsets(offset_link_t** offset_link, int off_val);
int unique_offsets(offset_link_t* offset1, offset_link_t* offset2);
void freeOffsets(offset_link_t* offset_link);

#ifdef _DEBUG_CRPD
void dump_offsets(offset_link_t* off);
void dump_offsets_egraph(egraph_node_t* v);
#endif

#endif
