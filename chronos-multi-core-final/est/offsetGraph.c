// Include standard library headers
#include <assert.h>
#include <limits.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

// Include local library headers
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <debugmacros/debugmacros.h>

// Include local headers
#include "offsetGraph.h"
//#include "header.h"
//#include "handler.h"
#include "common.h"

// #########################################
// #### Definitions of static functions ####
// #########################################

/* Print the ILP variable for offset graph edge */
static inline void fprintILP_og_edge( FILE *f, 
		  const offset_graph_edge *edge, uint lp_head_id )
{
  loop_t *parent;

  if ( edge->src )
    fprintf( f, "m%u.%u_%u", loop_map[lp_head_id]->id, edge->src->id, edge->dst->id );
  else
    fprintf( f, "m%u.Sta_%u", loop_map[lp_head_id]->id, edge->dst->id );
}

/* Print the ILP variable for offset graph node */
static inline void fprintILP_og_node( FILE *f, 
		  const offset_graph_node *node, uint lp_head_id )
{
  fprintf( f, "m%u.%u", loop_map[lp_head_id]->id, node->id );
}

/* Print the loopbound for the loop with tcfg edge with id 'lp_head_id' pointing to loop head  */
static inline void fprintILP_loopbound( FILE *f, uint lp_head_id )
{
  fprintf( f, "%i", loop_map[lp_head_id]->loopbound - 1);
}

/* Write constraints that enforces correct offset graph flow */
static void writeContextFlowConstraint( FILE * const f,
    const offset_graph * const og, const offset_graph_node * const node,
    const uint num_time_steps, uint lp_head_id )
{
  DSTART( "writeContextFlowConstraint" );

  uint j, k;
  offset_graph_edge *search_edge = NULL;

  // Skip empty restrictions
  if ( node->num_incoming_edges == 0 &&
       node->num_outgoing_edges == 0 ) {
    DRETURN();
  }

    _Bool firstTerm = 1;

	if ( node->in ) {
	    /* number of times dest node is entered is 1 times less than
		   number of times incoming edge is entered, or... */
		fprintILP_og_edge( f, node->in, lp_head_id );
        if ( node->in->next_in ) {
		    fprintf( f, " + " );
			fprintILP_og_edge( f, node->in->next_in, lp_head_id );
		}
		fprintf( f, " - " );
        fprintILP_og_node( f, node, lp_head_id );
        if ( loop_map[lp_head_id]->parent && loop_map[lp_head_id]->parent != loops[0] ) 
            fprintf( f, " - m%i.%i <= 0\n", loop_map[lp_head_id]->parent->id, loop_map[lp_head_id]->parent_prev_num_context - 1);
        else
            fprintf( f, " <= 1\n" );
		
		/* ...number of times entered is the same */
		fprintILP_og_edge( f, node->in, lp_head_id );
        if ( node->in->next_in ) {
		    fprintf( f, " + " );
			fprintILP_og_edge( f, node->in->next_in, lp_head_id );
		}
		fprintf( f, " - " );
        fprintILP_og_node( f, node, lp_head_id );
		fprintf( f, " >= 0\n" );
	}
	
	if ( node->out ) {
        fprintILP_og_node( f, node, lp_head_id );
        fprintf( f, " - " );
		fprintILP_og_edge( f, node->out, lp_head_id );
		fprintf( f, " = 0\n" );    
	}

  DEND();
}

/* Writes the ILP constraints. */
static void writeCPLEXILP( FILE *f_cons, FILE *f_gen, const offset_graph *og, uint loopbound, uint lp_head_id )
{
  DSTART( "writeCPLEXILP" );

  /* ILP hints:
   * - CPLEX demands constraints where all variables are on the left, and all
   *   constants on the right side of the constraint
   * - lp_solve treats '<' and '>' like '<=' and '>=' !!! Thus only use the
   *   latter in the ILP for clarification.
   */

  /* Helper variables. */
  uint i, j, k;
  _Bool firstTerm = 1;

  offset_graph_node* search_node = NULL;
  offset_graph_edge* search_edge = NULL;

  /* Write out constraints section. */
  //fprintf( f, "\n\nSUBJECT TO\n\n\n" );
 
  /* special case, root edge is executed one time for outermost loop,
     or number of times the parent loop's context is entered with */
  if ( loop_map[lp_head_id]->parent && loop_map[lp_head_id]->parent != loops[0] ) { 
    fprintILP_og_edge( f_cons, og->root->in, lp_head_id );
	fprintf( f_cons, " - m%i.%i <= 0\n", loop_map[lp_head_id]->parent->id, loop_map[lp_head_id]->parent_prev_num_context - 1);
  } else {
    fprintILP_og_edge( f_cons, og->root->in, lp_head_id );
    fprintf( f_cons, " <= 1\n" );
  }
  
  search_node = og->root;
  ITERATE_OG_NODE(og, search_node,
    writeContextFlowConstraint( f_cons, og, search_node, 0, lp_head_id ); 
  );

  /* loopbound */
  search_node = og->root;
  ITERATE_OG_NODE(og, search_node,
      if ( search_node != og->root )
		  fprintf( f_cons, " + " );
	  fprintILP_og_node( f_cons, search_node, lp_head_id );
  );
  if ( loop_map[lp_head_id]->parent && loop_map[lp_head_id]->parent != loops[0] ) { 
    fprintf( f_cons, " - ");
    fprintILP_loopbound( f_cons, lp_head_id );
	fprintf( f_cons, " m%i.%i <= 0\n", loop_map[lp_head_id]->parent->id, loop_map[lp_head_id]->parent_prev_num_context - 1);
  } else {
    fprintf( f_cons, " <= " );
    fprintILP_loopbound( f_cons, lp_head_id );
    fprintf( f_cons, "\n" );
  }

  /* og edges must be all zero or all non-zero */
  search_edge = og->root->out;
  ITERATE_OG_EDGE(og, search_edge,
    fprintILP_og_edge( f_cons, search_edge, lp_head_id );
	fprintf( f_cons, " - ");
	fprintILP_og_edge( f_cons, og->root->in, lp_head_id );
	fprintf( f_cons, " >= 0\n" );
  );

  /* ILP variable declarations */
  search_node = og->root;
  ITERATE_OG_NODE(og, search_node,
      fprintILP_og_node( f_gen, search_node, lp_head_id );
      fprintf( f_gen, "\n");
  );
  
  search_edge = og->root->in;
  ITERATE_OG_EDGE(og, search_edge,
      fprintILP_og_edge( f_gen, search_edge, lp_head_id );
      fprintf( f_gen, "\n");
  );

  DEND();
}


/* Generate ILP constraints for the offset graph, and append it to temporary files.
 * "ilp_og.cons" contains the constraints, "ilp_og.gen" contains the variable declaraions. 
 * Both files will be appended to the main ILP file later, and removed.
 * */
void generateOffsetGraphILPCons( const offset_graph *og, uint loopbound, uint lp_head_id )
{
  DSTART( "generateOffsetGraphILPCons" );
  
  FILE *f_cons = NULL;
  FILE *f_gen = NULL;
   
  f_cons = fopen( "ilp_og.cons", "a" );
  f_gen = fopen( "ilp_og.gen", "a" );

  writeCPLEXILP( f_cons, f_gen, og, loopbound, lp_head_id );

  fclose( f_cons );
  fclose( f_gen );
}

// #########################################
// #### Definitions of public functions ####
// #########################################


/* Creates a new offset graph with one node(root) */
offset_graph *createOffsetGraph( uint number_of_nodes )
{
  offset_graph *result;
  CALLOC( result, offset_graph*, 1, sizeof( offset_graph ), "result" );

  result->root = result->last = NULL;
  result->offsets_ins = NULL;
  result->offsets_stage = NULL;
  
  /* sudiptac */		  
  result->allIterationsAreEqual = TRUE;

  return result;
}

/* Returns the node that was added as last node*/
offset_graph_node *addOffsetGraphNode(offset_graph *og, offset_data* offsets, 
							          ull bcet, ull wcet)
{
		  int i;
		  offset_graph_node *new_node;
		  offset_graph_edge *new_edge;

		  CALLOC( new_node, offset_graph_node*, 1, 
					sizeof( offset_graph_node ), "new_node" );
		  
		  new_node->id = og->loop->total_num_context;
		  new_node->offsets = offsets; 
		  new_node->in = new_node->out = NULL;

		  /* attach node to graph */
		  if ( og->root == NULL ) {
					new_edge = addOffsetGraphEdge( og, NULL, new_node, bcet, wcet );
					og->root = og->last = new_node;
		  } else {
					new_edge = addOffsetGraphEdge( og, og->last, new_node, bcet, wcet );
					og->last = new_node;
		  } 

		  og->num_nodes++;

		  return new_node;
}

/* Returns the edge that was added or NULL if nothing was added. */
offset_graph_edge *addOffsetGraphEdge(
    offset_graph *og, offset_graph_node *src,
    offset_graph_node *dst, ull bcet, ull wcet )
{
    offset_graph_edge *new_edge;
    
	DSTART( "addOffsetGraphEdge" );
    assert( og && dst && "Invalid arguments!" );

    if ( getOffsetGraphEdge( og, src, dst ) != NULL ) {
    	DOUT( "Edge existed, returning NULL!\n" );
    	DRETURN( NULL );
    } else {
		CALLOC( new_edge, offset_graph_edge*, 1,
			sizeof( offset_graph_edge ), "new_edge" );
		  
        new_edge->edge_id  = og->num_edges;
		new_edge->src      = src;
		new_edge->dst      = dst;
		new_edge->next_in  = NULL;
		new_edge->next_out = NULL;
       
		// Register with the nodes
		if (src != NULL) {
		    src->num_outgoing_edges++;
		    if (src->out == NULL)
		  	    src->out = new_edge;
		    else
			    src->out->next_out = new_edge;
        }

        // Note: next_in must store the back edge, else freeOffsetGraph() may fail
		dst->num_incoming_edges++;
		if (dst->in == NULL)
			dst->in = new_edge;
		else
			dst->in->next_in = new_edge;

		og->num_edges++;
		
		// Return the new edge
		DRETURN( new_edge );
    }
}


/* Returns the edge with the given src and dst nodes, or NULL if it doesn't exist. */
offset_graph_edge *getOffsetGraphEdge( const offset_graph *og,
    const offset_graph_node *src, const offset_graph_node *dst )
{
  assert( og && dst && "Invalid arguments!" );

  if (src != NULL && src->out != NULL && isOffsetSetEqual(&src->out->dst->offsets, &dst->offsets))
    return src->out;
  else
    return NULL;
}

/* Gets the node which represents offset 'offset'. */
offset_graph_node *getOffsetGraphNode( offset_graph *og, offset_data *offsets )
{
  int i, is_equal;

  assert( og && "Invalid arguments!" );

  if (og->root == NULL)
    return NULL;

  offset_graph_node* search_node = og->root;
  ITERATE_OG_NODE(og, search_node,
    is_equal = 1;
    
	for(i = 0; i < og->num_offsets_set; i++)
      if (!isOffsetSetEqual(&offsets[i], &search_node->offsets[i]))
        is_equal = 0;
    
	if (is_equal)
	  return search_node;
  );
  
  return NULL;
}



/* Prints the offset graph to th given file descriptor. */
void dumpOffsetGraph( const offset_graph *og, FILE *out )
{
  int i, j;
  offset_graph_node *search_node = og->root;
  
  fprintf(out, "\nOffset graph with %u nodes and %u edges:\n",
      og->num_nodes, og->num_edges);
  
  if (search_node == NULL) {
    fprintf(out, "No offset graph created!\n");
    return;
  }
   
    ITERATE_OG_NODE(og, search_node,  
      fprintf(out, " ->\n");
	  for (j=0; j<og->num_offsets_set; j++) {
	    fprintf(out, "egraph[%i][%i]: ", og->offsets_ins[j], og->offsets_stage[j]);
		
		for(i=0; i < TECHNICAL_OFFSET_MAXIMUM; i++) {
          if(search_node->offsets[j].content.offset_set.offsets[i]!=0)
		    fprintf(out, "%-2i ", i);
        }
		fprintf(out, "\n");
      }
    );
  
    if(search_node->out) {
      fprintf(out, " -> (back edge)\n");
      
	  for (j=0; j<og->num_offsets_set; j++) {
        fprintf(out, "egraph[%i][%i]: ", og->offsets_ins[j], og->offsets_stage[j]);
        
		for(i=0; i < TECHNICAL_OFFSET_MAXIMUM; i++) 
	      if(search_node->out->dst->offsets[0].content.offset_set.offsets[i]!=0)
            fprintf(out, "%-2i ", i);
        fprintf(out, "\n");
      }
    }

  fprintf(out, "\n");
}

/* Deallocates an offset graph. */
void freeOffsetGraph( offset_graph *og )
{
  assert( og && "Invalid arguments!" );
  
  int i;
  offset_graph_node *search_node = og->root;

  ITERATE_OG_NODE(og, search_node,
	free( search_node->offsets );

    // Note: search_node->in must not be back edge
	if( search_node->in ) {
      if( search_node->in->src) {
	    free( search_node->in->src );
		search_node->in->src = NULL;
	  }
	  free( search_node->in );
	  search_node->in = NULL;
	}
  );
  if( search_node->out ) {
    free( search_node->out );
	search_node->out = NULL;
  }
  free( search_node );
  search_node = NULL;

  free ( og->offsets_ins );
  free ( og->offsets_stage );

  // Free the graph itself
  free( og );
  og = NULL;
}
