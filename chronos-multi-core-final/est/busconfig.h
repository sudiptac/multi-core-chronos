#ifndef __BUS_CONFIG_H_
#define __BUS_CONFIG_H_

/* Types of TDMA bus schedule */
#define PARTITION_CORE -1
typedef enum {
	SCHED_TYPE_1 = 0,
	SCHED_TYPE_2,
	SCHED_TYPE_3
} sched_type;

typedef enum {
		  NOBUS = -1,
		  RR_TDMA = 0,
		  FLEXRAY_STATIC = 1
} bus_type;

struct core_sched {
	int start_time; /* starting time of the first slot for the core */	
	int interval;	 /* time interval between two consecutive slots of the same core */ 
	int slot_len;	 /* slot length of the core in this segment */	
};
typedef struct core_sched core_sched_s;
typedef struct core_sched* core_sched_p;

struct segment {
	unsigned long long seg_start;	 /* starting time of the segment */	
	unsigned long long seg_end;	 /* ending time of the segment */	
	core_sched_p* per_core_sched;	/* Core wise schedule information */
};
typedef struct segment segment_s;
typedef struct segment* segment_p;

struct schedule {
	sched_type type;	   /* type of the TDMA bus schedule */
	segment_p* seg_list; /* list of segments in the whole schedule length */	
	int n_segments;	   /* number of segments in the full schedule */ 	
	int n_cores;	      /* number of cores active */ 	
};

/* Defined type for global TDMA bus schedule */
typedef struct schedule sched_s;
typedef struct schedule* sched_p;

void print_TDMA_sched();
segment_p find_segment(segment_p* head_seg, int nsegs, unsigned long long start_time);
sched_p getSchedule();
void setSchedule(char* sched_file);
void read_shared_bus_opt(int argc, char** argv);
int g_shared_bus_type;			/* shared bus type : RR or Flexray */
int max_cores_sharing_il2;		/* maximum number of cores sharing the l2 instruction cache */
sched_p global_sched_data;		/* global bus schedule */
char* sched_filename;			/* configuration file for bus schedule */
int max_cores;						/* total cores sharing the bus */
int fx_slot_length;				/* for schedules which has fixed schedules */
#endif
