#ifndef SIM_CMP_H 
#define SIM_CMP_H 


/*
 * sim_cmp.h: Contains CMP definitions
 *
 * Author: Rama Sangireddy & Sandeep Baldawa<sob051000@utdallas.edu>
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <signal.h>
#include "host.h"
#include "misc.h"
#include "machine.h"
#include "regs.h"
#include "memory.h"
#include "cache.h"
#include "loader.h"
#include "syscall.h"
#include "bpred.h"
#include "resource.h"
#include "bitmap.h"
#include "options.h"
#include "eval.h"
#include "stats.h"
#include "ptrace.h"
#include "dlite.h"
#include <time.h>
#include "sim.h"
#include "stats.h"

/*  Define the max no of CPU's you want in here */
#define MAX_CMP max_cores
/*   No of processors sharing a L2 */
#define NO_CORE_SHARE_L2 max_cores_sharing_il2

/* sudiptac: added to handle single preemption */
#define PREEMPTION_POINT_INTERVAL 1000
//#define IS_PREEMPTION_POINT(x) (((x)%PREEMPTION_POINT_INTERVAL == 0) ? 1 : 0)
#define IS_PREEMPTION_POINT(x) ((x) == PREEMPTION_POINT_INTERVAL)

/* resource pool indices, NOTE: update these if you change FU_CONFIG */
#define FU_IALU_INDEX			0
#define FU_IMULT_INDEX			1
#define FU_MEMPORT_INDEX		2
#define FU_FPALU_INDEX			3
#define FU_FPMULT_INDEX			4

/* text-based stat profiles */
#define MAX_PCSTAT_VARS 8

#define MAX_STD_UNKNOWNS		64

/* integer register file */
#define R_BMAP_SZ       (BITMAP_SIZE(MD_NUM_IREGS))

/* floating point register file */
#define F_BMAP_SZ       (BITMAP_SIZE(MD_NUM_FREGS))

/* miscellaneous registers */
#define C_BMAP_SZ       (BITMAP_SIZE(MD_NUM_CREGS))

#define RSLINK_NULL_DATA		{ NULL, NULL, 0 }

/* speculative memory hash table size, NOTE: this must be a power-of-two */
#define STORE_HASH_SIZE		32

/*
 * processor core definitions and declarations
 */

/* inst tag type, used to tag an operation instance in the RUU */
typedef unsigned int INST_TAG_TYPE;

/* inst sequence type, used to order instructions in the ready list, if
   this rolls over the ready list order temporarily will get messed up,
   but execution will continue and complete correctly */
typedef unsigned int INST_SEQ_TYPE;

/* define shared bus type */
typedef enum {
		  NOBUS = -1,
		  RR_TDMA = 0,
		  FLEXRAY_STATIC = 1
} SHARED_BUS_TYPE;


/* total input dependencies possible */
#define MAX_IDEPS               3

/* total output dependencies possible */
#define MAX_ODEPS               2

/* a register update unit (RUU) station, this record is contained in the
 processors RUU, which serves as a collection of ordered reservations
stations.  The reservation stations capture register results and await
the time when all operands are ready, at which time the instruction is
issued to the functional units; the RUU is an order circular queue, in which
instructions are inserted in fetch (program) order, results are stored in
the RUU buffers, and later when an RUU entry is the oldest entry in the
machines, it and its instruction's value is retired to the architectural
register file in program order, NOTE: the RUU and LSQ share the same
structure, this is useful because loads and stores are split into two
operations: an effective address add and a load/store, the add is inserted
into the RUU and the load/store inserted into the LSQ, allowing the add
to wake up the load/store when effective address computation has finished */

struct RUU_station {
  	/* inst info */
  	md_inst_t IR;			/* instruction bits */
  	enum md_opcode op;			/* decoded instruction opcode */
  	md_addr_t PC, next_PC, pred_PC;	/* inst PC, next PC, predicted PC */
 	 int in_LSQ;				/* non-zero if op is in LSQ */
  	int ea_comp;				/* non-zero if op is an addr comp */
  	int recover_inst;			/* start of mis-speculation? */
  	int stack_recover_idx;		/* non-speculative TOS for RSB pred */
  	struct bpred_update_t dir_update;	/* bpred direction update info */
  	int spec_mode;			/* non-zero if issued in spec_mode */
  	md_addr_t addr;			/* effective address for ld/st's */
  	INST_TAG_TYPE tag;			/* RUU slot tag, increment to
					   squash operation */
  	int context_id;                       /* the id of the context this entry belongs to */
  	INST_SEQ_TYPE seq;			/* instruction sequence, used to
					   sort the ready list and tag inst */
  	unsigned int ptrace_seq;		/* pipetrace sequence number */
  	int slip;
  	/* instruction status */
  	int queued;				/* operands ready and queued */
  	int issued;				/* operation is/was executing */
  	int completed;			/* operation has completed execution */
	int icache_miss;		/* sudiptac: added for Chronos */
	int icache_miss_l2;	/* sudiptac: added for Chronos */
  	/* output operand dependency list, these lists are used to
     	limit the number of associative searches into the RUU when
     	instructions complete and need to wake up dependent insts */
  	int onames[MAX_ODEPS];		/* output logical names (NA=unused) */
  	struct RS_link *odep_list[MAX_ODEPS];	/* chains to consuming operations */

  	/* input dependent links, the output chains rooted above use these
     	fields to mark input operands as ready, when all these fields have
     	been set non-zero, the RUU operation has all of its register
     	operands, it may commence execution as soon as all of its memory
     	operands are known to be read (see lsq_refresh() for details on
     	enforcing memory dependencies) */
  	int idep_ready[MAX_IDEPS];		/* input operand ready? */
};


/* IFETCH -> DISPATCH instruction queue definition */
struct fetch_rec {
  	md_inst_t IR;				/* inst register */
  	md_addr_t regs_PC, pred_PC;		/* current PC, predicted next PC */
  	struct bpred_update_t dir_update;	/* bpred direction update info */
  	int stack_recover_idx;		/* branch predictor RSB index */
  	unsigned int ptrace_seq;		/* print trace sequence id */
	int icache_miss;				/* sudiptac: added for Chronos */
	int icache_miss_l2;			/* sudiptac: added for Chronos */
};

/*
 * RS_LINK defs and decls
 */

/* a reservation station link: this structure links elements of a RUU
   reservation station list; used for ready instruction queue, event queue, and
   output dependency lists; each RS_LINK node contains a pointer to the RUU
   entry it references along with an instance tag, the RS_LINK is only valid if
   the instruction instance tag matches the instruction RUU entry instance tag;
   this strategy allows entries in the RUU can be squashed and reused without
   updating the lists that point to it, which significantly improves the
   performance of (all to frequent) squash events */
struct RS_link {
  struct RS_link *next;			/* next entry in list */
  struct RUU_station *rs;		/* referenced RUU resv station */
  INST_TAG_TYPE tag;			/* inst instance sequence number */
  union {
    tick_t when;			/* time stamp of entry (for eventq) */
    INST_SEQ_TYPE seq;			/* inst sequence */
    int opnum;				/* input/output operand number */
  } x;
};




/* speculative memory hash table definition, accesses go through this hash
   table when accessing memory in speculative mode, the hash table flush the
   table when recovering from mispredicted branches */
struct spec_mem_ent {
  struct spec_mem_ent *next;		/* ptr to next hash table bucket */
  md_addr_t addr;			/* virtual address of spec state */
  unsigned int data[2];			/* spec buffer, up to 8 bytes */
};

/*
 * the create vector maps a logical register to a creator in the RUU (and
 * specific output operand) or the architected register file (if RS_link
 * is NULL)
 */

/* an entry in the create vector */
struct CV_link {
  struct RUU_station *rs;               /* creator's reservation station */
  int odep_num;                         /* specific output operand */
};


typedef struct context_t{
  	int id;                     /* thread id */
  	FILE *infile, *outfile;     /* input(stdin) / output(stdout) redirects for the context */
  	FILE *argfile;              /* the arg file specifying commandline containing arguments for this thread */
  	struct stat_sdb_t *sdb;
  	/*options database */
  	struct opt_odb_t *sim_odb;

 	int context_expired;/****Flag indicating if this context has surpassed it maximum value*****/ 
	int shared_mem_latency;/****Value of the latency since the shared memory is locked*****/ 
  	struct stat_sdb_t *sim_sdb;
 	/*Register for the processor*/
  	struct regs_t regs;

  	struct regs_t spec_regs;    /* the "speculative" regsiters for the context */
 
  	/* cycles until fetch issue resumes - used for modeling minimum branch mispredition penalty */
  	unsigned fetch_issue_delay;

  	/* instruction count for the ICOUNT fetch policy */
  	int icount;


  	/* total number of instructions commited for this thread */
  	long long sim_num_insn;

 	/* simulated memory */
	struct mem_t *mem ;

	/* maximum number of inst's to execute */
	unsigned int max_insts;

	/* number of insts skipped before timing starts */
	int fastfwd_count;

	/* pipeline trace range and output filename */
	int ptrace_nelt ;
	char *ptrace_opts[2];

	/* instruction fetch queue size (in insts) */
	int ruu_ifq_size;

	/* extra branch mis-prediction latency */
	int ruu_branch_penalty;

	/* speed of front-end of machine relative to execution core */
	int fetch_speed;

	/* branch predictor type {nottaken|taken|perfect|bimod|2lev} */
	char *pred_type;

	/* bimodal predictor config (<table_size>) */
	int bimod_nelt;
	int bimod_config[1];

	/* 2-level predictor config (<l1size> <l2size> <hist_size> <xor>) */
	int twolev_nelt;
	int twolev_config[4];

	/* combining predictor config (<meta_table_size> */
	int comb_nelt;
	int comb_config[1];

	/* return address stack (RAS) size */
	int ras_size;

	/* BTB predictor config (<num_sets> <associativity>) */
	int btb_nelt;
	int btb_config[2];

	/* instruction decode B/W (insts/cycle) */
	int ruu_decode_width;

	/* instruction issue B/W (insts/cycle) */
	int ruu_issue_width;

	/* run pipeline with in-order issue */
	int ruu_inorder_issue;

	/* issue instructions down wrong execution paths */
	int ruu_include_spec;

	/* instruction commit B/W (insts/cycle) */
	int ruu_commit_width;

	/* register update unit (RUU) size */
	int RUU_size;

	/* load/store queue (LSQ) size */
	int LSQ_size;

	/* l1 data cache config, i.e., {<config>|none} */
	char *cache_dl1_opt;

	/* l1 data cache hit latency (in cycles) */
	unsigned int cache_dl1_lat;

	/* l2 data cache config, i.e., {<config>|none} */
	char *cache_dl2_opt;

	/* l2 data cache hit latency (in cycles) */
	unsigned int cache_dl2_lat;

	/* l1 instruction cache config, i.e., {<config>|dl1|dl2|none} */
	char *cache_il1_opt;

	/* l1 instruction cache hit latency (in cycles) */
	unsigned int cache_il1_lat;

	/* l2 instruction cache config, i.e., {<config>|dl1|dl2|none} */
	char *cache_il2_opt;

	/* l2 instruction cache hit latency (in cycles) */
	unsigned int cache_il2_lat;

	/* flush caches on system calls */
	int flush_on_syscalls;

	/* convert 64-bit inst addresses to 32-bit inst equivalents */
	int compress_icache_addrs;

		/* text-based stat profiles */
	int pcstat_nelt;
	char *pcstat_vars[MAX_PCSTAT_VARS];

	/* the number of cycles to fast foward this thread */
  	long long fastfwd_cnt;
  	
	/* the nubmer of instructions left to fast forward */
  	long long fastfwd_left;



	/* resource pool definition, NOTE: update FU_*_INDEX defs if you change this */
	struct res_desc fu_config[5];



	/* speculation mode, non-zero when mis-speculating, i.e., executing
	instructions down the wrong path, thus state recovery will eventually have
	to occur that resets processor register and memory state back to the last
	precise state */
	int spec_mode;

	/* cycles until fetch issue resumes */
	unsigned ruu_fetch_issue_delay;

	/* perfect prediction enabled */
	int pred_perfect;

	/* register update unit, combination of reservation stations and reorder
	buffer device, organized as a circular queue */
	struct RUU_station *RUU;		/* register update unit */
	int RUU_head, RUU_tail;			/* RUU head and tail pointers */
	int RUU_num;					/* num entries currently in RUU */

	/*
	* load/store queue (LSQ): holds loads and stores in program order, indicating
	* status of load/store access:
	*
	*   - issued: address computation complete, memory access in progress
	*   - completed: memory access has completed, stored value available
	*   - squashed: memory access was squashed, ignore this entry
	*
	* loads may execute when:
	*   1) register operands are ready, and
	*   2) memory operands are ready (no earlier unresolved store)
	*
	* loads are serviced by:
	*   1) previous store at same address in LSQ (hit latency), or
	*   2) data cache (hit latency + miss latency)
	*
	* stores may execute when:
	*   1) register operands are ready
	*
	* stores are serviced by:
	*   1) depositing store value into the load/store queue
	*   2) writing store value to the store buffer (plus tag check) at commit
	*   3) writing store buffer entry to data cache when cache is free
	 *
	* NOTE: the load/store queue can bypass a store value to a load in the same
	*   cycle the store executes (using a bypass network), thus stores complete
	*   in effective zero time after their effective address is known
	*/
	struct RUU_station *LSQ;         /* load/store queue */
	int LSQ_head, LSQ_tail;          /* LSQ head and tail pointers */
	int LSQ_num;                     /* num entries currently in LSQ */

	struct fetch_rec *fetch_data;	/* IFETCH -> DISPATCH inst queue */
	int fetch_num;					/* num entries in IF -> DIS queue */
	int fetch_tail, fetch_head;		/* head and tail pointers of queue */

	/*
	 * the execution unit event queue implementation follows, the event queue
	* indicates which instruction will complete next, the writeback handler
	* drains this queue
	*/

	/* pending event queue, sorted from soonest to latest event (in time), NOTE:
	RS_LINK nodes are used for the event queue list so that it need not be
	updated during squash events */
	struct RS_link *event_queue;

	/*
	 * the ready instruction queue implementation follows, the ready instruction
	* queue indicates which instruction have all of there *register* dependencies
	* satisfied, instruction will issue when 1) all memory dependencies for
	* the instruction have been satisfied (see lsq_refresh() for details on how
	* this is accomplished) and 2) resources are available; ready queue is fully
	* constructed each cycle before any operation is issued from it -- this
	* ensures that instruction issue priorities are properly observed; NOTE:
	* RS_LINK nodes are used for the event queue list so that it need not be
	* updated during squash events
	*/

	/* the ready instruction queue */
	struct RS_link *ready_queue;

	/* RS link free list, grab RS_LINKs from here, when needed */
	struct RS_link *rslink_free_list;

	/* the create vector, NOTE: speculative copy on write storage provided
	   for fast recovery during wrong path execute (see tracer_recover() for
	details on this process */
	BITMAP_TYPE(MD_TOTAL_REGS, use_spec_cv);
	struct CV_link create_vector[MD_TOTAL_REGS];
	struct CV_link spec_create_vector[MD_TOTAL_REGS];

	/* these arrays shadow the create vector an indicate when a register was
	last created */
	tick_t create_vector_rt[MD_TOTAL_REGS];
	tick_t spec_create_vector_rt[MD_TOTAL_REGS];

	/*
	 * routines for generating on-the-fly instruction traces with support
	 * for control and data misspeculation modeling
	*/
	
	/* integer register file */
	BITMAP_TYPE(MD_NUM_IREGS, use_spec_R);
	md_gpr_t spec_regs_R;

	/* floating point register file */
	BITMAP_TYPE(MD_NUM_FREGS, use_spec_F);
	md_fpr_t spec_regs_F;

	/* miscellaneous registers */
	BITMAP_TYPE(MD_NUM_FREGS, use_spec_C);
	md_ctrl_t spec_regs_C;

	/* speculative memory hash table */
	struct spec_mem_ent *store_htable[STORE_HASH_SIZE];

	/* speculative memory hash table bucket free list */
	struct spec_mem_ent *bucket_free_list;

	/* program counter */
	md_addr_t pred_PC;
	md_addr_t recover_PC;

	/* fetch unit next fetch address */
	md_addr_t fetch_regs_PC;
	md_addr_t fetch_pred_PC;

	/* the last operation that ruu_dispatch() attempted to dispatch, for
	implementing in-order issue */
	struct RS_link last_op;
 	int last_inst_missed;
	int last_inst_tmissed;
 	
	/* sudiptac: added for Chronos */
	int my_last_inst_missed;
	int my_last_inst_missed_in_l2;
	
	/* memory access latency (<first_chunk> <inter_chunk>) */
	int mem_nelt ;
	  

	/* memory access bus width (in bytes) */
	int mem_bus_width;

	/* instruction TLB config, i.e., {<config>|none} */
        char *itlb_opt;

	/* data TLB config, i.e., {<config>|none} */
	 char *dtlb_opt;

	/* inst/data TLB miss latency (in cycles) */
	 int tlb_miss_lat;

	/* total number of integer ALU's available */
	 int res_ialu;

	/* total number of integer multiplier/dividers available */
	 int res_imult;

	/* total number of memory system ports available (to CPU) */
	 int res_memport;

	/* total number of floating point ALU's available */
	 int res_fpalu;

	/* total number of floating point multiplier/dividers available */
	 int res_fpmult;

	/*
 	* simulator stats
 	*/
	/* SLIP variable */
 	counter_t sim_slip ;

	/* total number of instructions executed */
	 counter_t sim_total_insn ;

	/* total number of memory references committed */
	 counter_t sim_num_refs ;

	/* total number of memory references executed */
	 counter_t sim_total_refs ;

	/* total number of loads committed */
	 counter_t sim_num_loads ;

	/* total number of loads executed */
	 counter_t sim_total_loads ;

	/* total number of branches committed */
	 counter_t sim_num_branches ;

	/* total number of branches executed */
	 counter_t sim_total_branches ;

	/* occupancy counters */
	 counter_t IFQ_count;		/* cumulative IFQ occupancy */
	 counter_t IFQ_fcount;		/* cumulative IFQ full count */
	 counter_t RUU_count;		/* cumulative RUU occupancy */
	 counter_t RUU_fcount;		/* cumulative RUU full count */
	 counter_t LSQ_count;		/* cumulative LSQ occupancy */
	 counter_t LSQ_fcount;		/* cumulative LSQ full count */
	
	/* total non-speculative bogus addresses seen (debug var) */
	 counter_t sim_invalid_addrs;
	
	/*
	 * simulator state variables
 	*/
	
	/* instruction sequence counter, used to assign unique id's to insts */
 	unsigned int inst_seq ;

	/* pipetrace instruction sequence counter */
	 unsigned int ptrace_seq;


	/* speculative bpred-update enabled */
	 char *bpred_spec_opt;
	 enum { spec_ID, spec_WB, spec_CT } bpred_spec_update;

	/* level 1 instruction cache, entry level instruction cache */
	 struct cache_t *cache_il1;

	/* level 1 instruction cache */
	 struct cache_t *cache_il2;

	/* level 1 data cache, entry level data cache */
	 struct cache_t *cache_dl1;

	/* level 2 data cache */
	 struct cache_t *cache_dl2;

	/* instruction TLB */
	 struct cache_t *itlb;

	/* data TLB */
	 struct cache_t *dtlb;

	/* branch predictor */
	 struct bpred_t *pred;

	/* functional unit resource pool */
	 struct res_pool *fu_pool ;

	/* text-based stat profiles */
	 struct stat_stat_t *pcstat_stats[MAX_PCSTAT_VARS];
	 counter_t pcstat_lastvals[MAX_PCSTAT_VARS];
	 struct stat_stat_t *pcstat_sdists[MAX_PCSTAT_VARS];

	/* sudiptac :::: added total simulation cycle related to
	 * the particular context/core */	  
	 sqword_t sim_cycles;
	 
	 /* sudiptac :::: total bus delay */
	 sqword_t bdelay;

	/* sudiptac :::: Chronos related change in multi-core environment */	  
	/* By Chronos :::: For simulating only the user code */
	/* effective simulation cycles */
	 sqword_t effect_cycles;
	 
	 /* Effective L2 cache and L1 Cache misses */
	 sqword_t effect_icache_l1_miss;
	 sqword_t effect_icache_l2_miss;
	 
	 /* Effective miss predicted branches */
	 sqword_t effect_bpred_miss;

	 /* effective commit of instruction in pipeline (only in user code) */
	 sqword_t effect_commit;

	 /* sudiptac :::: effective bus delay */
	 /* Adding a shared bus in the simulator */
	 sqword_t effect_bus_delay;

	 /* sudiptac: added to handle single preemption */	  
	 char preempting_task;
	 /* preempting task (high priority) name. need to provide 
		* full path name of the preempting task binary */
	 char* preempting_task_name;

	 /* store the preempting context number */
	 char preempting_context;

	 /* define the preemption point of a low priority task */
	 /* this field is set through command line argument 
		* "-preempt:point <int>", the preemption point is 
		* defined in terms of the number of already simulated 
		* instructions in the low priority task */
	 long preemption_point;
	 
	 /* start time and finish time ... for response time 
	  * calculation */
	 sqword_t start_time;
	 sqword_t finish_time;

} context;

/* sudiptac: introducing a virtual to physical core mapping 
 * to handle multi-tasking system simulation */
char* virtual_to_physical_core_map;


#endif/* SIM_CMP_H */
