/localdisk/ee6304/team5/clean-code/simplescalar_linux/simplesim-3.0/sim-outorder \
-fastfwd 1000000 \
-max:inst 10000000 \
-fetch:ifqsize 4 \
-decode:width 4 \
-issue:width 4 \
-commit:width 4 \
-cache:il1 il1:1024:32:2:l \
-cache:il2 dl2 \
-cache:dl1 dl1:1024:16:4:l \
-cache:dl2 ul2:2048:64:4:l \
-fetch:mplat 11 \
-ruu:size 64 \
-lsq:size 32 \
-res:ialu 4 \
-res:imult 2 \
-res:fpalu 2 \
-res:fpmult 2 \
-mem:lat 50 2 \
/localdisk/ee6304/team5/clean-code/simplescalar_linux/spec2000binaries/gap00.peak.ev6 -l ./ -q -m 192M <gap.in \
/localdisk/ee6304/team5/clean-code/simplescalar_linux/Sim_results/Sim_spec2000/integer/gap00_simulate.out \
/localdisk/ee6304/team5/clean-code/simplescalar_linux/Sim_results/Sim_spec2000/integer/gap00_4issue
