/*! This is a header file of the Chronos timing analyzer. */

/*
 * A graph structure to represent an offset graph as needed in the
 * alignment-aware DAG-based WCET/BCET-analysis.
 */

#ifndef __CHRONOS_ANALYSIS_OFFSET_GRAPH_H
#define __CHRONOS_ANALYSIS_OFFSET_GRAPH_H

#include "common.h"
#include "offsetData.h"

// ######### Macros #########

#define ITERATE_OG_NODE(og, search_node, loop_body_stmts) \
{ \
		  while (search_node != og->last) { \
					loop_body_stmts \
					search_node = search_node->out->dst; \
		  } \
		  loop_body_stmts \
}

#define ITERATE_OG_EDGE(og, search_edge, loop_body_stmts) \
{ \
		  while (search_edge != og->last->out) { \
					loop_body_stmts \
					search_edge = search_edge->dst->out; \
		  } \
		  if (search_edge != NULL) { \
					loop_body_stmts \
		  } \
}

// ######### Datatype declarations  ###########


// Forward declarations
typedef struct og_edge offset_graph_edge;
typedef struct og_node offset_graph_node;
typedef struct og      offset_graph;


/* Represents an edge in the offset graph (see below) */
struct og_edge {
  uint edge_id; /* A unique identifier for the edge. */
  offset_graph_node *src;
  offset_graph_node *dst;
  offset_graph_edge *next_in; /* next incoming edge of the source node. */
  offset_graph_edge *next_out; /* next outgoing edge of the source node. */
};

/* Represents a node in the offset graph (see below) */
struct og_node {
  uint id;
  offset_data* offsets;  	 	/* The offsets that are represented by the node. */
  uint num_incoming_edges; 		/* The current size of 'num_incoming_edges'. */
  uint num_outgoing_edges; 		/* The current size of 'num_outgoing_edges'. */
  offset_graph_edge *in, *out;  /* Incoming and outgoing edges pointers */
};

/* Represents a graph whose nodes represent a loop execution at a certain
 * bus context. All edges (u,v) have cost equal
 * to the WCET that occurs during an execution of the loop which starts
 * at context(u) and ends at context(v).
 */
struct og {
  offset_graph_node *root, *last; /* The first node and last node respectively */
  uint num_nodes;
  uint num_edges;

  uint num_offsets_set;
  uint* offsets_ins;
  uint* offsets_stage;

  loop_t* loop; /* The tcfg loop associated with the offset graph */

  /* sudiptac ::: adding the status here */
  _Bool allIterationsAreEqual;
};


/* sudiptac :: pi_nodes data structure */
/* keeps track of all loop exe graph nodes which has a direct 
 * connection to outside loop nodes */
struct pi_node {
		  int edge_id;
		  int inst;
		  int stage;
		  offset_graph* graph;
		  offset_graph* wcet_graph;
		  offset_graph* bcet_graph;
		  offset_data offsets;
		  offset_data start_offsets;
};

typedef struct pi_node pi_node_t;

// ######### Function declarations  ###########


offset_graph *createOffsetGraph( uint number_of_nodes );

/* Returns the edge that was added or NULL if nothing was added. */
offset_graph_edge *addOffsetGraphEdge(
    offset_graph *og , offset_graph_node *src,
    offset_graph_node *dst, ull bcet, ull wcet );

/* Gets the node which represents offset 'offset'. */
offset_graph_node *getOffsetGraphNode( offset_graph *og, offset_data *offsets );

/* Returns the edge with the given src and dst nodes, or NULL if it doesn't exist. */
offset_graph_edge *getOffsetGraphEdge( const offset_graph *og,
    const offset_graph_node *src, const offset_graph_node *dst );

/* Prints the offset graph to th given file descriptor. */
void dumpOffsetGraph( const offset_graph *og, FILE *out );

/* Deallocates an offset graph. */
void freeOffsetGraph( offset_graph *og );


#endif
