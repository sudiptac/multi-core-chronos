#ifndef __BUS_SCHEDULE_H
#define __BUS_SCHEDULE_H

typedef unsigned long long ull;
typedef unsigned int uint;

int compute_bus_delay(int start_offset, int lat);

#endif
