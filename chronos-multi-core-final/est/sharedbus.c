#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "common.h"
#include "sharedbus.h"
#include "busconfig.h"
#include "cache.h"
extern cache_t	cache;
extern cache_t cache_l2;	
extern int first_b;

/* compute waiting time for flexray statuc segment */
static int compute_bus_delay_FLEXRAY_STATIC(int off_val, int latency)
{
		  int delay_elem;
		  int fx_interval;

		  fx_interval = max_cores * fx_slot_length;

		  /* FLEXRAY twik */
		  if(off_val == 0)	 
					 delay_elem = 0;
		  else
					 delay_elem = fx_interval - (off_val % fx_interval);

		  return (delay_elem + latency);
}

/* Compute waiting time for a memory request and a given deterministic TDMA schedule */
static int compute_bus_delay_RR_TDMA(int off_val, int latency)
{
		  int delay_elem;
		  int fx_interval;

		  fx_interval = max_cores * fx_slot_length;

		  if(off_val + latency <= fx_slot_length)	 
					 delay_elem = 0;
		  else
					 delay_elem = fx_interval - (off_val % fx_interval);

		  return (delay_elem + latency);
}

/* Top level routine which actually computes the total time for a bus access to complete */
int compute_bus_delay(int off_val, int latency)
{
		  switch(g_shared_bus_type)
		  {
					 case NOBUS:
								return 0;
					 case RR_TDMA:
								return compute_bus_delay_RR_TDMA(off_val, latency);
					 case FLEXRAY_STATIC:
								return compute_bus_delay_FLEXRAY_STATIC(off_val, latency);
					 default:
								fprintf(stdout, "Shared bus type not supported\n");
								exit(-1);
		  };

		  /* make compiler happy */
		  return -1;
}

/* compute output offset from a given offset and latency for FLEXRAY static segments */
static int compute_offset_out_FLEXRAY_STATIC(egraph_node_t* v, int off_val, int latency)
{
		  /* FIXME: */
		  return -1;
}

/* compute output offset from a given offset and latency for RR schedule */
static int compute_offset_out_RR_TDMA(egraph_node_t* v, int off_val, int latency)
{
		  int fx_interval;

		  fx_interval = fx_slot_length * max_cores;
		  
		  if (v->stage != 0)
					 return (off_val + latency) % fx_interval;
		  else
					 return (off_val + compute_bus_delay(off_val, latency)) % fx_interval;
}

/* compute outgoing offset from a given offset and given latency */
static int compute_offset_out(egraph_node_t* v, int off_val, int latency)
{
		  switch(g_shared_bus_type)
		  {
					 case NOBUS:
								return 0;
					 case RR_TDMA:
								return compute_offset_out_RR_TDMA(v, off_val, latency);
					 case FLEXRAY_STATIC:
								return compute_offset_out_FLEXRAY_STATIC(v, off_val, latency);
					 default:
								fprintf(stdout, "Shared bus type not supported\n");
								exit(-1);
		  };
		  
		  return -1;
}

static int* get_all_latencies(egraph_node_t* v, int* nl)
{
		  int* lat;
		  int j;

		  if (v->stage == 0)
		  {
					 if (v->lat.lo == 1 && v->lat.hi > cache.cmp + 1)
					 {
								*nl = 3;
								lat = (int *) calloc(*nl, sizeof(int));
								CHECK_MEM(lat);
								lat[0] = v->lat.lo; 
								lat[2] = v->lat.hi;
								lat[1] = cache.cmp + 1;
					 }
					 else
					 {
								*nl = 2;
								lat = (int *) calloc(*nl, sizeof(int));
								CHECK_MEM(lat);
								lat[0] = v->lat.lo; 
								lat[1] = v->lat.hi;
					 }
		  }
		  else
		  {
					 /* FIXME: consider contentions!!!! */
					 *nl = v->lat.hi - v->lat.lo + 1;
					 lat = (int *) calloc (*nl, sizeof(int));
					 CHECK_MEM(lat);
					 
					 for (j = v->lat.lo; j <= v->lat.hi; j++)
								lat[j - v->lat.lo] = j; 			
		  }
		  
		  return lat;
}


int max_delay_from_offset(egraph_node_t* v)
{
		  offset_link_t* iter;
		  int max_delay = 0;
		  int d1;
		  int* all_latency = NULL;
		  int nlatencies = 0;
		  int j;

		  assert(v->stage == 0);
		  
		  all_latency = get_all_latencies(v, &nlatencies);
		  
		  for (iter = v->offset_in; iter; iter = iter->next)
		  {
				for (j = 0; j < nlatencies; j++)
				{
					 d1 = compute_bus_delay(iter->off_val, all_latency[j]);
					 
					 if (max_delay < d1)
								max_delay = d1;
				}	 
		  }

		  if(all_latency)
					 free(all_latency);

		  return max_delay;
}

/* compute the maximum latency from a given execution graph edge latency 
 * (specified by edge_lat) */
int max_edge_latency(egraph_node_t* v, int edge_lat)
{
		  offset_link_t* iter;
		  int max_delay = 0;
		  int d1;
		  
		  /* if shared bus is not enable just return the maximum latency */
		  if(g_shared_bus_type == -1)
					 return edge_lat;

		  /* Only fetch stage latency is affected by the shared bus, all other stage 
			* latency must be independent of shared bus */
		  if (v->stage != 0)
					 return edge_lat;

		  /* If the IF node is an L1 cache hit, shared bus is not accessed */
		  if (v->lat.lo == 1 && v->lat.hi == 1)
					 return edge_lat;
		  
		  for (iter = v->offset_in; iter; iter = iter->next)
		  {
					 d1 = compute_bus_delay(iter->off_val, edge_lat);
		  			 
					 if (max_delay < d1)
								max_delay = d1;
		  }

		  return max_delay;
}

int max_latency(egraph_node_t* v)
{
		  /* if shared bus is not enable just return the maximum latency */
		  if(g_shared_bus_type == -1)
					 return v->lat.hi;

		  /* Only fetch stage latency is affected by the shared bus, all other stage 
			* latency must be independent of shared bus */
		  if (v->stage != 0)
					 return v->lat.hi;

		  /* If the IF node is an L1 cache hit, shared bus is not accessed */
		  if (v->lat.lo == 1 && v->lat.hi == 1)
					 return v->lat.hi;

		  return max_delay_from_offset(v);
}

int min_delay_from_offset(egraph_node_t* v)
{
		  offset_link_t* iter;
		  int min_delay = INFTY;
		  int d1, j;
		  int* all_latency = NULL;
		  int nlatencies = 0;
		  
		  all_latency = get_all_latencies(v, &nlatencies);
		  
		  for (iter = v->offset_in; iter; iter = iter->next)
		  {

				for (j = 0; j < nlatencies; j++) {
					 d1 = compute_bus_delay(iter->off_val, all_latency[j]);
				
					 if (d1 < min_delay)
								min_delay = d1;
				}				
		  }

		  if(all_latency)
					 free(all_latency);

		  return min_delay;
}

/* compute the minimum latency from a given execution graph edge latency 
 * (specified by edge_lat) */
int min_edge_latency(egraph_node_t* v, int edge_lat)
{
		  offset_link_t* iter;
		  int min_delay = INFTY;
		  int d1;
		  
		  /* if shared bus is not enable just return the maximum latency */
		  if(g_shared_bus_type == -1)
					 return edge_lat;

		  /* Only fetch stage latency is affected by the shared bus, all other stage 
			* latency must be independent of shared bus */
		  if (v->stage != 0)
					 return edge_lat;

		  /* If the IF node is an L1 cache hit, shared bus is not accessed */
		  if (v->lat.lo == 1 && v->lat.hi == 1)
					 return edge_lat;
		  
		  for (iter = v->offset_in; iter; iter = iter->next)
		  {
					 d1 = compute_bus_delay(iter->off_val, edge_lat);
		  			 
					 if (min_delay > d1)
								min_delay = d1;
		  }

		  return min_delay;
}

int min_latency(egraph_node_t* v)
{
		  /* if shared bus is not enabled, just return the minimum latency */
		  if (g_shared_bus_type == -1)
					 return v->lat.lo;

		  /* Only fetch stage latency is affected by the shared bus, all other stage 
			* latency must be independent of shared bus */
		  if(v->stage != 0)
					 return v->lat.lo;

		  if(v->lat.lo == 1 && v->lat.hi == 1)
					 return v->lat.lo;
		  
		  return min_delay_from_offset(v);
}


/* add one offset value to the offset link (only if it is not present already) */
void link_offsets(offset_link_t** offset_link, int off_val)
{
		  offset_link_t* off;
		  offset_link_t* iter;

		  for (iter = *offset_link; iter; iter = iter->next)
		  {
					 if(iter->off_val == off_val)
								return;
		  }

		  off = (offset_link_t *) calloc(1, sizeof(offset_link_t));
		  CHECK_MEM(off);
		  off->off_val = off_val;
		  off->next = NULL;
		  off->next = *offset_link;
		  *offset_link = off;
}

/* free all offsets in the offset link */
void freeOffsets(offset_link_t* offset_link)
{
		  offset_link_t* iter;

		  /* nothing to free */
		  if(offset_link == NULL)
					 return;

		  freeOffsets(offset_link->next);
		  
		  offset_link->next = NULL;
		  free(offset_link);
}

/* get length of the offset link */
static int get_length(offset_link_t* offset_link)
{
		  int n = 0;
		  offset_link_t* iter;

		  for(iter = offset_link; iter; iter = iter->next) 
					 n++;

#ifdef _NDEBUG_CRPD
		  fprintf(stdout, "offset length = %d\n", n);
#endif
		  
		  return n;
}

/* compute offset_out of a egraph node "v" from the offset_in of the same node */
void update_offsets(egraph_node_t* v)
{
		  offset_link_t* iter;
		  int off_out;
		  int* all_latency = NULL;
		  int nlatencies = 0;
		  int j;
		  
		  /* cleekee: code has memory leakage issue, disable to debug */
#if 1
		  if (v->offset_out != NULL)
		  {
					 //freeOffsets(v->offset_out);
					 v->offset_out = NULL;
		  }
#endif		  
		  all_latency = get_all_latencies(v, &nlatencies);

		  for (iter = v->offset_in; iter; iter = iter->next)
		  {
					 for (j = 0; j < nlatencies; j++) {
								off_out = compute_offset_out(v, iter->off_val, all_latency[j]);
								/* add one single offset to the exit offset structure */
								link_offsets(&v->offset_out, off_out);
					 }
		  }

		  if(all_latency)
					 free(all_latency);
}

/* merge multiple offset links */
offset_link_t* merge_offsets(offset_link_t* offset1, offset_link_t* offset2)
{
		  offset_link_t* iter;
		  offset_link_t* new_offset = NULL;
		  
		  for (iter = offset1; iter; iter = iter->next)
		  {
					 link_offsets(&new_offset, iter->off_val);
		  }
		  for (iter = offset2; iter; iter = iter->next)
		  {
					 link_offsets(&new_offset, iter->off_val);
		  }
		  
		  return new_offset;
}

/* This function is needed for propagating set of offsets through 
 * execution graph nodes. This procedure is not as straight forward 
 * as the merge operation */

offset_link_t* propagate_offsets(egraph_node_t* v)
{
		  egraph_edge_t* e = NULL;
		  egraph_node_t* v_in = NULL;
		  egraph_node_t* max_v = NULL;
		  int max_time = -INFTY - 1;
		  offset_link_t* ret_offsets = NULL;
		  offset_link_t* tmp_offsets = NULL;
		  offset_link_t* iter = NULL;

		  /* first find out the predecessor which has latest finish time */
		  for (e = v->in; e != NULL; e = e->next_in)
		  {
					 /* propagate offsets only for normal edges */
					 if(e->normal != EG_NORM_EDGE)
								continue;

					 /* FIXME: zero latency edge */
					 //if (e->lat.lo == 0 && e->lat.hi == 0)
							  //continue;

					 v_in = e->src;		 
					 if (v_in->offset_out && v_in->offset_out == v->offset_out)
							  printf("Gotcha!\n");
					 
					 if (max_time < v_in->fin.hi)
					 {
								max_time = v_in->fin.hi;
								max_v = v_in;
					 }
		  }

		  /* first node in the execution graph */
		  if (!max_v)
					 return NULL;

		  /* add max_v */
		  tmp_offsets = ret_offsets;
		  ret_offsets = merge_offsets(tmp_offsets, max_v->offset_out);
		  freeOffsets(tmp_offsets);

#if 1
		  /* in the second step merge all predecessors which may finish after max_v */
		  /* this condition is not satisfied for two nodes "u" and "v" if and only 
			* if u->fin.hi < v->fin.lo or v->fin.hi < u->fin.lo */
		  for (e = v->in; e != NULL; e = e->next_in)
		  {
					 if (e->src == max_v)
								continue;
					 /* max_v always finishes after e->src */			
					 if (/*e->src->fin.lo > max_v->fin.hi || */ e->src->fin.hi < max_v->fin.lo)
								continue;
					 
					 /* FIXME: zero latency edge */
					//if (e->lat.lo == 0 && e->lat.hi == 0)
							  //continue;
					 
					 /* add e->src */
					 tmp_offsets = ret_offsets;
					 ret_offsets = merge_offsets(tmp_offsets, e->src->offset_out);
					 freeOffsets(tmp_offsets);
#ifdef _NDEBUG_CRPD
					printf("Incoming count = %d ", e->src->merge_count);
					printf("Previous = %d ", v->merge_count);
					v->merge_count += e->src->merge_count;
					printf("After = %d\n", v->merge_count);
#endif
		  }
#endif
		  return ret_offsets;
}

/* check whether two offset links are identical */
int unique_offsets(offset_link_t* offset1, offset_link_t* offset2)
{
		  int len1, len2;
		  offset_link_t* iter1;
		  offset_link_t* iter2;
		  int found = 0;

		  /* if shared bus is not enabled, offset comparison is also 
			* disabled */
		  if(g_shared_bus_type == -1)
					 return 1;

		  len1 = get_length(offset1);
		  len2 = get_length(offset2);

		  if (len1 != len2) return 0;

		  for (iter1 = offset1; iter1; iter1 = iter1->next)
		  {
					found = 0;

					for(iter2 = offset2; iter2; iter2 = iter2->next)
					{
							if(iter2->off_val != iter1->off_val)
								continue;
							found = 1;	
					}
					if (found == 0)
							return 0;	
		  }

		  return 1;
}

#ifdef _DEBUG_CRPD
void dump_offsets(offset_link_t* off)
{
		  offset_link_t* iter;
		  int count = 0;

		  fprintf(stdout, "[");
		  
		  for (iter = off; iter; iter = iter->next)
		  {
					 if(iter->next == NULL)
								fprintf(stdout, "%d", iter->off_val);
					 else
								fprintf(stdout, "%d,", iter->off_val);
							  count++;
		  }			 
		  
		  fprintf(stdout, "]\n");
		  fprintf(stdout, "Printed offset length = %d\n", count);

}

void dump_offsets_egraph(egraph_node_t* v)
{
		  offset_link_t* iter;
		  egraph_edge_t* e;

		  if(g_shared_bus_type == NOBUS)
					 return;
		  fprintf(stdout, "\n--------------------------------------------------------------\n");
		  fprintf(stdout, "MERGE COUNT OF EGRAPH NODE = %d\n", v->merge_count);
		  fprintf(stdout, "\n-----Estimating Basic Block = [%d.%d]-----\n", estimating_body->bb->proc->id, estimating_body->bb->id);
		  fprintf(stdout, "Instruction index = %d\n", v->inst - first_b);
		  fprintf(stdout, "Pipeline stage = %d\n", v->stage);
		  fprintf(stdout, "Printing incoming offsets\n");
		  if (v->in == NULL)
					 fprintf(stdout, "NIL\n");
		  for (e = v->in; e != NULL; e = e->next_in)
		  {
					 fprintf(stdout, "Incoming [inst=%d. stage=%d]\n", e->src->inst - first_b, e->src->stage); 
					 fprintf(stdout, "[ready = %d, eFinish = %d, lFinish = %d]\n", e->src->rdy.lo, e->src->fin.lo, e->src->fin.hi);
					 dump_offsets(e->src->offset_out);			 
		  }
		
		  fprintf(stdout, "Printing Merged offsets\n");
		  dump_offsets(v->offset_in);

		  fprintf(stdout, "Printing latency of node\n");
		  fprintf(stdout, "[%d,%d]\n", v->lat.lo, v->lat.hi);
		  fprintf(stdout, "Printing time parameters of node\n");
		  fprintf(stdout, "[ready = %d, eFinish = %d, lFinish = %d]\n", v->rdy.lo, v->fin.lo, v->fin.hi);
		  fprintf(stdout, "Printing outgoing offsets\n");
		  dump_offsets(v->offset_out); 
		  fprintf(stdout, "--------------------------------------------------------------\n");
}
#endif

