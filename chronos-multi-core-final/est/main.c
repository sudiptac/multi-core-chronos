/*******************************************************************************
 *
 * Chronos: A Timing Analyzer for Embedded Software
 * =============================================================================
 * http://www.comp.nus.edu.sg/~rpembed/chronos/
 *
 * Copyright (C) 2005 Xianfeng Li
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * $Id: main.c,v 1.3 2006/07/15 03:22:50 lixianfe Exp $
 *
 ******************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cfg.h"
#include "bpred.h"
#include "cache.h"
#include "address.h"
#include "task.h"
#include "loops.h"
#include "sharedcache.h"
#include "busconfig.h"
#include "tcfg.h"
#include "offsetGraph.h"
#include "composeAIMC.h"

char DEBUG_INFEAS = 0;

int PROGRESS_STEP = 10000;
extern int num_tcfg_nodes;
extern int num_tcfg_edges;
extern int tcfg_size;
extern tcfg_node_t** tcfg;
extern tcfg_edge_t** tcfg_edges;
extern tcfg_nlink_t*** bbi_map;

extern char *run_opt;
extern FILE *filp, *fusr;

int	    bpred_scheme;
int	    enable_icache;
int 		 enable_dcache = 0;				/* For enabling data cache analysis */
int 		 enable_il2cache = 0;			/* For enabling level 2 instruction cache analysis */	
int 		 enable_ul2cache = 0;			/* For enabling level 2 unified cache analysis */	
int 		 enable_abs_inst_cache = 0;		/* For enabling abstract instruction 
													 * cache analysis */
char*		current_task_name;
prog_t	    prog;

enum ILPSolver {
		  ILP_CPLEX,
		  ILP_LPSOLVE
};

// vivy: infeasible path analysis
#include "infeasible.h"
char enable_infeas;

// vivy: marker for procedures to include in estimation
char *include_proc;

/* sudiptac:: For performance measurement */
typedef unsigned long long ticks;
#define CPU_MHZ 3000000000
static __inline__ ticks getticks(void)
{
		  unsigned a, d;
		  __asm volatile("rdtsc" : "=a" (a), "=d" (d));
		  return ((ticks)a) | (((ticks)d) << 32);
}

int findproc( int addr ) {
		  int i;
		  for( i = 0; i < prog.num_procs; i++ )
					 if( prog.procs[i].sa == addr )
								return i;
		  return -1;
}

// vivy: read list of functions to include in analysis
// Some analysis steps are expensive so avoid processing unnecessary functions.
int read_functions( char *obj_file ) {

		  FILE *fptr;
		  char fname[80];

		  int id;
		  int addr;
		  char name[80];

		  include_proc = (char*) calloc( prog.num_procs, sizeof(char) );

		  sprintf( fname, "%s.fnlist", obj_file );
		  fptr = fopen( fname, "r" );

		  if( !fptr ) {
					 if( DEBUG_INFEAS )
								printf( "%s.fnlist not found -- all procedures will be included in estimation.\n", obj_file );
					 for( id = 0; id < prog.num_procs; id++ )
								include_proc[id] = 1;
					 return -1;
		  }

		  while( fscanf( fptr, "%x %s", &addr, name ) != EOF ) {
					 id = findproc( addr );
					 if( id != -1 )
								include_proc[id] = 1;
					 else
								printf( "Warning: procedure [0x%x] %s not found.\n", addr, name );
		  }
		  fclose( fptr );
		  return 0;
}


// program flow analysis to construct control flow graphs from objective code
		  static void
path_analysis(char *obj_file)
{
		  // read object code, decode it
		  read_code(obj_file);

		  // create procs and their CFGs from the decoded text
		  build_cfgs();

		  // vivy: read list of functions to include in estimation
		  // do this after prog.procs are established
		  read_functions(obj_file);

		  // transform the CFGs into a global CFG called tcfg (transformed-cfg)
		  prog_tran();

		  // identify loop levels as well as block-loop mapping
		  loop_process();

		  /* vivy: infeasible path analysis */
		  if( enable_infeas )
					 infeas_analysis(obj_file);
}



		  static void
microarch_modeling()
{
		  if (bpred_scheme != NO_BPRED)
					 bpred_analysis();
		  if (enable_icache)
					 cache_analysis();
		  pipe_analysis();
}

		  static int
do_ilp(char *obj_file)
{
		  int	    exit_val;
		  int		wcet_dag;
		  char    s[256];

		  printf("do_ilp...\n");
		  sprintf(s, "%s.lp", obj_file);
		  filp = fopen(s, "w");
		  sprintf(s, "%s.cons", obj_file);
		  fusr = fopen(s, "r");
		  if ((filp == NULL) || (fusr == NULL)) {
					 fprintf(stderr, "fail to open ILP/CONS files for writing/reading\n");
					 exit(1);
		  }
		  constraints();

		  fclose(filp);
		  fclose(fusr);

		  /* vivy: print a cplex version */
		  sprintf( s, "%s.ilp", obj_file );
		  filp = fopen( s, "w" );
		  fprintf( filp, "enter Q\n" );
		  fclose( filp );

		  // same with lp_solve format but no comment supported
		  // sprintf( s, "sed '/\\\\/d' %s.lp >> %s.ilp", obj_file, obj_file );
		  // EDIT: cplex supports the same comment format as lp_solve
		  sprintf( s, "cat %s.lp >> %s.ilp", obj_file, obj_file );
		  system( s );

		  sprintf( s, "%s.ilp", obj_file );
		  filp = fopen( s, "a" );
		  fprintf( filp, "optimize\n" );
		  fprintf( filp, "set logfile %s.sol\n", obj_file );
		  fprintf( filp, "display solution objective\n" );
		  fprintf( filp, "display solution variables -\n" );
		  fprintf( filp, "quit\n" );
		  fclose( filp );

		  sprintf(s, "%s.lp", obj_file);
		  wcet_dag = (int) solve_ILP( s, ILP_LPSOLVE );

		 /* 
		  sprintf(s, "%s.lp", obj_file);
char commandline[1024];
sprintf( commandline, "LD_LIBRARY_PATH=%s:$LD_LIBRARY_PATH %s/lp_solve"
        " -presolve -rxli xli_CPLEX %s > %s\n", "~/lp_solve", "~/lp_solve",
		        s, "wcetprog_ilp.result" );

					    const int ret = system( commandline );
*/

/* cleekee: debugging purpose, can remove */
#ifdef _DEBUG_WCET
		  fprintf(stdout, "\n------------------<Printing WCET of DAG>------------------------------\n");
		  fprintf(stdout, "task name = %s\n", obj_file);
		  fprintf(stdout, "WCET of DAG = %d\n", wcet_dag);
		  fprintf(stdout, "-----------------------<END>------------------------------------------\n\n");
#endif
		  return wcet_dag;

		  /* Command:
			* rm -f %s.sol; cplex < %s.ilp >/dev/null 2>/dev/null; cat %s.sol | sed '/^/s/Obj/obj/'
			*/
}



		  int
run_est(char *obj_file)
{
		  int i;
		  int wcet_dag;

#if 0
		  fprintf(stdout, "Topological order\n");
		  for (i = 0; i < num_tcfg_nodes; i++)
		  {
					 fprintf(stdout, "Topo %d = %d\n", i, topo_tcfg[i]);
		  }
		  fprintf(stdout, "Topological order\n");
#endif
		  /* sudiptac: moved the micro-arch modeling part in main function 
			* this function is called only to generate constraints and solve 
			* ILP problem */
		  /* microarch_modeling(); */
		  wcet_dag = do_ilp(obj_file);
		  /* wcet_dag = do_wcet_path(); */

		  return wcet_dag;
}



		  static void
run_cfg(char *obj_file)
{
		  int	    i;
		  char    s[128];
		  FILE    *fcfg;

		  sprintf(s, "%s.cfg", obj_file);
		  //printf("dumping control flow graphs to file:%s\n", s);
		  fcfg = fopen(s, "w");
		  if (fcfg == NULL) {
					 fprintf(stderr, "fail to create file: %s.cfg\n", s);
					 exit(1);
		  }

		  for (i=0; i<prog.num_procs; i++) {
					 dump_cfg(fcfg, &prog.procs[i]);
		  }
		  fclose(fcfg);

		  //printf("done.\n");
}


extern int fetch_width;

/* modification to indirect jump */
/* liangyun */

typedef struct jptb{
		  addr_t adr;
		  int    ntarget;
		  addr_t *ptarget;
		  int    index;
}jptb;

jptb * pjptb;
int    bjptb = 0;
int    njp;

int lookup_jptable(addr_t adr){
		  int i;
		  int num = 0;
		  for(i = 0; i < njp; i++){
					 if(pjptb[i].adr == adr ){
								num = pjptb[i].ntarget;
					 }
		  }
		  return num;        
}

void get_jptable(addr_t src,int index, addr_t * target){
		  int i;
		  for(i = 0; i < njp; i++){
					 if(pjptb[i].adr == src){
								*target = pjptb[i].ptarget[index];
								break;
					 }
		  } 
		  assert(i < njp);
}

int get_jptable_static(addr_t src, addr_t * target){
		  int i,j;
		  int tail = -1;
		  for(i = 0; i < njp; i++){
					 if(pjptb[i].adr == src){
								j = pjptb[i].index;
								*target = pjptb[i].ptarget[j];
								pjptb[i].index++;
								if(pjptb[i].index == pjptb[i].ntarget)
										  tail = 0;
								else  
										  tail = 1;
								break;
					 }
		  } 
		  //assert(i < njp);
		  return tail;
}

/* sudiptac ::: a temporary loop bound reader */
/* FORMAT : <proc_id> <loop_id> <bound> in source code order */
void read_and_set_loop_bounds(char* objfile) 
{
		  char file[128];
		  FILE* fp;
		  int proc_id, loop_id, loop_bound, i;
		  
		  if (num_tcfg_loops == 0)
					 return;
		  
		  sprintf(file, "%s.loopbound", objfile);
		  fp = fopen(file, "r");

		  if (!fp) {
					 fprintf(stdout, "********Loop bound file missing for task %s.....********\n", objfile);
					 fprintf(stdout, "****please create a file name %s.loopbound in the following format****:\n", objfile);
					 fprintf(stdout, "****the file should contain a line in following format for each of the loops in source program****:\n");
					 fprintf(stdout, "****<procedure_id> <loop_id> <upper_bound>****\n");
					 fprintf(stdout, "****procedure_id: procedure id in source program order****\n");
					 fprintf(stdout, "****loop_id: loop id relative to procedure in source program order****\n");
					 fprintf(stdout, "****upper_bound: upper bound of the above loop****\n");
					 fprintf(stdout, "********To know the procedure id and loop id, consider file %s.c and %s.cfg********\n", objfile, objfile);
					 exit(-1);
		  }
		  fprintf(stdout, "reading and setting the loop bounds of task %s.....\n", objfile);
		  while (fscanf(fp, "%d %d %d", &proc_id, &loop_id, &loop_bound) != EOF) 
		  {
					 for (i = 1; i < num_tcfg_loops; i++)
					 {
								/* found the loop ? */
								if (loops[i]->proc_lpid == loop_id && loops[i]->head->bb->proc->id == proc_id) {
										  fprintf(stdout, "set bound %d for loop %d (@0x%x) at procedure %d.....\n", loop_bound, \
													 loop_id, loops[i]->head->bb->sa, proc_id);
										  loops[i]->loopbound = loop_bound;
								}
					 }
		  }

		  /* sanity check, loop[0] is excluded because it is the entire program */
		  for (i = 1; i < num_tcfg_loops; i++) {
					 if (loops[i]->loopbound == 0) {
								fprintf(stdout, "loop bound missing for loop at proc id = %d, loop id = %d\n", \
										  loops[i]->head->bb->proc->id, loops[i]->proc_lpid);
								fprintf(stdout, "ignoring......\n");
					 }
		  }
		  fclose(fp);
}

/* read indirect jump table (if any) */
void read_injp(char * objfile){
		  int  tablesize,i,j;
		  char file[100];
		  FILE *ftable;
		  sprintf(file,"%s.jtable",objfile);
		  ftable = fopen(file,"r");
		  if(!ftable){
					 // no indirect jump
					 bjptb = 0;
		  }else{
					 bjptb = 1;
					 fscanf(ftable,"%d",&tablesize);
					 njp = tablesize;
					 pjptb = (jptb *)calloc(tablesize, sizeof(jptb));
					 for(i = 0; i < tablesize; i++){
								fscanf(ftable,"%08x",&pjptb[i].adr);
								fscanf(ftable,"%d",&pjptb[i].ntarget);
								pjptb[i].index = 0;
								pjptb[i].ptarget = (addr_t *)calloc(pjptb[i].ntarget, sizeof(addr_t));
								for(j = 0; j < pjptb[i].ntarget; j++)
										  fscanf(ftable,"%08x",&pjptb[i].ptarget[j]);
					 }
					 fclose(ftable);
		  }
		  //printf("bool: %d\n",bjptb);
}

int    *pdepth;
int     bdepth = 0;

		  int  test_depth(int pid, int depth){
					 if (depth < pdepth[pid])
								return 1;
					 else
								return 0;
		  }

void read_recursive(char * objfile){
		  char file[100];
		  FILE *ftable;
		  int size,i;
		  sprintf(file,"%s.recursive",objfile);
		  ftable = fopen(file,"r");
		  if(!ftable){
					 bdepth = 0;
		  }else{
					 bdepth = 1;
					 fscanf(ftable,"%d",&size);
					 pdepth = (int *)calloc(size,sizeof(pdepth));
					 for(i = 0; i < size; i++)
								fscanf(ftable,"%d",&pdepth[i]);
					 fclose(ftable);
		  }
}

/* sudiptac :::: analyze two level cache hierarchies */
static void analyze_cache_hierarchy()
{
		  ticks a,b;

		  a = getticks();	  
#ifdef _DEBUG_CRPD
		  printf("Starting cache analysis........\n");
#endif

		  if(enable_icache)
		  {
					 /* FIXME: this flag need to be removed in final version */
					 //enable_abs_inst_cache = 1;
					 
					 /* cleekee: duplicated here from run_est() to access mp instructions */
					 if(bpred_scheme != NO_BPRED)
							  collect_mp_insts();
					 
					 /* sudiptac : analyze instruction cache (abstract interpretation approach) */
					 analyze_abs_instr_cache_all();
		  }

		  b = getticks();	  
#ifdef _DEBUG_CRPD
		  printf("Finished cache analysis........\n");
#endif

		  printf("===================================================\n");
		  printf("Maximum cache analysis time = %lf msecs\n", 1000*(b - a)/((1.0) * CPU_MHZ));
		  printf("===================================================\n");
}	  


		  int
read_and_analyze_one_task(char *argv)
{
		  /* liangyun: read jump table if necessary */   
		  read_injp(argv);

		  /* liangyun: read depth table for recursive function */
		  read_recursive(argv);

		  /* vivy: only these steps are needed to build CFG */
		  if (strcmp(run_opt, "CFG") == 0) {

					 read_code( argv );
					 build_cfgs();
					 run_cfg( argv );
					 return 0;
		  }

		  enable_infeas = 0;

		  path_analysis(argv);
		   
		  /* sudiptac ::: read loop bounds */
		  read_and_set_loop_bounds(argv);

#if 0
		  extern int l1_i1_ps, l1_d1_ps, i1_u1_ps, u1_d1_ps;	  

		  printf("=====================================================\n");
		  printf("PS I1 in L1 = %d\n", l1_i1_ps);
		  printf("PS D1 in L1 = %d\n", l1_d1_ps);
		  printf("PS D1 in U1 ans all-miss/nc at D1 = %d\n", u1_d1_ps);
		  printf("PS I1 in U1 ans all-miss/nc at L1 = %d\n", i1_u1_ps);
		  printf("=====================================================\n");
#endif

		  //if (strcmp(run_opt, "CFG") == 0)
		  //run_cfg(argv[argc - 1]);
		  //else

		  //run_est(argv);
}

/* sudiptac :::: reset all relevant data structures before analyzing 
 * a new task */
/* This reset is exteremely important as we don't want to affect 
 * one task's structure to be affected by some other task */
static void reset()
{
		  /* reset TCFG related structure */
		  num_tcfg_nodes = num_tcfg_edges = tcfg_size = 0;
		  tcfg_edges = NULL; tcfg = NULL;
		  bbi_map = NULL; topo_tcfg = NULL;

		  /* reset loop related structures */
		  loops = NULL; loop_map = NULL; loop_comm_ances = NULL;
		  num_tcfg_loops = 0;

		  /* add here */
		  inst_chmc_l1 = inst_chmc_l2 = NULL;
		  inst_age_l1 = inst_age_l2 = NULL;
		  ecb_log_l1 = ecb_log_l2 = NULL;

}

/* set analysis for this_task */
void set_task(task_p this_task)
{
		  /* set TCFG related parameters */
		  num_tcfg_nodes = this_task->num_tcfg_nodes;
		  num_tcfg_edges = this_task->num_tcfg_edges;
		  tcfg = this_task->tcfg;
		  tcfg_edges = this_task->tcfg_edges;
		  bbi_map = this_task->bbi_map;
		  topo_tcfg = this_task->topo_tcfg;

		  /* set loop related parameters */
		  num_tcfg_loops = this_task->num_tcfg_loops;
		  loops = this_task->loops;
		  loop_map = this_task->loop_map;
		  loop_comm_ances = this_task->loop_comm_ances;

		  /* set analysis related paramters */
		  inst_chmc_l1 = this_task->inst_chmc_l1;
		  inst_chmc_l2 = this_task->inst_chmc_l2;
		  inst_age_l1 = this_task->inst_age_l1;
		  inst_age_l2 = this_task->inst_age_l2;
		  ecb_log_l1 = this_task->ecb_log_l1;
		  ecb_log_l2 = this_task->ecb_log_l2;
}

/* sudiptac :::: read and analyze each task in the task graph */
static void read_and_analyze_task_graph(FILE* tfp)
{
		  int tid = 0, i;
		  char tname[MAX_TASKNAME_LEN];
		  Queue queue;
		  
		  fscanf(tfp, "%d", &ntasks);
		  /* allocate memory for all the tasks */
		  tasks = (task_p *) calloc(ntasks, sizeof(task_p));
		  CHECK_MEM(tasks);
		  
		  while (tid < ntasks)
		  {
					 reset();

					 fscanf(tfp, "%s", tname);

					 /* allocate memory for current task */
					 tasks[tid] = (task_p) calloc(1, sizeof(task_s));
					 CHECK_MEM(tasks[tid]);

					 tasks[tid]->name = (char *) calloc(strlen(tname) + 1, sizeof(char));
					 strcpy(tasks[tid]->name, tname);

					 tasks[tid]->tid = tid;
					 tasks[tid]->start_time_low = tasks[tid]->start_time_high = 0;

					 /* task parameters */
					 fscanf(tfp, "%d", &(tasks[tid]->priority));
					 fscanf(tfp, "%d", &(tasks[tid]->ncore));
					 /* FIXME: remove later */
					 fscanf(tfp, "%d", &(tasks[tid]->mscid));
#ifdef _DEBUG_CRPD
					 printf("Task %s....priority - %d\n", tname, tasks[tid]->priority); 
					 printf("Task %s....assigned core - %d\n", tname, tasks[tid]->ncore);
					 printf("Task %s....assigned MSC - %d\n", tname, tasks[tid]->mscid);
#endif
					 /* successor list */
					 fscanf(tfp, "%d", &(tasks[tid]->nsucc));
					 for (i = 0; i < tasks[tid]->nsucc; i++)
					 {
								if (!tasks[tid]->succ)
										  tasks[tid]->succ = (int *) calloc(tasks[tid]->nsucc, sizeof(int));
								fscanf(tfp, "%d", &(tasks[tid]->succ[i]));
#ifdef _DEBUG_CRPD
								printf("Reading successor %d.....\n", tasks[tid]->succ[i]);
#endif
					 }

					 /* read and construct CFG of one task */
					 read_and_analyze_one_task(tasks[tid]->name);
		   
					 /* set TCFG related parameters */
					 tasks[tid]->num_tcfg_nodes = num_tcfg_nodes;
					 tasks[tid]->num_tcfg_edges = num_tcfg_edges;
					 tasks[tid]->tcfg = tcfg;
					 tasks[tid]->tcfg_edges = tcfg_edges;
					 /* used for mapping CFG node to all TCFG nodes */
					 tasks[tid]->bbi_map = bbi_map;

					 /* set loop related parameters */
					 tasks[tid]->num_tcfg_loops = num_tcfg_loops;
					 tasks[tid]->loops = loops;
					 tasks[tid]->loop_map = loop_map;
					 tasks[tid]->loop_comm_ances = loop_comm_ances;
					 
					 /* sudiptac: order the TCFG edges in topological order */
					 init_queue(&queue, sizeof(tcfg_node_t *));
					 enqueue(&queue, &tcfg[0]);
					 topo_tcfg = (int *) calloc(num_tcfg_nodes, sizeof(int));
					 CHECK_MEM(topo_tcfg);
					 num_topo_tcfg = 0;
					 //set_id_tcfg_edges(queue);
					 set_topological_tcfg();
					 free_queue(&queue);
					 tasks[tid]->topo_tcfg = topo_tcfg;
					 
					 collect_tcfg_edges();
					 tasks[tid]->tcfg_edges = tcfg_edges;
					 
					 
					 /* sudiptac : a two level cache analysis */
					 analyze_cache_hierarchy();

					 /* set cache related parameters (after intra-core cache analysis) */
					 tasks[tid]->inst_chmc_l1 = inst_chmc_l1;
					 tasks[tid]->inst_chmc_l2 = inst_chmc_l2;
					 tasks[tid]->inst_age_l1 = inst_age_l1;
					 tasks[tid]->inst_age_l2 = inst_age_l2;

					 /* set evicted cache blocks :::: needed for CRPD analysis */
					 tasks[tid]->ecb_log_l1 = ecb_log_l1;
					 tasks[tid]->ecb_log_l2 = ecb_log_l2;
					 
					 tid++;
		  }
}


/* sudiptac :::: entry point when concurrent task graphs are handled */
int main(int argc, char** argv)
{
		  FILE* fptr;
		  int i;
		  ticks start_a, start_b;	

		  if (argc <= 1) {
					 fprintf(stderr, "Usage:\n");
					 fprintf(stderr, "%s <options> <task_graph>\n", argv[0]);
					 exit(1);
		  }

		  fptr = fopen(argv[argc-1], "r");
		  
		  if (!fptr)
		  {
					 fprintf(stderr, "Task graph file not specified\n");
					 exit(1);
		  }
		  
		  /* initialize ISA and the decoder here before reading any task */
		  init_isa();
		  md_init_decoder();

		  /* read options including (1) actions; (2) processor configuration */
		  /* number of cores and shared bus options */
		  /* sudiptac ::: moving the code here, assuming homogeneous processor 
			* cores */
		  read_opt(argc, argv);

		  start_a = getticks();
		  read_and_analyze_task_graph(fptr);

		  /* shared L2 cache conflict analysis */
		  if(enable_il2cache)
					 compute_inter_core_conflicts();		  
		  
		  /* run estimation */
		  //for (i = 0; i < ntasks; i++)
		  for (i = 0; i <= 0; i++)
		  {
					 set_task(tasks[i]);
					 /* cleekee: used in creating .ilp file */
					 current_task_name = tasks[i]->name;
#ifdef _DEBUG_WCET
					 fprintf(stdout, "\n--------------------------------------------------------------------------------\n");
					 fprintf(stdout, "Starting estimation of task %s....\n", tasks[i]->name);
#endif
		  			 microarch_modeling();
					 start_b = getticks();
		  			 fprintf(stdout, "Micro-arch modeling time = %lf msecs\n", 1000*(start_b - start_a)/((1.0) * CPU_MHZ));
		  			 start_a = getticks();
					 tasks[i]->wcet = run_est(tasks[i]->name);
		  			 start_b = getticks();
		  			 fprintf(stdout, "ILP solving time = %lf msecs\n", 1000*(start_b - start_a)/((1.0) * CPU_MHZ));
#ifdef _DEBUG_WCET
					 fprintf(stdout, "Ending estimation of task %s....\n", tasks[i]->name);
					 fprintf(stdout, "--------------------------------------------------------------------------------\n");
#endif
		  }

			/* AI+MC starts .... */
		 
		 /* set the oracle type for shared cache analysis and CRPD analysis refinement */		
		 if(getenv("RUN_AI_MC_SHARED_CACHE") || getenv("RUN_AI_MC_CRPD")) {
					 if (argc <= 4) 
							assert(0 && "you are a SATAN.....provide a refinement ORACLE ----- CBMC|KLEE");	
					 /* set the oracle type */		
					 if (strcmp(argv[4], "KLEE") == 0) {
							oracle_type = TYPE_SYMBOLIC_EXECUTION;
							printf("\n.....KLEE set as an oracle.....\n");
					 }					
					 else if (strcmp(argv[4], "CBMC") == 0) {	
							oracle_type = TYPE_MODEL_CHECK;
							printf("\n.....CBMC set as an oracle.....\n");
					 }					
					 else
							assert(0 && "you are a SATAN.....provide a proper ORACLE.....");	
					 /* bound the number of calls of the oracle */
					 if (argc > 5) {
								refinement_bound = atoi(argv[5]);
								printf("\n.....maximum number of refinement try set to %d.....\n", refinement_bound);
					}			
			}

	
		  /* refine cache analysis with model checking */
			if (getenv("RUN_AI_MC_SHARED_CACHE")) {
					 refine_cache_analysis_with_MC();
			}		 

			/* AI+MC ends .... */
			
			/* CRPD analysis */
			/* FIXME: remove the environment later */	
			if(getenv("RUN_CRPD"))
				crpd_analysis();	
}
