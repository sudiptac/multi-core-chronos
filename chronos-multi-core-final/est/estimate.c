/*****************************************************************************
 *
 * Chronos: A Timing Analyzer for Embedded Software
 * =============================================================================
 * http://www.comp.nus.edu.sg/~rpembed/chronos/
 *
 * Copyright (C) 2005 Xianfeng Li
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * $Id: estimate.c,v 1.2 2006/06/24 08:54:56 lixianfe Exp $
 *
 ****************************************************************************/

#include "common.h"
#include "bpred.h"
#include "pipeline.h"
#include "loops.h"
#include "exegraph.h"
#include "sharedbus.h"
#include "busconfig.h"
#include "offsetData.h"
#include "offsetGraph.h"
#include "ss/machine.h"
#include "ss/ss_machine.h"

#define	LATE	    0
#define	EARLY	    1
#define STEP_SEP    0
#define STEP_EST    1
#define MAX_ITERS   16
#define SHADED	    1


extern int		pipe_stages;
extern egraph_node_t	**egraph;
extern int		eg_len, plog_len, elog_len, body_len;
extern range16_t	*coexist;
extern int		*eg_inst_succ;
extern eg_chain_t	*eg_chain;
extern mas_inst_t	**eg_insts;
extern int		ruu_issue_width;
extern int		ruu_commit_width;
extern int		pipe_iwin_size;
extern int		bpred_type;	// from exegraph.c
extern int num_tcfg_edges;
extern tcfg_edge_t** tcfg_edges;
extern tcfg_node_t** tcfg;
extern code_link_t	**prologs;
extern mas_inst_t	**bodies;
extern mas_inst_t *start_body;

// either find_separation (STEP_SEP) or make_estimation (STEP_EST)
int		step = STEP_SEP;
int		changed;
// last_p: last prolog instr; first_b: first body instr; ...
// last_np: last normal prolog instr
int		last_p, last_np, first_b, last_b, first_e;



void
dump_egraph_times(int start, int end);

void
check_fetch_times();



// can only init latest times for prologues
		  static void
init_shaded_pnode(egraph_edge_t *e)
{
		  int		    hi, str, fin;
		  egraph_node_t   *src, *dst;

		  src = e->src; dst = e->dst;
		  str = dst->rdy.hi - e->lat.lo;

		  if ((src->flag != SHADED) || (src->str.hi > str)) {
					 src->rdy.hi = src->str.hi = str;
					 if (src->lat.lo == e->lat.lo)
								src->fin.hi = dst->rdy.hi;
					 else if (src->flag != SHADED)
								src->fin.hi = str + src->lat.hi;
					 else
								src->fin.hi = min(src->fin.hi, str + src->lat.hi);
					 if (src->flag != SHADED) {
								src->flag = SHADED;
								src->rdy.lo = src->str.lo = src->fin.lo = -INFTY;
					 }
					 for (e = src->in; e != NULL; e = e->next_in) {
								if (e->normal == EG_NORM_EDGE)
										  init_shaded_pnode(e);
					 }
		  }
}



		  static void
reset_nodes_flags(int first, int last)
{
		  int	    inst, stage, event;

		  for (inst = first; inst <= last; inst++) {
					 for (stage = 0; stage < pipe_stages; stage++)
								egraph[inst][stage].flag = 0;
		  }
}



// init intervals of egraph nodes (prolog/body/epilog) by assuming 0 at the
// first body node: t(first_b, IF, ready) = 0
		  static void
init_sep()
{
		  int		    inst, stage;
		  egraph_node_t   *vb, *v;
		  egraph_edge_t   *e;

		  // assume t(first_b, IF, ready) = 0
		  vb = &egraph[first_b][0];
		  vb->rdy.lo = vb->rdy.hi = 0;
		  vb->str.lo = vb->fin.lo = 0;
		  vb->str.hi = vb->fin.hi = INFTY;
		  for (e = vb->in; e != NULL; e = e->next_in)
					 init_shaded_pnode(e);

		  for (inst = 0; inst < eg_len; inst++) {
					 for (stage = 0; stage < pipe_stages; stage++) {
								v = &egraph[inst][stage];
								if ((v->flag == SHADED) || (v == vb))
										  continue;
								if (v->inst >= first_b) {
										  v->rdy.lo = v->str.lo = 0;
										  v->fin.lo = v->lat.lo;
								} else {
										  v->rdy.lo = v->str.lo = v->fin.lo = -INFTY;
								}
								v->rdy.hi = v->str.hi = v->fin.hi = INFTY;
					 }
		  }
}



		  static int
lcontd_delay(egraph_node_t *v)
{
		  int		    new_hi, tmp;
		  egraph_node_t   *u; 
		  egraph_edge_t   *e;

		  if (v->l_contd == NULL)
					 return v->rdy.hi;

		  if (v->inst >= first_e) {
					 if (v->lat.hi > 1)
								return v->rdy.hi + v->lat.hi - 1;
		  }

		  new_hi = v->rdy.hi;
		  for (e = v->l_contd; e != NULL; e = e->next_out) {
					 if (e->contd_type != BI_DELAY)
								continue;
					 u = e->dst;
					 tmp = min(u->fin.hi, v->rdy.hi + u->lat.hi - 1);
					 new_hi = max(new_hi, tmp);
		  }
		  return new_hi;
}



		  static int
econtd_delay(egraph_node_t *v, int new_hi)
{
		  egraph_node_t   *u; 
		  egraph_edge_t   *e;
		  int		    max_u = new_hi, sum_d = 0;

		  if (v->e_contd == NULL)
					 return new_hi;

		  for (e = v->e_contd; e != NULL; e = e->next_in) {
					 if (e->contd_type == NO_DELAY)
								continue;
					 u = e->src;
					 if (u->fin.hi <= new_hi)
								continue;
					 sum_d += min(u->fin.hi - new_hi, u->lat.hi);
					 max_u = max(max_u, u->fin.hi);
		  }
		  new_hi = min(max_u, new_hi + sum_d / v->num_fu);
		  return new_hi;
}



		  static int
scalar_delay(egraph_node_t *v, int new_hi)
{
		  // different policies for considering peers whose latest start times are within
		  // or beyond PEERS_WIN-1 cycles from new_hi
#define	PEERS_WIN   6
		  int		    inst, i, d, rest_peers, mrest, tmp;
		  // mpeers are ld/st which consume issue width twice: issue of address
		  // calculation followed by issue of actual ld/st
		  // FIXME: in current processor model, the second issue takes place *exactly*
		  // one cycle after the first issue since addr calc takes one cycle and all
		  // dependencies have been resolved by the first issue, thus the second issue
		  // will not be delayed further. If this assumption does not hold, then this
		  // implementation do not apply.
		  int		    peers[PEERS_WIN], mpeers[PEERS_WIN];
		  egraph_node_t   *u, *um;

		  for (i = 0; i < PEERS_WIN; i++)
					 peers[i] = mpeers[i] = 0;

		  // collect number of peers in each cycle
		  inst = v->inst;
		  for (i = 1; i < pipe_iwin_size - 1; i++) {
					 inst = eg_chain[inst].pred;
					 if (inst < 0)
								break;
					 u = &egraph[inst][v->stage];
					 d = u->str.hi - new_hi;
					 if (d < 0) {
								if ((u->fu == RdPort) || (u->fu == WrPort)) {
										  um = &egraph[inst][u->stage+1];
										  d = um->str.hi - new_hi;
										  if (d >= 0) {
													 d = min(d, PEERS_WIN-1);
													 peers[d]++;
										  }
								}
					 } else {
								d = min(d, PEERS_WIN-1);
								if ((u->fu == RdPort) || (u->fu == WrPort))
										  mpeers[d]++;
								else
										  peers[d]++;
					 }
		  }
		  for (d = PEERS_WIN-1; d > 0; d--) {
					 peers[d-1] += peers[d];
					 mpeers[d-1] += mpeers[d];
		  }
		  rest_peers = peers[0] + mpeers[0];
		  mrest = 0;
		  for (d = 0; d < PEERS_WIN-1; d++) {
					 if ((rest_peers + mrest) < ruu_issue_width)
								return new_hi + d;
					 tmp = rest_peers;
					 rest_peers = rest_peers - max(ruu_issue_width - mrest, 0);
					 rest_peers = min(rest_peers, peers[d+1] + mpeers[d+1]);
					 mrest = min(mpeers[d], tmp) - min(mpeers[d+1], rest_peers);
		  }
		  new_hi += d + (rest_peers + min(mpeers[d], rest_peers) + mrest) / ruu_issue_width;
		  return new_hi;
}



		  static int
fetch_dep(egraph_edge_t *e)
{
		  return min(e->src->str.hi + max_edge_latency(e->src, e->lat.hi), e->src->fin.hi);
}

		  static int
latest_time(egraph_node_t *v, int off_changed)
{
		  int		    new_hi;
		  egraph_node_t   *u;
		  egraph_edge_t   *e;

		  if (v->in == NULL) {
					 if ((v->l_contd == NULL) && v->str.hi != v->rdy.hi) {
								v->str.hi = v->rdy.hi;
								v->fin.hi = v->str.hi + max_latency(v);
								return 1;
					 } else
								return 0;
		  }
		 
		  /* sudiptac ::: changing the initialization for body nodes. Correct ? */
		  //if (v->inst >= first_b)
					 //new_hi = 0;
		  //else
					 new_hi = -INFTY;
		  
		  for (e = v->in; e != NULL; e = e->next_in) {
					 // FIXME: applicable only if stage 0 is instr fetch
					 if (v->stage == 0 && e->src->stage == 0)
								new_hi = max(new_hi, fetch_dep(e));
					 else
					 /* TODO: cleekee: e->src->fin.hi instead? */
								new_hi = max(new_hi, e->src->str.hi + max_edge_latency(e->src, e->lat.hi));			 
		  }

		  /* TODO: cleekee: experimental */
		  /* sudiptac : disable */
		  //if (v == &egraph[plog_len][0])
					//new_hi = 0;
		  
		  u = &egraph[0][pipe_stages-1];
		  if ((plog_len > 0) && (v->inst > 0) && coexist[v->inst].lo == 0)
					 v->rdy.hi = max(v->rdy.hi, u->rdy.hi - 1);
		  /* TODO: cleekee: value of v->rdy.hi being unconditionally replaced? */
		  v->rdy.hi = new_hi;
		  if (v->fu != 0) {
					 if (v->lat.hi > 1) {
								if (v->inst >= first_e)
										  new_hi += v->lat.hi - 1;
								else
										  new_hi = lcontd_delay(v);
					 }
					 new_hi = econtd_delay(v, new_hi);
		  }
		  
		  /* FIXME: should be generalized to stages constrained by issue width, etc. */
		  if (v->stage == STAGE_EX)
					 new_hi = scalar_delay(v, new_hi);
		  
		  if (new_hi < v->str.hi/*|| off_changed*/) {
					 v->str.hi = new_hi;
					 /* "max_latency" computes the maximum latency in presence of shared bus */
					 v->fin.hi = new_hi + max_latency(v);
					 changed = 1;
					 if ((v->e_contd != NULL) || (v->l_contd != NULL))
								return 1;
		  }
		  
		  return 0;
}


/* earliest time computation of an egraph node */
		  static int
earliest_time(egraph_node_t *v, int off_changed)
{
		  int		    new_lo;
		  egraph_edge_t   *e;

		  if (v->in == NULL)
					 return 0;
		  
		  /* event time inited from dependences */
		  new_lo = -INFTY;
		  /* sudiptac: changing the initialization. Correct ? */
		  //new_lo = 0;
		  
		  for (e = v->in; e != NULL; e = e->next_in) {
					 if (e->normal != EG_NORM_EDGE)
								continue;
					 new_lo = max(new_lo, e->src->str.lo + min_edge_latency(e->src, e->lat.lo));
		  }
		  
		  if (new_lo > v->rdy.lo /*|| off_changed*/) {
					 v->rdy.lo = v->str.lo = new_lo;
					 /* minimum latency of IF node may depend on the incoming offsets, 
					  * min_latency function computes this minimum latency */
					 v->fin.lo = new_lo + min_latency(v);
					 changed = 1;
					 
					 return 1;
		  }
		  
		  return 0;
}



// two nodes cannot contend if
// earliest(node1, ready) >= latest(node2, finish), or
// earliest(node2, ready) >= latest(node1, finish)
// in addition, a late node2 cannot delay an early node1 if
// earliest(node2, start) >= latest(node1, ready)
		  static void
update_contd(egraph_node_t *v)
{
		  egraph_node_t   *u;
		  egraph_edge_t   *e;

		  for (e = v->e_contd; e != NULL; e = e->next_in) {
					 if (e->contd_type == NO_DELAY)
								continue;
					 u = e->src;
					 if ((v->rdy.lo >= u->fin.hi) || (u->rdy.lo >= v->fin.hi))
								e->contd_type = NO_DELAY;
					 else if (v->str.lo >= u->rdy.hi)
								e->contd_type = UNI_DELAY;
		  }
		  for (e = v->l_contd; e != NULL; e = e->next_out) {
					 if (e->contd_type == NO_DELAY)
								continue;
					 u = e->dst;
					 if ((v->rdy.lo >= u->fin.hi) || (u->rdy.lo >= v->fin.hi))
								e->contd_type = NO_DELAY;
					 else if (u->str.lo >= v->rdy.hi)
								e->contd_type = UNI_DELAY;
		  }
}



		  static void
find_sep()
{
		  int		    inst, stage, echanged, lchanged;
		  egraph_node_t   *v;
		  offset_link_t* new_offset_in = NULL;
		  offset_link_t* tmp_offsets = NULL;
		  int off_changed = 0;
		  int first_iteration = 1;

		  init_sep();

		  
		  do {
					 changed = 0;
					 for (inst = 0; inst < eg_len; inst++) {
								for (stage = 0; stage < pipe_stages; stage++) {
										  v = &egraph[inst][stage];
										  /* sudiptac ::: merge and create new offsets for the body nodes */
										  if (inst >= first_b && g_shared_bus_type != NOBUS)
										  {
													 new_offset_in = propagate_offsets(v);
													 
													 if (new_offset_in && !unique_offsets(new_offset_in, v->offset_in)) {
																v->offset_in = new_offset_in;
																off_changed = 1;
													 }
													 else
																off_changed = 0;
										  }
										  else
													 off_changed = 0;
										  if (inst >= first_b)
													 echanged = earliest_time(v, off_changed);
										  lchanged = latest_time(v, off_changed);
										  if (echanged || lchanged)
													 update_contd(v);
										  /* sudiptac ::: update the outgoing offsets */
										  /* update offsets only for body nodes */
										  if (inst >= first_b) {
													if(!first_iteration) {
															freeOffsets(v->offset_out);
															v->offset_out = NULL;
													}
													 update_offsets(v);
										  }
								}
					 }
					 first_iteration = 0;
		  } while (changed);

#ifdef _NDEBUG_CRPD
		  for (inst = 0; inst < eg_len; inst++)
		  {
					 for (stage = 0; stage < pipe_stages; stage++)
					 {
								v = &egraph[inst][stage];
								dump_offsets_egraph(v);
					 }
		  }
#endif
}



		  static void
plog_backtrack(egraph_edge_t *e)
{
		  int		    str, fin, inst, stage;
		  egraph_node_t   *src, *dst, *v;
		  egraph_edge_t   *e1;

		  if (e->normal == EG_COND_EDGE)
					 return;

		  src = e->src; dst = e->dst;
		  src->rdy.lo = src->str.lo = src->fin.lo = -INFTY;
		  // first check & update e->src's latest time with e->dst's latest time, if
		  // updated, propagate this change by recursive backtracking
		  str = dst->rdy.hi - e->lat.lo;

		  if (src->str.hi > str) {
					 src->rdy.hi = src->str.hi = str;
					 if (src->lat.lo == e->lat.lo)
								src->fin.hi = dst->rdy.hi;
					 else
								src->fin.hi = min(src->fin.hi, str + src->lat.hi);
					 // FIXME: execution times of mpred nodes are bounded by branch
					 // write-back, this is only applicable to SimpleScalar
					 if ((eg_chain[src->inst].succ > src->inst + 1) && src->stage == 3) {
								for (inst = src->inst+1; inst < eg_chain[src->inst].succ; inst++) {
										  for (stage = 0; stage < pipe_stages; stage++) {
													 v = &egraph[inst][stage];
													 v->rdy.hi = v->str.hi = v->fin.hi = src->fin.hi;
										  }
								}
					 }
					 for (e = src->in; e != NULL; e = e->next_in)
								plog_backtrack(e);
		  }
}



// backtrack to all prolog nodes and get their latest times (cannot know their
// earliest times via backtracking)
		  static void
init_est_plog()
{
		  int		    inst, stage, i;
		  egraph_node_t   *v;
		  egraph_edge_t   *e;

		  // initialize conservative intervals
		  for (inst = 0; inst <= last_p; inst++) {
					 for (stage = 0; stage < pipe_stages; stage++) {
								v = &egraph[inst][stage];
								v->rdy.lo = v->str.lo = -INFTY;
								v->rdy.hi = v->str.hi = 0;
					 }
		  }
		  v = &egraph[last_np][pipe_stages-1];
		  v->fin.hi = v->fin.lo = 0;
		  v->rdy.lo = -INFTY;
		  v->str.lo = 0 - v->lat.hi;
		  v->rdy.hi = v->str.hi = 0 - v->lat.lo;
		  for (e = v->in; e != NULL; e = e->next_in)
					 plog_backtrack(e);

		  if (bpred_type == BP_CPRED)
					 return;

		  for (inst = last_np + 1; inst <= last_p; inst++) {
					 for (stage = 0; stage < pipe_stages; stage++) {
								v = &egraph[inst][stage];
								v->rdy.hi = v->str.hi = v->fin.hi = egraph[last_np][3].fin.hi;
					 }
		  }
}



		  static void
init_est_body()
{
		  int		    inst, stage;
		  egraph_node_t   *v;

		  // last normal prologue node (there could be mispred instr)
		  for (inst = first_b; inst < eg_len; inst++) {
					 for (stage = 0; stage < pipe_stages; stage++) {
								v = &egraph[inst][stage];
								v->rdy.lo = v->str.lo = 0; 
								v->rdy.hi = v->str.hi = INFTY;
					 }
		  }
}



// for each paths x->y, calculate its length by summing up minimum delay of each node
// along the path; the distance is the length of the longest path
		  static int
distance(egraph_node_t *v, egraph_node_t *u)
{
		  int		    len = 0, tmp;
		  egraph_edge_t   *e;

		  if (v == u)
					 return 0;

		  for (e = v->out; e != NULL; e = e->next_out) {
					 if (e->dst->inst > u->inst)
								continue;
					 tmp = e->lat.lo + distance(e->dst, u);
					 if (tmp > len)
								len = tmp;
		  }
		  return len;
}



// minimal overlap between x and y, this is for overlap between prolog and body
// e.g., where x is (first_body, IF, ready); y is (last_prolog, CM, finish)
		  static int
min_overlap(egraph_node_t *v, egraph_node_t *u)
{
		  int		    d, mo = INFTY;
		  egraph_edge_t   *e;

		  for (e = v->in; e != NULL; e = e->next_in) {
					 d = distance(e->src, u) - e->lat.hi;
					 if (d < mo)
								mo = d;
		  }
		  return mo;
}



		  static void
init_est()
{
		  init_est_plog();
		  init_est_body();
}



// make estimation based on separation info obtained from find_sep()
		  static void
make_est()
{
		  int		    inst, stage;
		  egraph_node_t   *v;
		  offset_link_t* tmp_offsets = NULL;
		  offset_link_t* new_offset_in = NULL;
		  int off_changed = 0;
		  int first_iteration = 1;

		  init_est();
		  
		  do {
					 changed = 0;
					 for (inst = first_b; inst < eg_len; inst++) {
								for (stage = 0; stage < pipe_stages; stage++) {
										  v = &egraph[inst][stage];
										  /* sudiptac ::: merge and create new offsets for the body nodes */
										  if (inst >= first_b && g_shared_bus_type != NOBUS)
										  {
													 new_offset_in = propagate_offsets(v);
													 if (new_offset_in && !unique_offsets(new_offset_in, v->offset_in)) {
																v->offset_in = new_offset_in;
																off_changed = 1;
													 }
													 else
																off_changed = 0;
										  }
										  else
													 off_changed = 0;
										  earliest_time(v, off_changed);
										  latest_time(v, off_changed);

										  if(!first_iteration) {
												  freeOffsets(v->offset_out);
												  v->offset_out = NULL;
										  }
										  update_offsets(v);
								}
					 }
					 first_iteration = 0;
		  } while (changed);

#ifdef _NDEBUG_CRPD
		  for (inst = first_b; inst < eg_len; inst++)
		  {
					 for (stage = 0; stage < pipe_stages; stage++)
					 {
								v = &egraph[inst][stage];
								dump_offsets_egraph(v);
					 }
		  }
#endif
}



		  int
est_egraph()
{
		  int		tm, min_ov;
		  egraph_node_t   *v, *u, *w;

		  first_b = plog_len;
		  last_p = first_b - 1;
		  last_np = eg_chain[first_b].pred;
		  first_e = plog_len + body_len;
		  last_b = first_e - 1;

		  find_sep();
		  // tm is the estimate from find_sep (where time of first body node was
		  // assumed 0), usually it is more conservative than the estimate by the
		  // following make_est procedure, thus tm is not the final estimate
		  // in most cases
		  v = &egraph[last_b][pipe_stages-1];
		  tm = v->str.hi + v->lat.hi;

		  if (plog_len == 0) {
					 return tm;
		  }			 

		  u = &egraph[first_b][0];
		  w = &egraph[last_np][pipe_stages-1];
		  min_ov = min_overlap(w, u);
		  tm -= min_ov;
		  make_est();
		  tm = min(tm, v->str.hi + v->lat.hi);
		  return tm;
}



		  void
check_fetch_times()
{
		  int	    inst, t1, t2;

		  for (inst = 1; inst < eg_len; inst++) {
					 t1= egraph[inst-1][0].str.hi + egraph[inst-1][0].lat.hi;
					 t2= egraph[inst][0].str.hi + egraph[inst][0].lat.hi;
					 if (t1 > t2)
								printf("IF[%d]: %d %d\n", inst-1, t1, t2);
		  }
}



		  void
dump_egraph_times(int start, int end)
{
		  int		    inst, stage;
		  egraph_node_t   *v;

		  if ((start == 0) && (end == 0)) 
					 end = plog_len + body_len + elog_len;
		  printf("dump execution graph times [%d %d]: %d, %d, %d\n",
								start, end, plog_len, body_len, elog_len);
		  for (inst = start; inst < end; inst++) {
					 if ((inst == first_b) || (inst == first_e))
								printf("----------------------------------------------------------\n");
					 printf("%3d[%x]", inst, eg_insts[inst]->inst->addr & 0xffff);
					 if (eg_insts[inst]->bp_flag == BP_CPRED)
								printf("c");
					 else if (eg_insts[inst]->bp_flag == BP_MPRED)
								printf("m");
					 else
								printf("u");
					 if ((eg_insts[inst]->ic_flag == IC_HIT) || (egraph[inst][0].lat.hi == 1))
								printf("/H");
					 else if (eg_insts[inst]->ic_flag == IC_MISS)
								printf("/M");
					 else
								printf("/U");
					 for (stage = 0; stage < pipe_stages; stage++) {
								v = &egraph[inst][stage];
								printf(" * %3d %3d %3d", v->rdy.hi, v->str.hi, v->fin.hi);
					 }
					 printf("\n");
		  }
		  printf("\n");
}



		  void
dump_egraph_earliest(int start, int end)
{
		  int		    inst, stage, event;
		  egraph_node_t   *v;

		  if ((start == 0) && (end == 0)) 
					 end = plog_len + body_len + elog_len;
		  printf("dump execution graph EARLIEST [%d %d]: %d, %d, %d\n", start, end,
								plog_len, body_len, elog_len);
		  for (inst = start; inst < end; inst++) {
					 if ((inst == first_b) || (inst == first_e))
								printf("------------------------------------------------------------------------\n");
					 printf("%3d[%x]", inst, eg_insts[inst]->inst->addr & 0xffff);
					 if (eg_insts[inst]->bp_flag == BP_CPRED)
								printf("c");
					 else if (eg_insts[inst]->bp_flag == BP_MPRED)
								printf("m");
					 else
								printf("u");
					 if ((eg_insts[inst]->ic_flag == IC_HIT) || (egraph[inst][0].lat.hi == 1))
								printf("/H");
					 else if (eg_insts[inst]->ic_flag == IC_MISS)
								printf("/M");
					 else
								printf("/U");
					 for (stage = 0; stage < pipe_stages; stage++) {
								v = &egraph[inst][stage];
								printf(" * %3d %3d %3d", v->rdy.lo, v->str.lo, v->str.lo + v->lat.lo);
					 }
					 printf("\n");
		  }
		  printf("\n");
}


		  static void
dump_econtd(egraph_node_t *v)
{
		  egraph_edge_t   *e;

		  printf("econtd[%d]\n", v->inst);
		  for (e = v->e_contd; e != NULL; e = e->next_in) {
					 printf("<-[%d]: %d\n", e->src->inst, e->contd_type);
		  }
}

/* find an equivalent pi_node in the database */
static int find_pi_nodes(pi_node_t* pi_nodes, int num_pi_nodes, int edge_id, int inst, int stage)
{
		 int i;

		 for (i = 0; i < num_pi_nodes; i++)
		 {
					 if(pi_nodes[i].edge_id == edge_id
								&& pi_nodes[i].inst == inst
								&& pi_nodes[i].stage == stage)

								return 1;
		 }

		 return 0;
}

/* sudiptac ::: set the pi_node parameters here */
void set_pi_nodes(pi_node_t** pi_nodes_ptr, int* num_pi_nodes_ptr, int edge_id, int plen, int blen)
{
		 int inst = 0, stage = 0;
		 egraph_node_t* v; 
		 egraph_edge_t* e;
		 egraph_node_t* out_v;

		 for (stage = 0; stage < pipe_stages; stage++)
		 { 
					 v = &egraph[plen][stage];

					 for (e = v->out; e; e = e->next_out)
					 {
							out_v = e->dst;
							
							/* found a pi_node */
							if(out_v->inst >= plog_len && !find_pi_nodes(*pi_nodes_ptr, *num_pi_nodes_ptr, edge_id, blen, stage))
							{	  
									(*pi_nodes_ptr)[(*num_pi_nodes_ptr)].edge_id = edge_id;
									(*pi_nodes_ptr)[(*num_pi_nodes_ptr)].inst = blen;
									(*pi_nodes_ptr)[(*num_pi_nodes_ptr)].stage = stage;
									*num_pi_nodes_ptr = *num_pi_nodes_ptr + 1;
							}
					 } 
		 }
}

/* sudiptac :::: match a prolog with body and collect the nodes which have a direct link 
 * to the exit block */
void __collect_pi_nodes(pi_node_t** pi_nodes_ptr, int* num_pi_nodes_ptr, int body_edge, int pl_edge, int old_body_len)
{
		 code_link_t* plog;
		 mas_inst_t* plc;
		 mas_inst_t blc;
		 int plen = 0;
		 int blen = 0;
		 tcfg_node_t* bbi;
		 int num_p = 0;
		 int num_b = 0;
		 int ound = 0;
		 int bbi_id = tcfg_edges[body_edge]->dst->id;
		 int first = 1;

		 bbi = tcfg_edges[pl_edge]->dst; 
		 num_b = bbi->bb->num_inst;

		 for (plog = prologs[pl_edge]; plog; plog = plog->next)
		 {
					 num_p = plog->num_inst;
					 first = 1;

					 for (plen = 0; plen < num_p; plen++)
					 {
								plc = &((plog->code)[plen]);

								for (blen = 0; blen < old_body_len; blen++)
								{
										  blc = bodies[body_edge][blen];

										  /* we found a match with a body instruction and 
											* some prolog instruction */
										  if (plc->inst == blc.inst && plc->bbi_id == blc.bbi_id)
										  {
													 if(first)
																create_egraph(plog->code, num_p, NULL, 0, bodies[pl_edge], num_b, BP_CPRED, NULL);
													 set_pi_nodes(pi_nodes_ptr, num_pi_nodes_ptr, body_edge, plen, blen);
													 first = 0;
										  }  
								}
					 }
		 }
}

/* sudiptac :::: collect possible pi_nodes from a basic block */
void collect_pi_nodes(pi_node_t** pi_nodes_ptr, int* num_pi_nodes_ptr, int edge_id, loop_t* this_loop) 
{
		  int max_nodes = 128; /* FIXME: */
		  int step_nodes;
		  int edge;
		  tcfg_node_t* bbi;
		  loop_t* lp;
		  int old_body_len = body_len;

		  if (*num_pi_nodes_ptr == 0)
		  {
					 *pi_nodes_ptr = (pi_node_t *) calloc (max_nodes, sizeof(pi_node_t));
					 CHECK_MEM(*pi_nodes_ptr);
		  }

		  bbi = tcfg_edges[edge_id]->dst;

		  /* search the prologs to which current body is part of */		  
		  for (edge = edge_id + 1; edge < num_tcfg_edges; edge++)
		  {
					 lp = loop_map[tcfg_edges[edge]->dst->id];
					 
					 /* by definition, we are only interested in nodes that are outside 
					  * this_loop */
					 if (loop_comm_ances[this_loop->id][lp->id] == this_loop)
								continue;
					 
					 __collect_pi_nodes(pi_nodes_ptr, num_pi_nodes_ptr, edge_id, edge, old_body_len);
		  }
		   
		  return;
}

/* clean up pi_node structure */
void cleanup_pi_nodes(pi_node_t* pi_nodes)
{
		  if(pi_nodes)
					 free(pi_nodes);
}

/* sudiptac ::: modify a particular offset set to all relevant prologs */
void modify_offsets_to_prologs(int edge_id, offset_link_t* off_link, int inst, int stage, char in_flag)
{
		  int edge;
		  mas_inst_t* plc;
		  mas_inst_t blc;
		  code_link_t* plog;
		  int blen = 0;
		  int plen = 0;
		  int num_inst;
		  int i;
		  
		  if(edge_id == -1)
					 blc = start_body[inst];
		  else
					 blc = bodies[edge_id][inst];
		  
		  /* current body could be a prologue of some other node only if the 
			* latter comes later in the topologically sorted order */
		  for(edge = edge_id + 1; edge < num_tcfg_edges; edge++)
		  {
					 for (plog = prologs[edge]; plog; plog = plog->next)
					 {	
								for (plen = 0; plen < plog->num_inst; plen++)
								{
										  plc = &((plog->code)[plen]);
 
										  /* check whether the body instruction is same as some 
										   * prologue instruction of another body */
										  if (plc->inst == blc.inst && plc->bbi_id == blc.bbi_id)
										  {
													 if (in_flag) { 
													 #if 0
																for (i = 0; i < plog_len; i++)
																{
																	  if (egraph[i][stage].offset_in == plc->offset_in[stage])
																				printf("Gotcha critical in\n");
																}
												     #endif
															    freeOffsets(plc->offset_in[stage]);		
																plc->offset_in[stage] = merge_offsets(NULL, off_link);
												     }
													 else {
													 #if 0
																for (i = 0; i < plog_len; i++)
																{
																	  if (egraph[i][stage].offset_out == plc->offset_out[stage])
																				printf("Gotcha critical out\n");
																}
												     #endif
															    freeOffsets(plc->offset_out[stage]);		
																plc->offset_out[stage] = merge_offsets(NULL, off_link);
												     }
										  }
								}
					 }
		  }
}

/* sudiptac ::: save the offsets to all relevant prologs to which the current 
 * body node is part of */
void save_offsets_to_prologs(int edge_id, offset_link_t*** off_link, int body_len, char in_flag)
{
		  int edge;
		  mas_inst_t* plc;
		  mas_inst_t blc;
		  code_link_t* plog;
		  int blen = 0;
		  int plen = 0;
		  int num_inst;
		  int i, stage;

		  /* current body could be a prologue of some other node only if the 
			* latter comes later in the topologically sorted order */
		  for(edge = 0; edge < num_tcfg_edges; edge++)
		  {
					 for (plog = prologs[edge]; plog; plog = plog->next)
					 {	
								for (plen = 0; plen < plog->num_inst; plen++)
								{
										  plc = &((plog->code)[plen]);

										  for (blen = 0; blen < body_len; blen++)
										  {
													 if(edge_id == -1)
																blc = start_body[blen];
													 else
																blc = bodies[edge_id][blen];
										  
													 /* check whether the body instruction is same as some 
													  * prologue instruction of another body */
													 if (plc->inst == blc.inst && plc->bbi_id == blc.bbi_id)
													 {
																if (in_flag) {
																		  if (!plc->offset_in) {
																				plc->offset_in = (offset_link_t **) calloc(pipe_stages, 
																						  sizeof(offset_link_t *));	  
																				CHECK_MEM(plc->offset_in);	  
																		  }
																		  for (stage = 0; stage < pipe_stages; stage++) {
													 #if 0
																for (i = 0; i < plog_len; i++)
																{
																	  if (egraph[i][stage].offset_in == plc->offset_in[stage])
																				printf("Gotcha critical in in save\n");
																}
												     #endif
																				freeOffsets(plc->offset_in[stage]);		
																				plc->offset_in[stage] = merge_offsets(NULL, off_link[blen][stage]);
																		  }		
																		  #if 0
																		  plc->offset_in = off_link[blen];
																		  #endif		
																}	  
																else {
																		  if (!plc->offset_out) {
																				plc->offset_out = (offset_link_t **) calloc(pipe_stages, 
																						  sizeof(offset_link_t *));	  
																				CHECK_MEM(plc->offset_out);	  
																		  }
													 #if 0
																for (i = 0; i < plog_len; i++)
																{
																	  if (egraph[i][stage].offset_out == plc->offset_out[stage])
																				printf("Gotcha critical in at save\n");
																}
												     #endif
																		  for (stage = 0; stage < pipe_stages; stage++) {
																				freeOffsets(plc->offset_out[stage]);		
																				plc->offset_out[stage] = merge_offsets(NULL, off_link[blen][stage]);
																	      }
																		  #if 0
																		  plc->offset_out = off_link[blen];
																		  #endif
																}	  
#ifdef _NDEBUG_CRPD
					for (i = 0; i < 5; i++) {
							  if (in_flag) continue;
							  if (!plc->offset_out || !plc->offset_out[i]) {
										printf("empty\n"); 
										continue;
							  }
							  else {
										printf("printing saved offsets for prolog 0x%x bbi = %d inst = %d, stage = %d\n", \
												  plog, plc->bbi_id, plen, i);
										dump_offsets(plc->offset_out[i]);
							  }
					}
#endif
													 }
										  }
								}
					 }
		  }
		  
}
/* sudiptac ::: this function is used to merge all offsets of the body node due to different 
 * prologues and epilogues */
offset_link_t*** merge_offsets_from_body(offset_link_t*** off_link, int body_len, char in_flag)
{
		  offset_link_t*** new_off = NULL;
		  int inst, stage;
		  egraph_node_t* node;

		  new_off = (offset_link_t ***) calloc(body_len, sizeof(offset_link_t **));
		  CHECK_MEM(new_off);

		  for(inst = 0; inst < body_len; inst++)
		  {
					 new_off[inst] = (offset_link_t **) calloc(pipe_stages, sizeof(offset_link_t *));
					 CHECK_MEM(new_off[inst]);
		  }

		  for (inst = first_b; inst < first_b + body_len; inst++)
		  {
					 for (stage = 0; stage < pipe_stages; stage++)
					 {
								node = &egraph[inst][stage];
								
								if (!off_link || !off_link[inst - first_b]) {
										  if (in_flag)
													 new_off[inst - first_b][stage] = merge_offsets(NULL, node->offset_in);
										  else
													 new_off[inst - first_b][stage] = merge_offsets(NULL, node->offset_out);
								}
								else
								{
										  if (in_flag)
													 new_off[inst - first_b][stage] = merge_offsets(off_link[inst - first_b][stage], node->offset_in);
										  else
													 new_off[inst - first_b][stage] = merge_offsets(off_link[inst - first_b][stage], node->offset_out);
								}
					 }
		  }

		  /* off_link is obsolete now. Free the related memory */
		  /* FIXME: cleekee: bug detected. Free'd memory is reused. Temporarily disabled. */
#if 1
		  if (off_link)
		  {
					 for (inst = 0; inst < body_len; inst++)
					 {
								if(!off_link[inst])
										  continue;
								for (stage = 0; stage < pipe_stages; stage++)
								{
										  freeOffsets(off_link[inst][stage]);
										  off_link[inst][stage] = NULL;
								}
								off_link[inst] = NULL;
					 }
					 /* cleekee: temp, remove to debug */
					 off_link = NULL;
		  }
#endif
		  return new_off;
}

#ifdef _DEBUG_CRPD
void dump_pi_nodes(pi_node_t* pi_nodes, int num_pi_nodes, loop_t* lp)
{	
		  int i;
		  int edge_id, bbi_id;

		  fprintf(stdout, "\n-----------------------------------------------------------------\n");
		  fprintf(stdout, "Printing PI nodes of loop %d\n", lp->id);

		  for (i = 0; i < num_pi_nodes; i++)
		  {
					 edge_id = pi_nodes[i].edge_id;
					 bbi_id = tcfg_edges[edge_id]->dst->id;

					 fprintf(stdout, "[PI node id = %d,", i);
					 fprintf(stdout, "BB ID = (%d.%d), ", tcfg[bbi_id]->bb->proc->id, tcfg[bbi_id]->bb->id);
					 fprintf(stdout, "Inst = %d, ", pi_nodes[i].inst);
					 fprintf(stdout, "Stage = %d]\n", pi_nodes[i].stage);
		  }
		  fprintf(stdout, "-----------------------------------------------------------------\n");
}
#endif
