#ifndef _SHAREDCACHE_H_
#define _SHAREDCACHE_H_
#include "common.h"
#include "tcfg.h"
#include "cache.h"
#include "task.h"
extern cache_t cache_l2;
extern int assoc_l2, bsize_l2, bsize, assoc, nsets_l2, nsets;
extern int max_cores_sharing_il2;
#define GET_MEM_BLK(x) ((x) / bsize)
#define GET_MEM_BLK_L2(x) ((x) / bsize_l2)
#define GET_SET_L1(x) ((((x) / bsize)) % nsets)
#define GET_SET_L2(x) ((((x) / bsize_l2)) % nsets_l2)
#define CINFTY assoc_l2
void compute_inter_core_conflicts();
#endif
